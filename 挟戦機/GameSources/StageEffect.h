#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class SSAnime :  public SS5ssae;
	//	用途: 画像のアニメーション
	//--------------------------------------------------------------------------------------
	class SSAnime : public SS5ssae {
		Vector3 m_Scale;
		Vector3 m_Position;
	public:
		//構築と消滅
		SSAnime(const shared_ptr<Stage>& StagePtr, const wstring& BaseDir,
			const wstring& SSAEDir,const wstring& AnimDir,
			const Vector3& Pos, const Vector3& Scale);
		virtual ~SSAnime();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};



	//----------------------------------------------------------------------
	//Spriteの作成
	//画像を2次元で表示
	class Sprite : public GameObject {

	protected:
		//透過処理をするか
		bool m_Trace;
		Vector2 m_StartScale;
		Vector3 m_StartPos;
		//画像のテクスチャのキー登録
		wstring m_Media;
		//頂点配列の設定
		vector<VertexPositionTexture> m_BackupVertices;
	public:
		//
		Sprite(const shared_ptr<Stage> & StagePtr, bool Trace,
			const Vector2& StartScale, const Vector3& StartPos, const wstring& Media);
		virtual ~Sprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};

	//フェードイン・フェードアウトの作成
	class Fade : public GameObject {
		//白か黒を設定
		wstring m_WorB;
		//明けかくもりか
		wstring m_INorOUT;
	public:
		Fade(const shared_ptr<Stage>& StagePtr,
			const wstring& WorB, const wstring& INorOUT);
		virtual ~Fade() {}
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		//文字列を取得する関数
		wstring GetFader()const;
		//取得したフェードのし方を再定義する関数
		void SetFader(const wstring& INorOUT);
	};


	enum class SelectStgeParam {
		ToStage1,
		ToStage2,
		ToStage3,
	};

	//-----------------------------------------------------------------------------
	//
	//

	class BulletTypeSp : public Sprite {
		bool m_Trace;
		Vector2 m_StartScale;
		Vector3 m_StartPos;
		wstring m_Media;

		vector<VertexPositionTexture> m_BackupVertices;
	public:
		//
		BulletTypeSp(const shared_ptr<Stage> & StagePtr, bool Trace,
			const Vector2& StartScale, const Vector3& StartPos, const wstring& Media);
		virtual ~BulletTypeSp();
		virtual void OnUpdate() override;

		void B_TypeChanger();

	};


	//SelectSpriteの生成
	class SelectSprite : public Sprite {
		bool m_Trace;
		Vector2 m_StartScale;
		Vector3 m_StartPos;
		float m_TotalTime;
		wstring m_Media;

		bool m_Select, m_Select1;

		SelectStgeParam m_Param;


		vector<VertexPositionTexture> m_BackupVertices;
	public:
		//
		SelectSprite(const shared_ptr<Stage> & StagePtr, bool Trace,
			const Vector2& StartScale, const Vector3& StartPos, const wstring& Media);
		virtual ~SelectSprite();
		virtual void OnUpdate() override;
	};



	//ブースターの演出画像
	class BoosterSprite : public GameObject {

	protected:
		//透過処理をするか
		bool m_Trace;
		Vector2 m_StartScale;
		Vector3 m_StartPos;
		//画像のテクスチャのキー登録
		wstring m_Media;
		//頂点配列の設定
		vector<vector<VertexPositionColorTexture>> m_BackupVertices;
		vector<shared_ptr<MeshResource>> m_Mesh;
		shared_ptr<Enemy>m_EnemyPtr;
	public:
		//
		BoosterSprite(const shared_ptr<Stage> & StagePtr, bool Trace,
			const Vector2& StartScale, const Vector3& StartPos, const wstring& Media, const shared_ptr<Enemy>& EnemyPtr);
		virtual ~BoosterSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		float m_time = 0;
		int m_count = 0;

		bool DestroyFLG = false;


	};

	//爆発の画像
	class ExplosionSprite : public GameObject {

	protected:
		//透過処理をするか
		bool m_Trace;
		Vector2 m_StartScale;
		Vector3 m_StartPos;
		//画像のテクスチャのキー登録
		wstring m_Media;
		//頂点配列の設定

		Vector3 m_Down;
		vector<vector<VertexPositionColorTexture>> m_BackupVertices;
		vector<shared_ptr<MeshResource>> m_Mesh;
	public:
		//
		ExplosionSprite(const shared_ptr<Stage> & StagePtr, bool Trace,
			const Vector2& StartScale, const Vector3& StartPos, const wstring& Media);
		virtual ~ExplosionSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		float m_time = 0;
		int m_count = 0;
		float DestroyTime = 90;

	};



	//---------------------------
	//分割して表示する画像
	//---------------------------
	class SplitSprite : public GameObject {
		//1枚の大きさ
		Vector2 m_Scale;
		//初期位置
		Vector2 m_Pos;
		//縦の分割数
		UINT m_VerticalCount;
		//横の分割数
		UINT m_HorizontalCount;
		//テクスチャリソース
		wstring m_TextureKey;
		//透過許可
		bool m_Trace;
		//バックアップ頂点データ
		vector<vector<VertexPositionColorTexture> > m_BackupVertices;
		//表示する番号
		int m_Num = 0;
		//選択
		void SelectNumPos();
		//スティックを倒したか
		bool IsStick = false;

	public:
		SplitSprite(const shared_ptr<Stage>& Stageptr, const wstring& TextureKey,
			const bool& Trace, const Vector2& Scale, const Vector2& Position,
			const UINT& VerticalCount, const UINT& HorizontalCount);
		//
		virtual ~SplitSprite();
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate()override;

	};

	//HPの画像作成
	class HPSprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		HPSprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~HPSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		bool Flag_Hit = false;

	};


	//弾数UI

	class RemaingBullet : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float Max;
		//float Now;
		float m_TotalTime;

		int PreviousBullet;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		RemaingBullet(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~RemaingBullet();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};



	//--------------------------------------------------------------------------------------
	///	連番アニメーションクラス
	//--------------------------------------------------------------------------------------
	class SerialAnimeObject : public GameObject {
		Vector3 m_StartPos;
		wstring m_TexResKey;
		size_t m_XPieceCount;
		size_t m_YPieceCount;
		//現在のピース番号
		size_t m_PieceNowNumber;
		//1秒当たりの再生枚数
		size_t m_PieceBySecond;
		//計算に使うピースあたりのタイム
		float m_PieceTotalTime;
		//再生中かどうかのフラグ
		bool m_IsRun;
		//バックアップ頂点
		vector<VertexPositionColorTexture> m_BackupVertices;
		//このオブジェクトのみで使用するスクエアメッシュ
		shared_ptr<MeshResource> m_SquareMeshResource;
		void CreateSquareMesh();
	public:
		//構築と破棄
		//--------------------------------------------------------------------------------------
		/*!
		@brief	コンストラクタ
		@param[in]	StartPos	位置
		*/
		//--------------------------------------------------------------------------------------
		SerialAnimeObject(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos,
			const wstring& TexResKey, size_t XPieceCount, size_t YPieceCount);
		//--------------------------------------------------------------------------------------
		/*!
		@brief	デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~SerialAnimeObject() {}

		void SetPieceBySecond(size_t sz) {
			m_PieceBySecond = sz;
		}

		void Start(size_t StartPicNum = 0) {
			m_IsRun = false;
			size_t maxNu = m_XPieceCount * m_YPieceCount;
			if (StartPicNum >= maxNu) {
				StartPicNum = maxNu - 1;
			}
			m_PieceNowNumber = StartPicNum;
			m_PieceTotalTime = 0;
			m_IsRun = true;
		}

		void Stop() {
			m_IsRun = false;
		}
		//--------------------------------------------------------------------------------------
		/*!
		@brief	初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief	更新
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate() override;
	};

}
//end basecross
