/*!
@file Scene.h
@brief シーン
*/
#pragma once

#include "stdafx.h"

namespace basecross{

	struct GameParametors {
		//プレイヤー耐久
		int m_PlayerHP;
		//ボス耐久
		int m_BossHP;
		//捕獲した敵のタイプ
		UINT m_EnemyType;
		//敵の弾数,残りの数字に応じて増やすなど
		UINT m_BulletGain;
		//挟んだ
		bool m_PLGetEnemy;
		GameParametors():
			m_PlayerHP(),
			m_BossHP(),
			m_EnemyType(),
			m_BulletGain(),
			m_PLGetEnemy(false)
			{}
	};



	struct EnemyPalametor {
		//弾の種類
		wstring m_BulletType[5];
		//発射のパターン
		wstring m_FirePatern[5];
		//威力
		UINT m_DamegePower[5];
		//残弾数
		UINT m_RemaindBullet[5];
		//動き
		wstring m_Moving[5];
		//スピード
		UINT m_SpeedMetor[5];

		EnemyPalametor() :
			m_BulletType(),
			m_FirePatern(),
			m_DamegePower(),
			m_RemaindBullet(),
			m_Moving(),
			m_SpeedMetor()
		{}
	};

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	class Scene : public SceneBase{
		//音楽関係
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
	
		//パラメータ
		GameParametors m_GameParam;

		EnemyPalametor m_EnemyParam;

		wstring m_PreAudio;

	public:
	

		//ステージの番号を格納
		int m_StageNumber = 11;

		int DestroyCount = 0;

		/////追加
		//ステージ番号の取得
		const int getStageNumber()const {
			return m_StageNumber;
		}
		//ステージ番号の設定
		void setStageNumber(int num) {
			m_StageNumber = num;
		}

		Vector3 TPos;


		Vector3 EnemyPos;
		bool DestroyFLG = false;

		//6/23///////////////////////
		bool GameOver = false;
		bool GameClear = false;
		int flg = 0;
		//ステージクリア時に画像を出すまでの時間
		float DrawTime = 0;
		///////////////////6/23///////

		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		*/
		//--------------------------------------------------------------------------------------
		Scene() :SceneBase(),m_GameParam(),m_EnemyParam(){}
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Scene(){}
		//--------------------------------------------------------------------------------------
		/*!
		@brief ゲームパラメータを得る
		@return	ゲームパラメータ
		*/
		//--------------------------------------------------------------------------------------
		GameParametors& GetGameParametors() {
			return m_GameParam;
		}
		//--------------------------------------------------------------------------------------
		/*!
		@brief エネミーのパラメータ得る
		@return	ゲームパラメータ
		*/
		//--------------------------------------------------------------------------------------
		EnemyPalametor& GetEnemyParametors() {
			return m_EnemyParam;
		}

		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;

		//イベント取得
		virtual void OnEvent(const shared_ptr<Event>& event)override;
	
		void CreateResource();
	};

}

//end basecross
