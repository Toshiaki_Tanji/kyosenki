
/*!
@file Scene.cpp
@brief シーン実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------

	void Scene::CreateResource() {
		wstring DataDir;
		//サンプルのためアセットディレクトリを取得
		//App::GetApp()->GetAssetsDirectory(DataDir);
		//各ゲームは以下のようにデータディレクトリを取得すべき
		App::GetApp()->GetDataDirectory(DataDir);
		wstring	strTexture = DataDir + L"\\Texture\\TitleBt.png";
		App::GetApp()->RegisterTexture(L"TITLEBUTTON_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Next.png";
		App::GetApp()->RegisterTexture(L"NEXTBUTTON_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Retry.png";
		App::GetApp()->RegisterTexture(L"RETRYBUTTON_TX", strTexture);

		//
		strTexture = DataDir + L"\\Texture\\Space.png";
		App::GetApp()->RegisterTexture(L"GAMEBG_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Gauge_Empty.png";
		App::GetApp()->RegisterTexture(L"GAUGE_EMP_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Gauge_Max.png";
		App::GetApp()->RegisterTexture(L"GAUGE_MAX_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Life_Empty.png";
		App::GetApp()->RegisterTexture(L"LIFE_EMP_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Life_max.png";
		App::GetApp()->RegisterTexture(L"LIFE_MAX_TX", strTexture);
		//タイトルの
		strTexture = DataDir + L"\\Texture\\TitleBg.png";
		App::GetApp()->RegisterTexture(L"TITLE_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Title_Logo_3.png";
		App::GetApp()->RegisterTexture(L"TITLELOGO_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Title_Push.png";
		App::GetApp()->RegisterTexture(L"TITLEPUSH_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\BG_SS.png";
		App::GetApp()->RegisterTexture(L"GALAXY_TX", strTexture);

		strTexture = DataDir + L"\\Texture\\nan.png";
		App::GetApp()->RegisterTexture(L"NUN_TX", strTexture);
		//セレクトの
		strTexture = DataDir + L"\\Texture\\Select.png";
		App::GetApp()->RegisterTexture(L"SELECT_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\star.png";
		App::GetApp()->RegisterTexture(L"STAR_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\background.png";
		App::GetApp()->RegisterTexture(L"GALACY_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Frame.png";
		App::GetApp()->RegisterTexture(L"FRAME_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\BG_TiringTX.png";
		App::GetApp()->RegisterTexture(L"BGTILE_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\spark.png";
		App::GetApp()->RegisterTexture(L"KIRAKIRA_TX", strTexture);

		strTexture = DataDir + L"\\Texture\\RED.png";
		App::GetApp()->RegisterTexture(L"RED_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\BLUE.png";
		App::GetApp()->RegisterTexture(L"BULE_TX", strTexture);

		strTexture = DataDir + L"\\Texture\\Namber_B.png";
		App::GetApp()->RegisterTexture(L"NAMBER_TX", strTexture);


		//ステージクリア時に出すやつ/////////////////////////////////
		strTexture = DataDir + L"\\Texture\\StageClear.png";
		App::GetApp()->RegisterTexture(L"CLEAR_TX", strTexture);

		strTexture = DataDir + L"\\Texture\\Next.png";
		App::GetApp()->RegisterTexture(L"NEXT_TX", strTexture);

		strTexture = DataDir + L"\\Texture\\TitleBt.png";
		App::GetApp()->RegisterTexture(L"GOTITLE_TX", strTexture);

		strTexture = DataDir + L"\\Texture\\Cursor.png";
		App::GetApp()->RegisterTexture(L"CURSOR_TX", strTexture);


		strTexture = DataDir + L"\\Texture\\TitleBt.png";
		App::GetApp()->RegisterTexture(L"GOTITLE_TX", strTexture);

		strTexture = DataDir + L"\\Texture\\Cursor.png";
		App::GetApp()->RegisterTexture(L"CURSOR_TX", strTexture);

		strTexture = DataDir + L"\\Texture\\Score.png";
		App::GetApp()->RegisterTexture(L"SCORE_TX", strTexture);


		strTexture = DataDir + L"\\Texture\\Rank.png";
		App::GetApp()->RegisterTexture(L"RANK_TX", strTexture);

		strTexture = DataDir + L"\\Texture\\S.png";
		App::GetApp()->RegisterTexture(L"S_TX", strTexture);
		///////////////////////////////////////////////////////////////

		//ステージセレクトのやつ
		strTexture = DataDir + L"\\Texture\\Select.png";
		App::GetApp()->RegisterTexture(L"SELECT_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Select2.jpg";
		App::GetApp()->RegisterTexture(L"SELECT2_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Select3.jpg";
		App::GetApp()->RegisterTexture(L"SELECT3_TX", strTexture);
		//フェードのやつ
		strTexture = DataDir + L"\\Texture\\White.png";
		App::GetApp()->RegisterTexture(L"FADE_TX", strTexture);
		//クリアとゲームオーバー
		strTexture = DataDir + L"\\Texture\\BG_SC.png";
		App::GetApp()->RegisterTexture(L"STAGECLEARBG_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\SteageClear.png";
		App::GetApp()->RegisterTexture(L"STAGECLEARLOGO_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\BG_GO.png";
		App::GetApp()->RegisterTexture(L"GAMEOVERBG_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\gameover.png";
		App::GetApp()->RegisterTexture(L"GAMEOVERLOGO_TX", strTexture);

		strTexture = DataDir + L"\\Texture\\LanguageMap.png";
		App::GetApp()->RegisterTexture(L"LANGAGEMAP_TX", strTexture);


		strTexture = DataDir + L"\\Bullet\\Bullet.png";
		App::GetApp()->RegisterTexture(L"BULLET_TX", strTexture);


		//モデルテクスチャ
		strTexture = DataDir + L"Funnel_Left.tga";
		App::GetApp()->RegisterTexture(L"FUNNEL_LEFT_TX", strTexture);
		strTexture = DataDir + L"Funnel_Right.tga";
		App::GetApp()->RegisterTexture(L"FUNNEL_RIGHT_TX", strTexture);
		strTexture = DataDir + L"Enemy_Laser_TX.tga";
		App::GetApp()->RegisterTexture(L"ZAKOENEMY1_TX", strTexture);
		strTexture = DataDir + L"Enemy_001.tga";
		App::GetApp()->RegisterTexture(L"ZAKOENEMY2_TX", strTexture);
		strTexture = DataDir + L"Enemy_Missile.tga";
		App::GetApp()->RegisterTexture(L"ZAKOENEMY3_TX", strTexture);
		strTexture = DataDir + L"Player.tga";
		App::GetApp()->RegisterTexture(L"PLAYER_TX", strTexture);

		strTexture = DataDir + L"Boss_TX.tga";
		App::GetApp()->RegisterTexture(L"BOSS_TX", strTexture);
		strTexture = DataDir + L"Catapult.tga";
		App::GetApp()->RegisterTexture(L"CATAPULT_TX", strTexture);
		strTexture = DataDir + L"Stone.tga";
		App::GetApp()->RegisterTexture(L"METEORITE_TX",strTexture);


		//エフェクト用
		strTexture = DataDir + L"\\Effect\\kirakira.png";
		App::GetApp()->RegisterTexture(L"KIRAKIRA_TX", strTexture);
		strTexture = DataDir + L"\\Effect\\Laser_anime.png";
		App::GetApp()->RegisterTexture(L"LASERANIME_TX", strTexture);
		//エフェクト用
		strTexture = DataDir + L"\\EffectBoost\\Booster.png";
		App::GetApp()->RegisterTexture(L"Boost_TX", strTexture);
		//爆発エフェクト用
		strTexture = DataDir + L"\\Effect\\Explosion_Con.png";
		App::GetApp()->RegisterTexture(L"Explosion_TX", strTexture);
		//仮の回復アイテム
		strTexture = DataDir + L"\\Texture\\Cure_Item.png";
		App::GetApp()->RegisterTexture(L"CURE_TX", strTexture);


		strTexture = DataDir + L"\\Texture\\Bullat_Empty.png";
		App::GetApp()->RegisterTexture(L"BULLET_EMPTY_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Bullet_Remaining.png";
		App::GetApp()->RegisterTexture(L"BULLET_REMAINING_TX", strTexture);
		strTexture = DataDir + L"\\Texture\\Bullet_Frame.png";
		App::GetApp()->RegisterTexture(L"BULLET_FRAME_TX", strTexture);


		//Laserのエフェクト用
		strTexture = DataDir + L"\\Effect\\Laser_01.png";
		App::GetApp()->RegisterTexture(L"LASER_TX", strTexture);

		//LaserのRingエフェクト用
		strTexture = DataDir + L"\\Effect\\Ring.png";
		App::GetApp()->RegisterTexture(L"LASERRING_TX", strTexture);

		//Laserのボールエフェクト用
		strTexture = DataDir + L"\\Effect\\Ball.png";
		App::GetApp()->RegisterTexture(L"LASERBALL_TX", strTexture);
		//Laserのボールエフェクト用
		strTexture = DataDir + L"\\Effect\\Warp.png";
		App::GetApp()->RegisterTexture(L"WARP_TX", strTexture);

		//オペレーター
		strTexture = DataDir + L"Operator.png";
		App::GetApp()->RegisterTexture(L"OPERATOR_TX",strTexture);

		//チュートリアル
		strTexture = DataDir + L"\\Tutorial_TX\\Tutorial_1.png";
		App::GetApp()->RegisterTexture(L"TUTOREAL1_TX", strTexture);
		strTexture = DataDir + L"\\Tutorial_TX\\Tutorial_2.png";
		App::GetApp()->RegisterTexture(L"TUTOREAL2_TX", strTexture);
		strTexture = DataDir + L"\\Tutorial_TX\\Tutorial_3.png";
		App::GetApp()->RegisterTexture(L"TUTOREAL3_TX", strTexture);
		strTexture = DataDir + L"\\Tutorial_TX\\Tutorial_4.png";
		App::GetApp()->RegisterTexture(L"TUTOREAL4_TX", strTexture);
		//
		strTexture = DataDir + L"\\zako\\Enemy_B.png";
		App::GetApp()->RegisterTexture(L"ENEMY_B_TX", strTexture);
		strTexture = DataDir + L"\\zako\\Enemy_F.png";
		App::GetApp()->RegisterTexture(L"ENEMY_F_TX", strTexture);
		strTexture = DataDir + L"\\zako\\Enemy_L.png";
		App::GetApp()->RegisterTexture(L"ENEMY_L_TX", strTexture);
		strTexture = DataDir + L"\\zako\\Enemy_M.png";
		App::GetApp()->RegisterTexture(L"ENEMY_M_TX", strTexture);
		strTexture = DataDir + L"\\zako\\Enemy_S.png";
		App::GetApp()->RegisterTexture(L"ENEMY_S_TX", strTexture);
		strTexture = DataDir + L"\\zako\\clear.png";
		App::GetApp()->RegisterTexture(L"EMPTY_TX", strTexture);


		//ファン練り
		strTexture = DataDir + L"Funnel_UV.tga";
		App::GetApp()->RegisterTexture(L"FUNNEL_TX", strTexture);


		//ボーン入りモデル
		auto ModelMesh = MeshResource::CreateBoneModelMesh(DataDir, L"Boss_Deformation_Animation.bmf");
		App::GetApp()->RegisterResource(L"BOSS_MESH", ModelMesh);
		ModelMesh = MeshResource::CreateBoneModelMesh(DataDir, L"CatapultOpen.bmf");
		App::GetApp()->RegisterResource(L"CATAPULT_MESH", ModelMesh);

		//スタティックモデル
		auto StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Enemy_Laser_1.bmf");
		App::GetApp()->RegisterResource(L"ZAKOENEMY1_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Enemy_Barrett_2.bmf");
		App::GetApp()->RegisterResource(L"ZAKOENEMY2_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Enemy_Missile.bmf");
		App::GetApp()->RegisterResource(L"ZAKOENEMY3_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Player.bmf");
		App::GetApp()->RegisterResource(L"PLAYER_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Funnel_Left.bmf");
		App::GetApp()->RegisterResource(L"FUNNEL_LEFT_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Funnel_Right.bmf");
		App::GetApp()->RegisterResource(L"FUNNEL_RIGHT_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Stone.bmf");
		App::GetApp()->RegisterResource(L"METEORITE_01_MESH", StaticModelMesh);
		StaticModelMesh = MeshResource::CreateStaticModelMesh(DataDir, L"Stone_02.bmf");
		App::GetApp()->RegisterResource(L"METEORITE_02_MESH", StaticModelMesh);


		//音楽関係
		//サウンド
		wstring strsewav = DataDir + L"\\SE\\launcher1.wav";
		App::GetApp()->RegisterWav(L"MachineGun", strsewav);
		strsewav = DataDir + L"\\SE\\retro12.wav";
		App::GetApp()->RegisterWav(L"Explosion", strsewav);
		strsewav = DataDir + L"\\SE\\syber05.wav";
		App::GetApp()->RegisterWav(L"OpenTheAny", strsewav);
		strsewav = DataDir + L"\\SE\\system13.wav";
		App::GetApp()->RegisterWav(L"Cursor", strsewav);
		strsewav = DataDir + L"\\SE\\system03.wav";
		App::GetApp()->RegisterWav(L"Starting", strsewav);
		strsewav = DataDir + L"\\SE\\system12.wav";
		App::GetApp()->RegisterWav(L"Decide", strsewav);
		//BGM
		wstring strMusic = DataDir + L"\\BGM\\bgm45.wav";
		App::GetApp()->RegisterWav(L"bgm45",strMusic);
		strMusic = DataDir + L"\\BGM\\cyber11.wav";
		App::GetApp()->RegisterWav(L"Battle1", strMusic);
		strMusic = DataDir + L"\\BGM\\NewWorld.wav";
		App::GetApp()->RegisterWav(L"BOSSBattle", strMusic);
		strMusic = DataDir + L"\\BGM\\vigilante.wav";
		App::GetApp()->RegisterWav(L"Title",strMusic);

	}

	void Scene::OnCreate(){
		try {
			CreateResource();

			//BGMの再生
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"Title");
			m_AudioObjectPtr->Start(L"Title", XAUDIO2_LOOP_INFINITE, 0.4f);
			m_PreAudio = L"Title";
			//最初のアクティブステージの設定
			ResetActiveStage<TitleStage>();
		}
		catch (...) {
			throw;
		}
	}


	void Scene::OnEvent(const shared_ptr<Event>& event)
	{
		////最初のアクティブステージの設定
		//ResetActiveStage<TitleStage>();
		///タイトルへ
		if (event->m_MsgStr == L"ToTitle") {
			//最初のアクティブステージの設定
			ResetActiveStage<TitleStage>();

			if (m_PreAudio != L"Title") {
				m_AudioObjectPtr->Stop(m_PreAudio);
			}
			//BGMの再設定
			m_AudioObjectPtr->AddAudioResource(L"Title");
			m_AudioObjectPtr->Start(L"Title", XAUDIO2_LOOP_INFINITE, 0.4f);
			m_PreAudio = L"Title";
		}
		else if (event->m_MsgStr == L"ToSelect") {
			//最初のアクティブステージの設定
			ResetActiveStage<SelectStage>();
			if (m_PreAudio != L"Title") {
				m_AudioObjectPtr->Stop(m_PreAudio);
				//BGMの再設定
				m_AudioObjectPtr->AddAudioResource(L"Title");
				m_AudioObjectPtr->Start(L"Title", XAUDIO2_LOOP_INFINITE, 0.4f);
				m_PreAudio = L"Title";
			}
		}
		else if (event->m_MsgStr == L"ToGame") {
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage>();
			//BGMの再設定
			m_AudioObjectPtr->Stop(m_PreAudio);
			m_AudioObjectPtr->AddAudioResource(L"Battle1");
			m_AudioObjectPtr->Start(L"Battle1", XAUDIO2_LOOP_INFINITE, 0.4f);
			m_PreAudio = L"Battle1";

		}

		else if (event->m_MsgStr == L"BossSpawn") {
			//BGMの再設定
			m_AudioObjectPtr->Stop(m_PreAudio);
			m_AudioObjectPtr->AddAudioResource(L"BOSSBattle");
			m_AudioObjectPtr->Start(L"BOSSBattle", XAUDIO2_LOOP_INFINITE, 0.4f);
			m_PreAudio = L"BOSSBattle";

		}


	}


}
//end basecross
