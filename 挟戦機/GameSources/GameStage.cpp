/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------

	//ビューとライトの作成
	void GameStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		////ビューのカメラの設定
		auto PtrMainCamera = ObjectFactory::Create<MainCamera>();
		PtrView->SetCamera(PtrMainCamera);

		//auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		//PtrView->SetCamera(PtrLookAtCamera);
		//PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		//PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));

		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}


	//プレートの作成
	void GameStage::CreatePlate() {
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTrans = Ptr->GetComponent<Transform>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
		PtrTrans->SetScale(60.0f, 120.0f, 1.0f);
		PtrTrans->SetQuaternion(Qt);
		PtrTrans->SetPosition(0.0f, 0.0f, 60.0f);
		auto ColPtr = Ptr->AddComponent<CollisionRect>();

		//ステージへのゲームオブジェクトの追加
		auto PtrBG = AddGameObject<GameObject>();
		auto PtrTransBG = PtrBG->GetComponent<Transform>();
		PtrTransBG->SetScale(64.0f, 64.0f, 1.0f);
		PtrTransBG->SetQuaternion(Qt);
		PtrTransBG->SetPosition(32.0f, -10.0f, 32.0f);
		//描画コンポーネントの追加
		auto DrawComp = PtrBG->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"GALACY_TX");
		//
		auto PtrBG2 = AddGameObject<GameObject>();
		auto PtrTransBG2 = PtrBG2->GetComponent<Transform>();
		PtrTransBG2->SetScale(64.0f, 64.0f, 1.0f);
		PtrTransBG2->SetQuaternion(Qt);
		PtrTransBG2->SetPosition(-32.0f, -10.0f, 32.0f);
		//描画コンポーネントの追加
		auto DrawComp2 = PtrBG2->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp2->SetMeshResource(L"DEFAULT_SQUARE");
		//描画コンポーネントテクスチャの設定
		DrawComp2->SetTextureResource(L"GALACY_TX");

		Vector3 Rot(XM_PIDIV2, 0, 0);
			Vector3 Pos(0,-3,0);
			//プレートの回転の引数はクオータニオンになっているので変換
			//Quaternion Qt;
			Qt.RotationRollPitchYawFromVector(Rot);
			//ステージへのゲームオブジェクトの追加
			AddGameObject<TileFloor>(32,32,Vector3(256,256,1), Qt, Pos);
	//	}


	}

	//セルマップの作成
	void GameStage::CreateStageCellMap() {
		auto Group = CreateSharedObjectGroup(L"CellMap");
		float  PieceSize = 1.0f;
		auto Ptr = AddGameObject<StageCellMap>(Vector3(-10.0f, 0, 4.0f), PieceSize, 20, 7);
		//セルマップの区画を表示する場合は以下の設定
		Ptr->SetDrawActive(false);
		//さらにセルのインデックスとコストを表示する場合は以下の設定
		Ptr->SetCellStringActive(false);
		SetSharedGameObject(L"StageCellMap1", Ptr);
		//グループに追加
		Group->IntoGroup(Ptr);

		Ptr = AddGameObject<StageCellMap>(Vector3(-10.0f, 0, 16.0f), PieceSize, 20, 7);
		//セルマップの区画を表示する場合は以下の設定
		Ptr->SetDrawActive(false);
		//さらにセルのインデックスとコストを表示する場合は以下の設定
		//Ptr->SetCellStringActive(false);
		SetSharedGameObject(L"StageCellMap2", Ptr);
		//グループに追加
		Group->IntoGroup(Ptr);

		//以下3つ目のセルマップはグループを別にする
		//動的にセルマップを変更する敵用
		auto Group2 = CreateSharedObjectGroup(L"CellMap2");

		Ptr = AddGameObject<StageCellMap>(Vector3(-10.0f, 0, 28.0f), PieceSize, 20, 7);
		//セルマップの区画を表示する場合は以下の設定
		Ptr->SetDrawActive(false);
		//さらにセルのインデックスとコストを表示する場合は以下の設定
		Ptr->SetCellStringActive(false);
		SetSharedGameObject(L"StageCellMap3", Ptr);
		//グループに追加
		Group2->IntoGroup(Ptr);


	}



	//固定のボックスの作成
	void GameStage::CreateFixedBox() {
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			{
				Vector3(1.0f, 0.5f, 120.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(29.5f, 0.25f, 60.0f)
			},
			{
				Vector3(1.0f, 0.5f, 120.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(-29.5f, 0.25f, 60.0f)
			},

			{
				Vector3(60.0f, 0.5f, 1.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, 0.25f, 0.5f)
			},

			{
				Vector3(60.0f, 0.5f, 1.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, 0.25f, 119.5f)
			},
		};

		//ボックスのグループを作成
		auto BoxGroup = CreateSharedObjectGroup(L"FixedBoxes");
		//オブジェクトの作成
		for (auto v : Vec) {
			auto BoxPtr = AddGameObject<FixedBox>(v[0], v[1], v[2]);
			//ボックスのグループに追加
			BoxGroup->IntoGroup(BoxPtr);
		}
		//最初の2つのセルマップへのボックスのコスト設定
		SetCellMapCost(L"CellMap");
		//奥のセルマップへのボックスのコスト設定
		SetCellMapCost(L"CellMap2");
	}

	//固定のボックスのコストをセルマップに反映
	void GameStage::SetCellMapCost(const wstring& CellMapGroupName) {
		//セルマップ内にFixedBoxの情報をセット
		auto Group = GetSharedObjectGroup(CellMapGroupName);
		auto BoxGroup = GetSharedObjectGroup(L"FixedBoxes");

		//セルマップグループを取得
		for (auto& gv : Group->GetGroupVector()) {
			auto MapPtr = dynamic_pointer_cast<StageCellMap>(gv.lock());
			if (MapPtr) {
				//セルマップからセルの配列を取得
				auto& CellVec = MapPtr->GetCellVec();
				//ボックスグループからボックスの配列を取得
				auto& BoxVec = BoxGroup->GetGroupVector();
				vector<AABB> ObjectsAABBVec;
				for (auto& v : BoxVec) {
					auto FixedBoxPtr = dynamic_pointer_cast<FixedBox>(v.lock());
					if (FixedBoxPtr) {
						auto ColPtr = FixedBoxPtr->GetComponent<CollisionObb>();
						//ボックスの衝突判定かラッピングするAABBを取得して保存
						ObjectsAABBVec.push_back(ColPtr->GetWrappingAABB());
					}
				}
				//セル配列からセルをスキャン
				for (auto& v : CellVec) {
					for (auto& v2 : v) {
						for (auto& vObj : ObjectsAABBVec) {
							if (HitTest::AABB_AABB_NOT_EQUAL(v2.m_PieceRange, vObj)) {
								//ボックスのABBとNOT_EQUALで衝突判定
								v2.m_Cost = -1;
								break;
							}
						}
					}
				}
			}
		}
	}


	//プレイヤーの作成
	void GameStage::CreatePlayer() {
		CreateSharedObjectGroup(L"AttackBall");
		//アタックボールを10個用意する
		for (int i = 0; i < 100; i++) {
			AddGameObject<AttackBall>();
		}
		CreateSharedObjectGroup(L"AttackLaser");
		//アタックレーザーを100個用意する
		for (int i = 0; i < 100; i++) {
			AddGameObject<AttackLaser>();
		}
		CreateSharedObjectGroup(L"AttackMissile");
		//アタックミサイルを50個用意する
		for (int i = 0; i < 50; i++) {
			AddGameObject<AttackMissile>();
		}

		//子機の用意
		auto FunnelPtr = AddGameObject<CatchFunnel>(L"RIGHTFUNNEL");
		SetSharedGameObject(L"FUNNEL_R", FunnelPtr);

		FunnelPtr = AddGameObject<CatchFunnel>(L"LEFTFUNNEL");
		SetSharedGameObject(L"FUNNEL_L", FunnelPtr);

		//えっふぇくと
		auto Effect = AddGameObject<KirakiraEffect>();
		SetSharedGameObject(L"PLEFFECT", Effect);
		App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PLGetEnemy = false;
		//エネミーのタイプを初期化
		App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType = 0;
		//プレーヤーの作成
		auto PlayerPtr = AddGameObject<Player>(Vector3(0.0f, 0.625f, 2.0f));
		//シェア配列にプレイヤーを追加
		SetSharedGameObject(L"Player", PlayerPtr);
}


	//敵の作成
	void GameStage::CreateEnemy() {

		CreateSharedObjectGroup(L"EnemyAttackBall");
		//アタックボールを50個用意する
		for (int i = 0; i < 50; i++) {
			AddGameObject<EnemyAttackBall>();
		}
		CreateSharedObjectGroup(L"EnemyAttackLaser");
		//アタックボールを50個用意する
		for (int i = 0; i < 50; i++) {
			AddGameObject<EnemyAttackLaser>();
		}
		auto MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap1");
		auto EnemyGroup = CreateSharedObjectGroup(L"EnemyGroup");
		vector< vector<Vector3> > Vec1 = {
			{
				Vector3(0.25f, 0.25f, 0.25f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(-2.5f, 0.125f, 9.0f)
			},
		};

		MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap2", 1);
		vector< vector<Vector3> > Vec2 = {
			{
				Vector3(0.25f, 0.25f, 0.25f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(4.5f, 0.125f, 21.0f)
			},
			{
				Vector3(0.25f, 0.25f, 0.25f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(-2.5f, 0.125f, 24.0f)
			},

		};
		//3つ目の敵

		MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap3");
		vector< vector<Vector3> > Vec3 = {
			{
				Vector3(0.25f, 0.25f, 0.25f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(6.5f, 0.125f, 33.0f)
			},
			{
				Vector3(0.25f, 0.25f, 0.25f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(1.5f, 0.125f, 17.0f)
			},
			{
				Vector3(0.25f, 0.25f, 0.25f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(-3.5f, 0.125f, 15.0f)
			},
			{
				Vector3(0.25f, 0.25f, 0.25f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(2.5f, 0.125f, 21.0f)
			},
		};
		
		vector< vector<Vector3> > Vec4 = {
			{
				Vector3(0.25f, 0.25f, 0.25f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(20.5f, 0.125f, 33.0f)
			},
			{
				Vector3(0.25f, 0.25f, 0.25f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(1.5f, 0.125f, 67.0f)
			},
			//{
			//	Vector3(0.25f, 0.25f, 0.25f),
			//	Vector3(0.0f, 0.0f, 0.0f),
			//	Vector3(-13.5f, 0.125f, 25.0f)
			//},
			//{
			//	Vector3(0.25f, 0.25f, 0.25f),
			//	Vector3(0.0f, 0.0f, 0.0f),
			//	Vector3(20.f, 0.125f, 51.0f)
			//},
		};
	}

	//敵の作成
	void GameStage::CreateBoss() {
		auto MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap1");
		MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap3");
		vector<Vector3> Vec3 = {
			{
				Vector3(2.5f, 2.5f, 2.5f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0, 0.75f, 80.0f)
			},
		};
		//セルマップを変更する敵
		auto BossPtr = AddGameObject<GameBoss>(MapPtr, Vec3[0], Vec3[1], Vec3[2], 30);
		SetSharedGameObject(L"AREABOSS", BossPtr);
		BossPtr->SetDrawActive(false);
		BossPtr->SetUpdateActive(false);
	}
	void GameStage::CreateHPUI() {
		//HPUIの動かす
		auto HPUIPtr = AddGameObject<HPSprite>(true, Vector2(300, 600), Vector3(-480, 340, 1), L"BULE_TX");
		HPUIPtr->SetDrawLayer(2);
		SetSharedGameObject(L"HPUI", HPUIPtr);
	}

	void GameStage::CreateUI() {
		//auto RemaingBulletPtr =
		//	AddGameObject<RemaingBullet>(true, Vector2(190, 256), Vector3(490, -350, 1), L"BULLET_REMAINING_TX");
		//RemaingBulletPtr->SetDrawLayer(1);

		//体力のUI
		AddGameObject<Sprite>(true, Vector2(350, 460), Vector3(500, -270, 1), L"FRAME_TX");
		AddGameObject<Sprite>(true, Vector2(300, 600), Vector3(-480, 340, 1), L"RED_TX");
		//弾の画像
		AddGameObject<BulletTypeSp>(true, Vector2(256, 256), Vector3(510, -260, 1), L"EMPTY_TX");
		//auto BulletEmptyPtr =
		//	AddGameObject<Sprite>(true, Vector2(190, 256), Vector3(490, -350, 1), L"BULLET_EMPTY_TX");
		//BulletEmptyPtr->SetDrawLayer(0);
		//auto BulletEramePtr =
		//	AddGameObject<Sprite>(true, Vector2(190, 256), Vector3(490, -350, 1), L"BULLET_FRAME_TX");
		//BulletEramePtr->SetDrawLayer(2);

	}


	void GameStage::CreateEffectBoost()
	{
		//SetSharedGameObject(L"EB", AddGameObject<EffectBoost>(Vector3(0.0f, -200.0f, 0.0f)));
		//auto Boost=AddGameObject<BoosterSprite>(true, Vector3(-520, 360, 1), Vector2(128,128), L"Boost_TX");
		//シェア配列にボックスを追加,
		//SetSharedGameObject(L"Boost", Boost);
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto EPos = ScenePtr->EnemyPos;
		//	AddGameObject<BoosterSprite>(true, Vector2(100, 100), Vector3(EPos.x, EPos.y, EPos.z), L"Boost_TX");

		if (ScenePtr->DestroyFLG) {
			AddGameObject<ExplosionSprite>(true, Vector2(100, 100), Vector3(EPos.x, EPos.z, 0.0f), L"Explosion_TX");
		}
	}

	void GameStage::CreateLaserBall()
	{
		auto PtrLaserEffect = GetSharedGameObject<LaserEffect>(L"LaserEffect", false);
		auto LaserPos = PtrLaserEffect->GetComponent<Transform>()->GetWorldPosition();
		auto LaserBall = AddGameObject<LaserBallEffect>(true, Vector2(3, 3), Vector3(LaserPos), L"LASERBALL_TX");
		SetSharedGameObject(L"LBall", LaserBall);
	}

	void GameStage::CreateLaserParticle()
	{
		auto PtrLaserEffect = AddGameObject<LaserEffect>();
		auto PtrEnemyLaserEffect = AddGameObject<EnemyLaserEffect>();
		auto PtrLaserRingEffect = AddGameObject<LaserRingEffect>();
		//シェア配列に追加
		SetSharedGameObject(L"LaserEffect", PtrLaserEffect);
		SetSharedGameObject(L"EnemyLaserEffect", PtrEnemyLaserEffect);
		SetSharedGameObject(L"LaserRingEffect", PtrLaserRingEffect);
		//エフェクトはZバッファを使用する
		//GetParticleManager()->SetZBufferUse(true);
	}

	//GameClear時のスプライト作成/6/23///////////////////////////////////////////////////////////////////////////////////
	void GameStage::CreateStageClear()
	{

		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		if (ScenePtr->GameClear)
		{
			auto ClearPtr = AddGameObject<StageClearSprite>(true, Vector2(900, 900), Vector3(-130, 220, 1), L"CLEAR_TX");
			ClearPtr->SetDrawLayer(5);
			SetSharedGameObject(L"STAGECLEARTX", ClearPtr);


			auto NextPtr = AddGameObject<NextSprite>(true, Vector2(350, 400), Vector3(400, -200, 1), L"NEXT_TX");
			NextPtr->SetDrawLayer(5);
			SetSharedGameObject(L"NEXTTX", NextPtr);

			auto TitlePtr = AddGameObject<TitleSprite>(true, Vector2(350, 400), Vector3(400, -300, 1), L"GOTITLE_TX");
			TitlePtr->SetDrawLayer(5);
			SetSharedGameObject(L"GOTITLETX", TitlePtr);

			auto CursorPtr = AddGameObject<CursorSprite>(true, Vector2(350, 400), Vector3(400, -200, 1), L"CURSOR_TX");
			CursorPtr->SetDrawLayer(4);
			SetSharedGameObject(L"CURSORTX", CursorPtr);
			//青い線
			auto BGSCPtr = AddGameObject<BGSCSprite>(true, Vector2(850, 800), Vector3(-250, 0, 1), L"STAGECLEARBG_TX");
			CursorPtr->SetDrawLayer(3);
			SetSharedGameObject(L"BGSCTX", BGSCPtr);
			BGSCPtr->SetAlphaActive(true);
			BGSCPtr->SetDrawActive(false);



			auto ScorePtr = AddGameObject<ScoreSprite>(true, Vector2(350, 250), Vector3(-200, -50, 1), L"SCORE_TX");
			ScorePtr->SetDrawLayer(5);
			SetSharedGameObject(L"SCORETX", ScorePtr);



			auto RankSPtr = AddGameObject<RankSSprite>(true, Vector2(350, 250), Vector3(0, -200, 1), L"S_TX");
			RankSPtr->SetDrawLayer(5);
			SetSharedGameObject(L"STX", RankSPtr);



			auto RankPtr = AddGameObject<RankSprite>(true, Vector2(300, 250), Vector3(-200, -200, 1), L"RANK_TX");
			RankPtr->SetDrawLayer(5);
			SetSharedGameObject(L"RANKTX", RankPtr);
		}

	}

	//ゲームオーバー時のスプライト作成/6/23////////////////////////////////////////////////////////////////////////
	void GameStage::CreateGameOver()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		if (ScenePtr->GameOver)
		{
			auto GameOverPtr = AddGameObject<GameOverSprite>(true, Vector2(1000, 900), Vector3(0, 100, 1), L"GAMEOVERLOGO_TX");
			GameOverPtr->SetDrawLayer(5);
			SetSharedGameObject(L"GAMEOVERTX", GameOverPtr);


			auto RetryPtr = AddGameObject<RetrySprite>(true, Vector2(400, 400), Vector3(0, -150, 1), L"RETRYBUTTON_TX");
			RetryPtr->SetDrawLayer(5);
			SetSharedGameObject(L"RETRYTX", RetryPtr);


			auto TitlePtr = AddGameObject<TitleSprite>(true, Vector2(400, 400), Vector3(0, -270, 1), L"GOTITLE_TX");
			TitlePtr->SetDrawLayer(5);
			SetSharedGameObject(L"GOTITLETX", TitlePtr);

			auto CursorPtr = AddGameObject<CursorSprite>(true, Vector2(350, 400), Vector3(0, -150, 1), L"CURSOR_TX");
			CursorPtr->SetDrawLayer(4);
			SetSharedGameObject(L"CURSORTX", CursorPtr);
		}
		/*auto CursorPos = CursorPtr->GetComponent<Transform>()->GetPosition();
		auto RetryPos = RetryPtr->GetComponent<Transform>()->GetPosition();*/

	}


	//固定のボックスの作成
	void GameStage::CreateSpawner() {
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			{
				Vector3(1.0f, 1.0f, 1.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, 0.0f, 5.0f)
			},
		};

		//ボックスのグループを作成
		auto TSGroup = CreateSharedObjectGroup(L"TS");
		//オブジェクトの作成
		for (auto v : Vec) {
			auto TopPtr = AddGameObject<TopSpawner>(v[0], v[1], v[2]);
			//ボックスのグループに追加
			TSGroup->IntoGroup(TopPtr);
			SetSharedGameObject(L"Top", TopPtr);
		}




		//配列の初期化
		vector< vector<Vector3> > Vec2 = {
			{
				Vector3(1.0f, 1.0f, 1.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f,0.0f, -5.0f)
			},
		};

		//ボックスのグループを作成
		auto USGroup = CreateSharedObjectGroup(L"US");
		//オブジェクトの作成
		for (auto v : Vec2) {
			auto UnderPtr = AddGameObject<UnderSpawner>(v[0], v[1], v[2]);
			//ボックスのグループに追加
			USGroup->IntoGroup(UnderPtr);
			SetSharedGameObject(L"Under", UnderPtr);
		}



		//配列の初期化
		vector< vector<Vector3> > Vec3 = {
			{
				Vector3(1.0f, 1.0f, 1.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(5.0f, 0.0f, 0.0f)
			},
		};

		//ボックスのグループを作成
		auto RSGroup = CreateSharedObjectGroup(L"RS");
		//オブジェクトの作成
		for (auto v : Vec3) {
			auto RightPtr = AddGameObject<RightSpawner>(v[0], v[1], v[2]);
			//ボックスのグループに追加
			RSGroup->IntoGroup(RightPtr);
			SetSharedGameObject(L"Right", RightPtr);
		}


		//配列の初期化
		vector< vector<Vector3> > Vec4 = {
			{
				Vector3(1.0f, 1.0f, 1.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(-5.0f, 0.0f, 0.0f)
			},
			/*{
			Vector3(1.0f, 0.5f, 40.0f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(-9.5f, 0.25f, 20.0f)
			},

			{
			Vector3(20.0f, 0.5f, 1.0f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, 0.25f, 0.5f)
			},

			{
			Vector3(20.0f, 0.5f, 1.0f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, 0.25f, 39.5f)
			},*/
		};

		//ボックスのグループを作成
		auto LSGroup = CreateSharedObjectGroup(L"LS");
		//オブジェクトの作成
		for (auto v : Vec4) {
			auto LeftPtr = AddGameObject<LeftSpawner>(v[0], v[1], v[2]);
			//ボックスのグループに追加
			USGroup->IntoGroup(LeftPtr);
			SetSharedGameObject(L"Left", LeftPtr);
		}



		//最初の2つのセルマップへのボックスのコスト設定
		SetCellMapCost(L"CellMap");
		//奥のセルマップへのボックスのコスト設定
		SetCellMapCost(L"CellMap2");
	}


	//固定のボックスの作成
	void GameStage::CreateWarpBox() {
		//配列の初期化
		vector< vector<Vector3> > Vec5 = {
			{
				Vector3(50.0f, 1.0f, 1.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f, 0.0f, 38.5f)
			},
		};

		//ボックスのグループを作成
		auto TWBGroup = CreateSharedObjectGroup(L"TWB");
		//オブジェクトの作成
		for (auto v : Vec5) {
			auto TopPtr = AddGameObject<TopWarpBox>(v[0], v[1], v[2]);
			//ボックスのグループに追加
			TWBGroup->IntoGroup(TopPtr);
			SetSharedGameObject(L"TW", TopPtr);
		}




		//配列の初期化
		vector< vector<Vector3> > Vec6 = {
			{
				Vector3(50.0f, 1.0f, 1.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0.0f,0.0f, 1.0f)
			},
		};

		//ボックスのグループを作成
		auto UWBGroup = CreateSharedObjectGroup(L"UWB");
		//オブジェクトの作成
		for (auto v : Vec6) {
			auto UnderPtr = AddGameObject<UnderWarpBox>(v[0], v[1], v[2]);
			//ボックスのグループに追加
			UWBGroup->IntoGroup(UnderPtr);
			SetSharedGameObject(L"UW", UnderPtr);
		}



		//配列の初期化
		vector< vector<Vector3> > Vec7 = {
			{
				Vector3(1.0f, 1.0f, 55.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(9.5f, 0.0f, 10.0f)
			},
		};

		//ボックスのグループを作成
		auto RWBGroup = CreateSharedObjectGroup(L"RWB");
		//オブジェクトの作成
		for (auto v : Vec7) {
			auto RightPtr = AddGameObject<RightWarpBox>(v[0], v[1], v[2]);
			//ボックスのグループに追加
			RWBGroup->IntoGroup(RightPtr);
			SetSharedGameObject(L"RW", RightPtr);
		}


		//配列の初期化
		vector< vector<Vector3> > Vec8 = {
			{
				Vector3(1.0f, 1.0f, 55.0f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(-9.5f, 0.0f, 10.0f)
			},
		};

		//ボックスのグループを作成
		auto LWBGroup = CreateSharedObjectGroup(L"LWB");
		//オブジェクトの作成
		for (auto v : Vec8) {
			auto LeftPtr = AddGameObject<LeftWarpBox>(v[0], v[1], v[2]);
			//ボックスのグループに追加
			UWBGroup->IntoGroup(LeftPtr);
			SetSharedGameObject(L"LW", LeftPtr);
		}



		//最初の2つのセルマップへのボックスのコスト設定
		SetCellMapCost(L"CellMap");
		//奥のセルマップへのボックスのコスト設定
		SetCellMapCost(L"CellMap2");
	}


	//CSVマッピングによるステージ作成　2017.04.20 田代
	void GameStage::CreateRoad()
	{
		wstring MediaPath;
		//CSVファイルからゲームステージの選択
		App::GetApp()->GetDataDirectory(MediaPath); //+ L"GameStage\\";//csvファイルの入っているフォルダを指定
		MediaPath += L"GameStage\\";//Stage_csv							
		//シーンからゲームセレクトで選んだゲームステージの番号取得
		int i_StageNumber = App::GetApp()->GetScene<Scene>()->getStageNumber();
		//ステージ番号からステージの名前を取得
		wstring i_StageName = Util::IntToWStr(i_StageNumber);
		wstring FileName = MediaPath + L"Stage_";

		FileName += i_StageName + L".csv";

		//wstring FileName = MediaPath + L"Stage_1";//
		//FileName += i_StageName + L".csv";//Stage番号と拡張子の指定
		//FileName += L".csv";//Stage番号と拡張子の指定
		//wstring FileName = MediaPath + L"Stage_1.csv";

		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(FileName);
		//ファイルがあるかどうか
		if (!GameStageCsv.ReadCsv()) {
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません",
				FileName,
				L"選択された場所にdアクセスできません"
			);
		}
		const int iDataSizeRow = 0; //データが0行目から始まるように定数
		vector<wstring>StageMapVec; //ワーク用のベクター配列
		GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);
		//行、列の取得
		m_sRowSize = static_cast<size_t>(_wtoi(StageMapVec[0].c_str())); //Row=行
		m_sColSize = static_cast<size_t>(_wtoi(StageMapVec[1].c_str())); //Col=列
																		 //マップデータの配列作成
		m_MapDateVec = vector<vector<size_t>>(m_sRowSize, vector<size_t>(m_sColSize)); //列の数と行の数分
		m_MapDateVec2 = vector<vector<size_t>>(m_sRowSize, vector<size_t>(m_sColSize)); //列の数と行の数分
																						//配列の初期化
		for (size_t i = 0; i < m_sRowSize; i++) {
			for (size_t j = 0; j < m_sColSize; j++) {
				m_MapDateVec[i][j] = 0;	//0で初期化
				m_MapDateVec2[i][j] = 0;	//0で初期化
			}
		}
		//1行目からステージが定義されている
		const int iDataStartRow = 1;
		//(float)m_sRowSize / 2;
		if (m_sRowSize > 0) {
			for (size_t i = 0; i < m_sRowSize; i++) {
				GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec); //START + iだから最初は２が入る
				for (size_t j = 0; j < m_sColSize; j++) {
					const int iNum = _wtoi(StageMapVec[j].c_str()); //列から取得したwstringをintに
					float ihalf = i / 2;
					float jhalf = j / 2;
					//マップデータ配列に格納
					m_MapDateVec[i][j] = iNum;
					//マップデータ配列に格納
					m_MapDateVec2[i][j] = iNum;
					//配置されるオブジェクトの基準のスケール
					float ObjectScale = 1.0f;
					Vector3 RedCircleScale(0.0f, 0.0f, 0.0f);
					//**********************//
					//CSV用オブジェクトScale//
					//**********************//
					//基準スケール
					Vector3 Scale(ObjectScale, ObjectScale, ObjectScale);
					Vector3 EnemyScale(0.25, 0.25, 0.25);
					//Cursorスケール
					Vector3 Scale_Cursor(ObjectScale*1.0, ObjectScale*0.1, ObjectScale*1.0);
					//*************************//
					//CSV用オブジェクトRotation//
					//*************************//
					Vector3 Rotation(0.0f, 0.0f, 0.0f);
					Vector3 FloarRot((90.0f / 180.0f * XM_PI), 0.0f, 0.0f);//床用のRotation
																		   //列の半分の数
					float ColHalf = m_sColSize / 2;
					//行の半分の数
					float RowHalf = m_sRowSize / 2;
					//ステージの真ん中をX、Z軸０にしたいのでポジションのXを行の半分の値分引いて、Zを列の半分の値でプラスしてあげる
					//**************//
					//基準ポジション//
					//*************//

					//Spawnerの
					auto TopPtr = GetSharedGameObject<TopSpawner>(L"Top");
					auto TopTrans = TopPtr->GetComponent<Transform>();
					TopPos = TopTrans->GetPosition();

					auto UnderPtr = GetSharedGameObject<UnderSpawner>(L"Under");
					auto UnderTrans = UnderPtr->GetComponent<Transform>();
					UnderPos = UnderTrans->GetPosition();

					auto RightPtr = GetSharedGameObject<RightSpawner>(L"Right");
					auto RightTrans = RightPtr->GetComponent<Transform>();
					RightPos = RightTrans->GetPosition();

					auto LeftPtr = GetSharedGameObject<LeftSpawner>(L"Left");
					auto LeftTrans = LeftPtr->GetComponent<Transform>();
					LeftPos = LeftTrans->GetPosition();

					//外壁の
					auto TopWPtr = GetSharedGameObject<TopWarpBox>(L"TW");
					auto TopWTrans = TopWPtr->GetComponent<Transform>();
					TopWPos = TopWTrans->GetPosition();

					auto UnderWPtr = GetSharedGameObject<UnderWarpBox>(L"UW");
					auto UnderWTrans = UnderWPtr->GetComponent<Transform>();
					UnderWPos = UnderWTrans->GetPosition();

					auto RightWPtr = GetSharedGameObject<RightWarpBox>(L"RW");
					auto RightWTrans = RightWPtr->GetComponent<Transform>();
					RightWPos = RightWTrans->GetPosition();

					auto LeftWPtr = GetSharedGameObject<LeftWarpBox>(L"LW");
					auto LeftWTrans = LeftWPtr->GetComponent<Transform>();
					LeftWPos = LeftWTrans->GetPosition();

					auto PlayerPtr = GetSharedGameObject<Player>(L"Player");
					auto PlayerTransform = PlayerPtr->GetComponent<Transform>();
					auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();

					CornerFlg = false;

					//右上にいるときの敵の出現位置
					if (TopPos.z >= TopWPos.z&&RightPos.x >= RightWPos.x)
					{
						CornerFlg = true;
						SpawnPos = Vector3(static_cast<float>(j) - ColHalf - 5.0f, 0.125f, -static_cast<float>(i) - ColHalf - 20.0f);
					}
					//左上にいるときの敵の出現位置
					if (TopPos.z >= TopWPos.z&&LeftPos.x <= LeftWPos.x)
					{
						CornerFlg = true;
						SpawnPos = Vector3(static_cast<float>(j) + ColHalf + 5.0f, 0.125f, -static_cast<float>(i) - ColHalf - 20.0f);
					}
					//右下にいるときの敵の出現位置
					if (UnderPos.z <= UnderWPos.z&&RightPos.x >= RightWPos.x)
					{
						CornerFlg = true;
						SpawnPos = Vector3(static_cast<float>(j) - ColHalf - 5.0f, 0.125f, -static_cast<float>(i) + ColHalf + 20.0f);
					}
					//左下にいるとときの敵の出現位置
					if (UnderPos.z <= UnderWPos.z&&LeftPos.x <= LeftWPos.x)
					{
						CornerFlg = true;
						SpawnPos = Vector3(static_cast<float>(j) + ColHalf + 5.0f, 0.125f, -static_cast<float>(i) + ColHalf + 20.0f);
					}


					if (TopPos.z >= TopWPos.z && !CornerFlg) {
						SpawnPos = Vector3(static_cast<float>(j) - ColHalf, 0.125f, -static_cast<float>(i) - ColHalf - 10);
					}
					if (UnderPos.z <= UnderWPos.z && !CornerFlg)
					{
						SpawnPos = Vector3(static_cast<float>(j) - ColHalf, 0.125f, -static_cast<float>(i) + ColHalf + 10);
					}
					if (RightPos.x >= RightWPos.x && !CornerFlg)
					{
						SpawnPos = Vector3(static_cast<float>(j) - ColHalf - 10, 0.125f, -static_cast<float>(i) + PlayerPos.z);
					}
					if (LeftPos.x <= LeftWPos.x && !CornerFlg) {
						SpawnPos = Vector3(static_cast<float>(j) + ColHalf + 10, 0.125f, -static_cast<float>(i) + PlayerPos.z);
					}


					//カーソル用Position
					Vector3 Pos_Cursor(static_cast<float>(j) - ColHalf, -0.5f, -static_cast<float>(i) + RowHalf);
					//床用のポジション
					Vector3 Pos_Froar(static_cast<float>(j) - ColHalf, -0.5f, -static_cast<float>(i) + RowHalf);
					auto MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap1");
					switch (iNum)
					{
					case DataID::BLOCK:
						//auto PtrBG2 = AddGameObject<GameObject>();
						//auto PtrTransBG2 = PtrBG2->GetComponent<Transform>();
						//Quaternion Qt;
						//Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
						//PtrTransBG2->SetScale(Scale);
						//PtrTransBG2->SetQuaternion(Qt);
						//PtrTransBG2->SetPosition(Pos);
						////・ｽ`・ｽ・ｽR・ｽ・ｽ・ｽ|・ｽ[・ｽl・ｽ・ｽ・ｽg・ｽﾌ追会ｿｽ
						//auto DrawComp2 = PtrBG2->AddComponent<PNTStaticDraw>();
						////・ｽ`・ｽ・ｽR・ｽ・ｽ・ｽ|・ｽ[・ｽl・ｽ・ｽ・ｽg・ｽﾉ形・ｽ・ｽi・ｽ・ｽ・ｽb・ｽV・ｽ・ｽ・ｽj・ｽ・ｽﾝ抵ｿｽ
						//DrawComp2->SetMeshResource(L"DEFAULT_SQUARE");
						////・ｽ`・ｽ・ｽR・ｽ・ｽ・ｽ|・ｽ[・ｽl・ｽ・ｽ・ｽg・ｽe・ｽN・ｽX・ｽ`・ｽ・ｽ・ｽﾌ設抵ｿｽ
						//DrawComp2->SetTextureResource(L"BGTILE_TX");
						////PtrBG2->SetAlphaActive(true);
						break;

					case DataID::ENEMY:
						AddGameObject<WeakEnemy>(MapPtr, EnemyScale, Rotation, SpawnPos, L"Enemy1", 1);
						break;
					case DataID::AENEMY:
						AddGameObject<AimingEnemys>(MapPtr, EnemyScale, Rotation, SpawnPos, L"Enemy3", 2);
						break;
					case DataID::TELEPORT:
						AddGameObject<TeleportEnemys>(MapPtr, EnemyScale, Rotation, SpawnPos, L"Enemy2", 3);
						break;
					}
				}
			}
		}
		//--------------------------------------------------------
		//最適化,
		//********************************************************
		////ステージデータのコピーを作成,
		auto & WorkVec = m_MapDateVec;
		auto & WorkVec2 = m_MapDateVec2;
	}

	void GameStage::SpawnEnemy() {

		auto GetParamEnenmy = App::GetApp()->GetScene<Scene>()->GetGameParametors();

					


		auto MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap1");
		Vector3 EnemyScale(0.25, 0.25, 0.25);

		AddGameObject<WeakEnemy>(MapPtr, EnemyScale, Vector3(0,0,0), SpawnPos, L"Enemy1", 1);
		AddGameObject<AimingEnemys>(MapPtr, EnemyScale, Vector3(0, 0, 0), SpawnPos, L"Enemy3", 1);
		AddGameObject<TeleportEnemys>(MapPtr, EnemyScale, Vector3(0, 0, 0), SpawnPos, L"Enemy2", 1);

	}

		////CSVマッピングによるステージ作成　2017.04.20 田代
		//void GameStage::CreateRoad()
		//{
		//	wstring MediaPath;
		//	//CSVファイルからゲームステージの選択
		//	App::GetApp()->GetDataDirectory(MediaPath); //+ L"GameStage\\";//csvファイルの入っているフォルダを指定
		//	MediaPath += L"GameStage\\";//Stage_csv
		//								//シーンからゲームセレクトで選んだゲームステージの番号取得
		//								//	int i_StageNumber = App::GetApp()->GetScene<Scene>()->getStageNumber();
		//								//ステージ番号からステージの名前を取得
		//								//	wstring i_StageName = Util::IntToWStr(i_StageNumber);
		//	wstring FileName = MediaPath + L"Stage_1";//
		//											  //FileName += i_StageName + L".csv";//Stage番号と拡張子の指定
		//	FileName += L".csv";//Stage番号と拡張子の指定
		//						//wstring FileName = MediaPath + L"Stage_1.csv";

		//						//ローカル上にCSVファイルクラスの作成
		//	CsvFile GameStageCsv(FileName);
		//	//ファイルがあるかどうか
		//	if (!GameStageCsv.ReadCsv()) {
		//		//ファイルが存在しなかった
		//		//初期化失敗
		//		throw BaseException(
		//			L"CSVファイルがありません",
		//			FileName,
		//			L"選択された場所にdアクセスできません"
		//		);
		//	}
		//	const int iDataSizeRow = 0; //データが0行目から始まるように定数
		//	vector<wstring>StageMapVec; //ワーク用のベクター配列
		//	GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);
		//	//行、列の取得
		//	m_sRowSize = static_cast<size_t>(_wtoi(StageMapVec[0].c_str())); //Row=行
		//	m_sColSize = static_cast<size_t>(_wtoi(StageMapVec[1].c_str())); //Col=列
		//																	 //マップデータの配列作成
		//	m_MapDateVec = vector<vector<size_t>>(m_sRowSize, vector<size_t>(m_sColSize)); //列の数と行の数分
		//	m_MapDateVec2 = vector<vector<size_t>>(m_sRowSize, vector<size_t>(m_sColSize)); //列の数と行の数分
		//																					//配列の初期化
		//	for (size_t i = 0; i < m_sRowSize; i++) {
		//		for (size_t j = 0; j < m_sColSize; j++) {
		//			m_MapDateVec[i][j] = 0;	//0で初期化
		//			m_MapDateVec2[i][j] = 0;	//0で初期化
		//		}
		//	}
		//	//1行目からステージが定義されている
		//	const int iDataStartRow = 1;
		//	//(float)m_sRowSize / 2;
		//	if (m_sRowSize > 0) {
		//		for (size_t i = 0; i < m_sRowSize; i++) {
		//			GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec); //START + iだから最初は２が入る
		//			for (size_t j = 0; j < m_sColSize; j++) {
		//				const int iNum = _wtoi(StageMapVec[j].c_str()); //列から取得したwstringをintに
		//				float ihalf = i / 2;
		//				float jhalf = j / 2;
		//				//マップデータ配列に格納
		//				m_MapDateVec[i][j] = iNum;
		//				//マップデータ配列に格納
		//				m_MapDateVec2[i][j] = iNum;
		//				//配置されるオブジェクトの基準のスケール
		//				float ObjectScale = 10.0f;
		//				Vector3 RedCircleScale(0.0f, 0.0f, 0.0f);
		//				//**********************//
		//				//CSV用オブジェクトScale//
		//				//**********************//
		//				//基準スケール
		//				Vector3 Scale(ObjectScale, ObjectScale, 0.1);
		//				//Cursorスケール
		//				Vector3 Scale_Cursor(ObjectScale*1.0, ObjectScale*0.1, ObjectScale*1.0);
		//				//*************************//
		//				//CSV用オブジェクトRotation//
		//				//*************************//
		//				Vector3 Rotation(0.0f, 0.0f, 0.0f);
		//				Vector3 FloarRot((90.0f / 180.0f * XM_PI), 0.0f, 0.0f);//床用のRotation
		//																	   //列の半分の数
		//				float ColHalf = m_sColSize / 2;
		//				//行の半分の数
		//				float RowHalf = m_sRowSize / 2;
		//				//ステージの真ん中をX、Z軸０にしたいのでポジションのXを行の半分の値分引いて、Zを列の半分の値でプラスしてあげる
		//				//**************//
		//				//基準ポジション//
		//				//*************//
		//				Vector3 Pos(static_cast<float>(j)*10 - ColHalf, -10.0f, -static_cast<float>(i) * 10 + RowHalf);
		//				//カーソル用Position
		//				Vector3 Pos_Cursor(static_cast<float>(j) - ColHalf, -0.5f, -static_cast<float>(i) + RowHalf);
		//				//床用のポジション
		//				Vector3 Pos_Froar(static_cast<float>(j) - ColHalf, -0.5f, -static_cast<float>(i) + RowHalf);
		//				switch (iNum)
		//				{
		//				case DataID::BLOCK:	
		//					//auto PtrBG2 = AddGameObject<GameObject>();
		//					//auto PtrTransBG2 = PtrBG2->GetComponent<Transform>();
		//					//Quaternion Qt;
		//					//Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
		//					//PtrTransBG2->SetScale(Scale);
		//					//PtrTransBG2->SetQuaternion(Qt);
		//					//PtrTransBG2->SetPosition(Pos);
		//					////・ｽ`・ｽ・ｽR・ｽ・ｽ・ｽ|・ｽ[・ｽl・ｽ・ｽ・ｽg・ｽﾌ追会ｿｽ
		//					//auto DrawComp2 = PtrBG2->AddComponent<PNTStaticDraw>();
		//					////・ｽ`・ｽ・ｽR・ｽ・ｽ・ｽ|・ｽ[・ｽl・ｽ・ｽ・ｽg・ｽﾉ形・ｽ・ｽi・ｽ・ｽ・ｽb・ｽV・ｽ・ｽ・ｽj・ｽ・ｽﾝ抵ｿｽ
		//					//DrawComp2->SetMeshResource(L"DEFAULT_SQUARE");
		//					////・ｽ`・ｽ・ｽR・ｽ・ｽ・ｽ|・ｽ[・ｽl・ｽ・ｽ・ｽg・ｽe・ｽN・ｽX・ｽ`・ｽ・ｽ・ｽﾌ設抵ｿｽ
		//					//DrawComp2->SetTextureResource(L"BGTILE_TX");
		//					////PtrBG2->SetAlphaActive(true);
		//					break;
		//				}
		//			}
		//		}
		//	}
		//	//--------------------------------------------------------
		//	//最適化,
		//	//********************************************************
		//	////ステージデータのコピーを作成,
		//	auto & WorkVec = m_MapDateVec;
		//	auto & WorkVec2 = m_MapDateVec2;
		//}


	//回復アイテム
	void GameStage::CreateRecovery() {
		AddGameObject<Recovery>(Vector3(1, 1, 1), Vector3(0, 0, 0), Vector3(-5, -5, 0), L"CURE_TX");
	}

		//敵（仮）の作成
	void GameStage::CreateEnemy1() {
		auto MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap1");
		vector< vector<Vector3> > Vec1 = {
			{
				Vector3(0.40f, 0.40f, 0.40f),
				Vector3(180.0f, 0.0f, 0.0f),
				Vector3(-2.5f, 0.125f, 33.0f)
			},
		};
		//オブジェクトの作成
		for (auto v : Vec1) {
			//AddGameObject<Enemy1>(v[0], v[1], v[2]);
		}
	}

	void GameStage::CreateEnemy2() {
		auto MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap1");
		vector< vector<Vector3> > Vec1 = {
			{
				Vector3(0.40f, 0.40f, 0.40f),
				Vector3(180.0f, 0.0f, 0.0f),
				Vector3(5.0f, 0.125f, 33.0f)
			},
		};
		//オブジェクトの作成
		for (auto v : Vec1) {
			//AddGameObject<Enemy2>(v[0], v[1], v[2]);
		}
	}

		////・ｽX・ｽ|・ｽi・ｽ[・ｽﾌ撰ｿｽ・ｽ・
		//void GameStage::CreateSpawner() {
		//	AddGameObject<Spawner>(Vector3(1.0f,1.0f,1.0f),Vector3(0,0,0), Vector3(-6, 0.0f, 25.0f),
		//		100,Color4(1.0f,0,0,1.0f));
		//	AddGameObject<Spawner>(Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(0, 0.0f, 20.0f),
		//		200, Color4(0.0f, 1.0, 0, 1.0f));
		//	AddGameObject<Spawner>(Vector3(1.0f, 1.0f, 1.0f), Vector3(0, 0, 0), Vector3(8, 0.0f, 20.0f),
		//		300, Color4(0.0f, 0, 1.0, 1.0f));
		//}

	void GameStage::CreateTutoreal() {
		auto Ptr = AddGameObject<Sprite>(true, Vector2(256, 256), Vector3(-500, -128, 1), L"OPERATOR_TX");
		SetSharedGameObject(L"OPELETOR", Ptr);
		Ptr = AddGameObject<Sprite>(true, Vector2(1024, 512), Vector3(128, -128, 1), L"TUTOREAL1_TX");
		SetSharedGameObject(L"TUTORIAL", Ptr);
	}

	void GameStage::CreateMeteoRite() {
		//AddGameObject<MeteoRite>(Vector3(1,1,1),Vector3(0,0,0),Vector3(0,0,10), L"METEORITE_01_MESH");
	}


	void GameStage::CreateResultSprite() {

		auto Ptr = AddGameObject<Sprite>(true, Vector2(1280, 800), Vector3(-0, 0, 1), L"STAGECLEARBG_TX");
		Ptr->SetDrawActive(false);
		SetSharedGameObject(L"SCBGSP", Ptr);

		Ptr = AddGameObject<Sprite>(true, Vector2(1280, 800), Vector3(-0, 0, 1), L"STAGECLEARLOGO_TX");
		Ptr->SetDrawActive(false);
		SetSharedGameObject(L"SCLGSP", Ptr);

		Ptr = AddGameObject<Sprite>(true, Vector2(1280, 800), Vector3(-0, 0, 1), L"GAMEOVERBG_TX");
		Ptr->SetDrawActive(false);
		SetSharedGameObject(L"GOBGSP", Ptr);

		Ptr = AddGameObject<Sprite>(true, Vector2(1280, 800), Vector3(-0, 0, 1), L"GAMEOVERLOGO_TX");
		Ptr->SetDrawActive(false);
		SetSharedGameObject(L"GOLGSP", Ptr);
		//セレクトの画像
		auto SpriteGroup = CreateSharedObjectGroup(L"SelectActionGroup");
		//タイトル
		auto SelectPtr = AddGameObject<Sprite>(true, Vector2(256, 256), Vector3(-300, -200, 1), L"TITLEBUTTON_TX");
		SelectPtr->SetDrawActive(false);
		SpriteGroup->IntoGroup(SelectPtr);
		//ネクスト
		SelectPtr = AddGameObject<Sprite>(true, Vector2(256, 256), Vector3(300, -200, 1), L"NEXTBUTTON_TX");
		SelectPtr->SetDrawActive(false);
		SpriteGroup->IntoGroup(SelectPtr);
		//リトライ
		SelectPtr = AddGameObject<Sprite>(true, Vector2(256, 256), Vector3(300, -200, 1), L"RETRYBUTTON_TX");
		SelectPtr->SetDrawActive(false);
		SpriteGroup->IntoGroup(SelectPtr);
	}

	void GameStage::ClearAction()
	{
	/*	auto BGPtr = GetSharedGameObject<Sprite>(L"SCBGSP", false);
		auto LGPtr = GetSharedGameObject<Sprite>(L"SCLGSP", false);
		auto ElapsedTime = App::GetApp()->GetElapsedTime();

		auto sp = GetSharedObjectGroup(L"SelectActionGroup");

		if (App::GetApp()->GetScene<Scene>()->GetGameParametors().m_BossHP <= 0 && m_BossSpawn) {
			m_EventActionTime += ElapsedTime;
			if (m_EventActionTime >= 0.5f) {
				m_flgnum++;
				m_EventActionTime = 0.0f;
			}
			switch (m_flgnum)
			{
			case 1:
				BGPtr->SetDrawActive(true);
				break;
			case 2:
				LGPtr->SetDrawActive(true);
				break;
			case 5:
				sp->at(0)->SetDrawActive(true);
				sp->at(1)->SetDrawActive(true);
				break;
			default:
				break;
			}
		}*/
	}

	void GameStage::GameOverAction() {
		//auto PLPtr = GetSharedGameObject<Player>(L"Player", false);
		//auto BGPtr = GetSharedGameObject<Sprite>(L"GOBGSP", false);
		//auto LGPtr = GetSharedGameObject<Sprite>(L"GOLGSP", false);
		//auto ElapsedTime = App::GetApp()->GetElapsedTime();

		//auto sp = GetSharedObjectGroup(L"SelectActionGroup");

		//if (App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PlayerHP <= 0) {
		//	m_EventActionTime += ElapsedTime;
		//	if (m_EventActionTime >= 0.5f) {
		//		m_flgnum++;
		//		m_EventActionTime = 0.0f;
		//	}
		//	switch (m_flgnum)
		//	{
		//	case 1:
		//		BGPtr->SetDrawActive(true);
		//		break;
		//	case 2:
		//		LGPtr->SetDrawActive(true);
		//		break;
		//	case 5:
		//		sp->at(0)->SetDrawActive(true);
		//		sp->at(2)->SetDrawActive(true);
		//		break;

		//	default:
		//		break;
		//	}
		//}

	}



	void GameStage::CreateSS5Anime() {
		wstring DataDir;
		//サンプルのためアセットディレクトリを取得
		App::GetApp()->GetDataDirectory(DataDir);

		auto Ptr = AddGameObject<SSAnime>(DataDir + L"EffectWarning\\", L"warning.ssae", L"anime_1", Vector3(0, 0, 0), Vector3(20, 20, 1.0f));
		SetSharedGameObject(L"SSANIME", Ptr);
	}


	void GameStage::OnCreate() {
		try {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			ScenePtr->m_StageNumber = 0;
			ScenePtr->DestroyCount = 0;
			//スコアアップアイテムのグループ
			CreateSharedObjectGroup(L"Boost");
			CreateSharedObjectGroup(L"Enemys");
			CreateSharedObjectGroup(L"Laser");
			CreateSharedObjectGroup(L"EnemyLaser");

			//ビューとライトの作成
			CreateViewLight();
			//プレートの作成
			CreatePlate();
			//セルマップの作成
			CreateStageCellMap();
			//固定のボックスの作成
			CreateFixedBox();
			//プレーヤーの作成
			CreatePlayer();
			//敵の作成
			CreateEnemy();
			//HP
			CreateHPUI();
			//UIの作成
			CreateUI();
			//隕石
			CreateMeteoRite();
			//回復
			CreateRecovery();
			CreateSpawner();

			//ボス作成
			CreateBoss();

			//ステージクリア時に出すテクスチャの作成6/23
			//CreateStageClear();
			//ゲームオーバー時に出すテクスチャの作成6/23
			//CreateGameOver();

			CreateWarpBox();

			CreateTutoreal();
			//レーザーのParticleの作成
			CreateLaserParticle();

			////CSVマップ(田代)
			//CreateRoad();
			//effectBoostの作成
			CreateEffectBoost();
			CreateLaserBall();


			//敵（仮）の作成(生田目)
			CreateEnemy1();
			CreateEnemy2();
			//・ｽX・ｽ|・ｽi・ｽ[・ｽﾌ撰ｿｽ・ｽ・ｽ
			//CreateSpawner();
			//
			CreateResultSprite();
		}
		catch (...) {
			throw;
		}
	}

	void GameStage::OnUpdate() {

		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		auto Ope = Stage::GetSharedGameObject<Sprite>(L"OPELETOR", true);
		auto Tuto = Stage::GetSharedGameObject<Sprite>(L"TUTORIAL", true);
		auto PLPtr = Stage::GetSharedGameObject<Player>(L"Player", true);
		auto LeftFun = Stage::GetSharedGameObject<CatchFunnel>(L"FUNNEL_L", true);
		auto RightFun = Stage::GetSharedGameObject<CatchFunnel>(L"FUNNEL_R", true);
		auto Boss = Stage::GetSharedGameObject<GameBoss>(L"AREABOSS", true);

		//チュートリアルのカウント
		switch (TutorealCount)
		{
		case 0:
			LeftFun->SetUpdateActive(false);
			RightFun->SetUpdateActive(false);
			break;
		case 1:
			PLPtr->SetUpdateActive(false);

			Tuto->GetComponent<PTSpriteDraw>()->SetTextureResource(L"TUTOREAL2_TX");
			break;
		case 2:
			Tuto->GetComponent<PTSpriteDraw>()->SetTextureResource(L"TUTOREAL3_TX");
			break;
		case 3:
			Tuto->GetComponent<PTSpriteDraw>()->SetTextureResource(L"TUTOREAL4_TX");
			break;
		case 4:
			Ope->SetDrawActive(false);
			Tuto->SetDrawActive(false);
			PLPtr->SetUpdateActive(true);
			LeftFun->SetUpdateActive(true);
			RightFun->SetUpdateActive(true);
			break;
		default:
			break;
		}


		//6/23
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();


		//auto ScenePtr = App::GetApp()->GetScene<Scene>();
		//デバックようにXボタン押したらゲームオーバーフラグをtrueにしている///6/23
		if (App::GetApp()->GetScene<Scene>()->GetGameParametors().m_BossHP <= 0)//CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_X)
		{
			PLPtr->SetUpdateActive(false);
			ScenePtr->GameClear = true;
		}
		else if (App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PlayerHP <= 0)//CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_X)
		{
			ScenePtr->GameOver = true;
		}

		if (ScenePtr->GameClear&&ScenePtr->flg == 0)
		{
			CreateStageClear();
			ScenePtr->flg = 1;
		}
		else if (ScenePtr->GameOver&&ScenePtr->flg == 0)
		{
			CreateGameOver();
			ScenePtr->flg = 1;
		}


		//
		if (TutorealCount >= 4 && ScenePtr->m_StageNumber == 0) {
			ScenePtr->setStageNumber(1);
			CreateRoad();
		}
		//敵を36体倒したら新しい敵を出す
		if (ScenePtr->DestroyCount >= 2 && ScenePtr->m_StageNumber == 1) {
			ScenePtr->setStageNumber(11);
			CreateRoad();
		}
		//敵を36体倒したら新しい敵を出す
		if (ScenePtr->DestroyCount >= 20 && ScenePtr->m_StageNumber == 11) {
			ScenePtr->setStageNumber(12);
			CreateRoad();
		}

		//敵を36体倒したら新しい敵を出す
		if (ScenePtr->DestroyCount >= 40 && ScenePtr->m_StageNumber == 12) {
			ScenePtr->setStageNumber(13);
			CreateRoad();
		}
		if (ScenePtr->DestroyCount >= 60 && ScenePtr->m_StageNumber == 13) {
			ScenePtr->setStageNumber(14);
		}
		else if (ScenePtr->m_StageNumber == 14) {
			UpDateBoss += ElapsedTime;
		}
		if (UpDateBoss >= 3 && CreateOnce2) {
			CreateOnce2 = false;
			CreateSS5Anime();
		}
		if (UpDateBoss >= 6 && CreateOnce) {
			auto Ptr = GetSharedGameObject<SSAnime>(L"SSANIME", false);
			Ptr->SetDrawActive(false);
			Ptr->SetUpdateActive(false);
			CreateOnce = false;
			Boss->SetUpdateActive(true);
			Boss->SetDrawActive(true);
			m_BossSpawn = true;
			PostEvent(0.0f, GetThis<GameStage>(), App::GetApp()->GetSceneInterface(), L"BossSpawn");
		}

		//
		ClearAction();
		//
		GameOverAction();

		//コントローラの取得
		//auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		///////////6/23//////////////////////////////////////////////////////////////////
		if (ScenePtr->GameOver )//|| App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PlayerHP <= 0)
		{
			auto RetryPtr = GetSharedGameObject<RetrySprite>(L"RETRYTX");
			auto RetryPos = RetryPtr->GetComponent<Transform>()->GetPosition();
			auto CursorPtr = GetSharedGameObject<CursorSprite>(L"CURSORTX");
			auto CursorPos = CursorPtr->GetComponent<Transform>()->GetPosition();

			//ゲームオーバー時にRetryを押したときの処理
			if (CursorPos == RetryPos&&CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A)
			{
								//各初期化
				ScenePtr->GameOver = false;
				ScenePtr->DrawTime = 0;
				ScenePtr->flg = 0;	
				ScenePtr->setStageNumber(1);

				PostEvent(0.5f, GetThis<GameStage>(), App::GetApp()->GetSceneInterface(), L"ToGame");
				//CreateRoad();
			}
		}


		//////////////////////////////////////////////////////////6/23//////////////////////////////////


		if (CntlVec[0].bConnected) {
			//Aボタンが押されたら.

			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
				TutorealCount += 1;
			}
		}
	}

	//--------------------------------------------------------------------------------------
		//	ゲームステージクラス実体
		//--------------------------------------------------------------------------------------


	//ビューとライトの作成
	void TitleStage::CreateViewLight() {

		auto PtrView = CreateView<SingleView>();
		////ビューのカメラの設定
		auto PtrMainCamera = ObjectFactory::Create<MainCamera>();
		PtrView->SetCamera(PtrMainCamera);
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}


	void TitleStage::CreateUI() {
		auto Ptr = AddGameObject<Sprite>(true, Vector2(1024, 512), Vector3(-0, 128, 0), L"TITLELOGO_TX");
		SetSharedGameObject(L"TITLELOGO",Ptr);
		Ptr = AddGameObject<Sprite>(true, Vector2(1280, 800), Vector3(32, -32, 0), L"TITLEPUSH_TX");
		SetSharedGameObject(L"TITLEPUSH", Ptr);
	
		Ptr = AddGameObject<Sprite>(true, Vector2(1280, 800), Vector3(-0, 0, 0), L"GALAXY_TX");
		Ptr->SetDrawLayer(-1);

		auto BPtr = AddGameObject<Fade>(L"W",L"IN");
		SetSharedGameObject(L"FADE",BPtr);
	}

	void TitleStage::CreateModel() {
		auto Ptr = AddGameObject<EmptyModel>(Vector3(1,1,1), Vector3(XM_PIDIV2, XM_PI,XM_PI), Vector3(0,1.5,0.5),L"Player");
		SetSharedGameObject(L"DEMOPL",Ptr);
		Ptr = AddGameObject<EmptyModel>(Vector3(1,1,1), Vector3(-XM_PIDIV2, XM_PI,0), Vector3(0,0,0),L"Catapult");
		SetSharedGameObject(L"DEMOCATAPULT",Ptr);
	}

	void TitleStage::CreateSplitSprite() {
		auto Ptr = AddGameObject<SplitSprite>(L"NUN_TX",true,Vector2(256,128),Vector2(0,0),1,3);

		SetSharedGameObject(L"NUMBER", Ptr);
		Ptr->SetDrawActive(false);
	}

	//void TitleStage::CreateSS5Anime() {
	//	wstring DataDir;
	//	//サンプルのためアセットディレクトリを取得
	//	App::GetApp()->GetDataDirectory(DataDir);

	//	AddGameObject<SSAnime>(DataDir + L"EffectWarning\\",L"warning.ssae",L"anime_1",Vector3(0,0,0),Vector3(20,20,1.0f));

	//}

	void TitleStage::OpeningEvent() {

		auto CataPtr = GetSharedGameObject<EmptyModel>(L"DEMOCATAPULT", false);
		auto Animation = CataPtr->GetComponent<PNTBoneModelDraw>();
		auto CataTrans = CataPtr->GetComponent<Transform>();
		auto PlayPtr = GetSharedGameObject<EmptyModel>(L"DEMOPL", false);
		auto PlayTrans = PlayPtr->GetComponent<Transform>();

		auto LogoPtr = GetSharedGameObject<Sprite>(L"TITLELOGO", false);
		auto PushPtr = GetSharedGameObject<Sprite>(L"TITLEPUSH", false);

		auto FadePtr = GetSharedGameObject<Fade>(L"FADE");

		auto NumPtr = GetSharedGameObject<SplitSprite>(L"NUMBER");

		auto ElapsedTime = App::GetApp()->GetElapsedTime();		

		m_EventTime += ElapsedTime;
		if (m_EventTime >= 1.0f) {
			m_EventCounter++;
			m_EventTime = 0.0f;
		}

		switch (m_EventCounter)
		{
		case 0:
			m_ClearSprite -= ElapsedTime;
			LogoPtr->GetComponent<PTSpriteDraw>()->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, m_ClearSprite));
			PushPtr->GetComponent<PTSpriteDraw>()->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, m_ClearSprite));
			NumPtr->GetComponent<PCTSpriteDraw>()->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, m_ClearSprite));
			break;
		case 1:
			if (Animation->GetCurrentAnimation() == L"Close") {
				CataPtr->GetComponent<MultiSoundEffect>()->Start(L"OpenTheAny",2, 0.3f);
				Animation->ChangeCurrentAnimation(L"Open");
			}
			break;
		case 2:
			m_CataMove = true;
			break;
		case 3:
			m_PLMove = true;
			break;
		case 4:
			FadePtr->SetFader(L"OUT");
		default:
			break;
		}

		if (m_CataMove) {
			m_Move += ElapsedTime;
			CataTrans->SetPosition(0, m_Move *m_Move, 0);
		}
		if (m_PLMove) {
			PlayTrans->SetPosition(0, 1.5f - m_Move * m_Move, 0.5 + m_Move*m_Move / 10);
		}

	}

	void TitleStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateViewLight();
			//UIの作成
			CreateUI();
			//モデルの作成
			CreateModel();
			//
			CreateSplitSprite();
			
		}
		catch (...) {
			throw;
		}
	}

	void TitleStage::OnUpdate() {
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		auto PushPtr = GetSharedGameObject<Sprite>(L"TITLEPUSH", false);

		if (CntlVec[0].bConnected) {
			//Aボタンが押されたら.
			if (CntlVec[0].wPressedButtons && OnceEvent) {
				PushPtr->GetComponent<MultiSoundEffect>()->Start(L"Decide", 0, 0.3f);
				PushButton = true;
				OnceEvent = false;
				PostEvent(6.0f, GetThis<TitleStage>(), App::GetApp()->GetSceneInterface(), L"ToGame");
				App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType = 0;
			}
		}

		if (PushButton) {
			OpeningEvent();
		}	

		else if (!PushButton) {
			auto ElapsedTime = App::GetApp()->GetElapsedTime();
			AnimTime += ElapsedTime;
			PushPtr->GetComponent<PTSpriteDraw>()->SetDiffuse(Color4(1, 1, 1, cos(AnimTime * 3)));
		}

	}




	//ビューとライトの作成
	void SelectStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);

		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}


	void SelectStage::CreateUI() {
		AddGameObject<Sprite>(true, Vector2(1280, 800), Vector3(-0, 0, 1), L"SELECT_TX");
		AddGameObject<SelectSprite>(true, Vector2(256, 160), Vector3(-0, 0, 1), L"SELECT_TX");
	}

	void SelectStage::OnCreate() {
		try {
			//ビューとライトの作成
			CreateViewLight();
			//UIの作成
			CreateUI();
		}
		catch (...) {
			throw;
		}
	}

	void SelectStage::OnUpdate() {
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {

			//Aボタンが押されたら.
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
				//イベント送出
				PostEvent(0.0f, GetThis<SelectStage>(), App::GetApp()->GetSceneInterface(), L"ToGame");
			}
		}

	}

}
//end basecross
