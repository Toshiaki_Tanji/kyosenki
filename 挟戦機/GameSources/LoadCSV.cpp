#include "stdafx.h"
#include "Project.h"

namespace basecross {


	//--------------------------------------------
	//オブジェクト名で作る
	//Unityなどでステージを作成してCSVとして書き出したものから作成

	CSVParametor::CSVParametor(const shared_ptr<Stage>& StagePtr,
		const wstring & FileName) :
		GameObject(StagePtr),
		m_FilesName(FileName),
		m_isFinish(false)
	{
	}
	CSVParametor::~CSVParametor() {}

	void CSVParametor::CreateLoad()
	{
		//Csvのコンストラクタ
		CsvFile StageCsv(m_FilesName);

		//Csvファイルの取得
		if (!StageCsv.ReadCsv())
		{
			throw BaseException(
				L"if (!StageCsv.ReadCsv())",
				L"読み込みに失敗しました",
				L"CSVファイルの確認をしてください"
			);
		}

		//Csvファイルの行数取得
		const int RowNum = StageCsv.GetRowCount();
		//オブジェクト作成ループ
		for (int i = 0; i < RowNum; i++) {

			StageCsv.GetRowVec(i, RowStr);
			//文字によって作るものを決める
			//弾のタイプ
			if (RowStr[0].find(L"B_Type") != -1) {
				for (int j = 0; j < 5; j++) {
					App::GetApp()->GetScene<Scene>()->GetEnemyParametors().m_BulletType[j] = RowStr[j];
				}
			}
			//発射する(方向)パターン
			if (RowStr[0].find(L"FirePatern") != -1) {
				for (int j = 0; j < 5; j++) {
					App::GetApp()->GetScene<Scene>()->GetEnemyParametors().m_FirePatern[j] = RowStr[j];
				}
			}
			//弾の威力
			if (RowStr[0].find(L"B_Power") != -1) {
				for (int j = 0; j < 5; j++) {
					App::GetApp()->GetScene<Scene>()->GetEnemyParametors().m_DamegePower[j] = stoi(RowStr[j]);
				}
			}
			//残弾数
			if (RowStr[0].find(L"B_Remaind") != -1) {
				for (int j = 0; j < 5; j++) {
					App::GetApp()->GetScene<Scene>()->GetEnemyParametors().m_RemaindBullet[j] = stoi(RowStr[j]);
				}
			}
			//挙動
			if (RowStr[0].find(L"Moving") != -1) {
				for (int j = 0; j < 5; j++) {
					App::GetApp()->GetScene<Scene>()->GetEnemyParametors().m_Moving[j] = RowStr[j];
				}
			}
		}
		m_isFinish = true;	//作り終えた
	}
}