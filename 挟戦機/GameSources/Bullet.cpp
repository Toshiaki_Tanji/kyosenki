#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	class AttackBall : public GameObject;
	//	用途: 飛んでいくボール
	//--------------------------------------------------------------------------------------
	//構築と破棄
	AttackBall::AttackBall(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr) {}

	AttackBall::~AttackBall() {}

	void AttackBall::Weakup(const Vector3& Position, const Quaternion& Rotation, const Vector3& Velocity) {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(0.2f, 0.1f, 0.2f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetQuaternion(Rotation);
		PtrTransform->ResetPosition(Position);
		auto PtrRedid = GetComponent<Rigidbody>();
		PtrRedid->SetVelocity(Velocity);
		SetDrawActive(true);
		SetUpdateActive(true);
	}


	//初期化
	void AttackBall::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(0.1f, 0.1f, 0.1f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(0, 0, 0);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();


		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionSphere>();

		auto PtrDraw = AddComponent<BcPNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");
		PtrDraw->SetTextureResource(L"BULLET_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"AttackBall");
		Group->IntoGroup(GetThis<AttackBall>());

		//弾同士での
		AddTag(L"Bullet");
		PtrCol->AddExcludeCollisionTag(L"Bullet");


		//最初は無効にしておく
		SetDrawActive(false);
		SetUpdateActive(false);

	}

	void AttackBall::OnUpdate() {
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto PtrTransform = GetComponent<Transform>();
		//カメラを得る
		//auto PtrCamera = dynamic_pointer_cast<MainCamera>(OnGetDrawCamera());
		auto PtrCamera = GetStage()->GetView()->GetTargetCamera();

		Quaternion Qt;
		//向きをビルボードにする
		Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());
		PtrTransform->SetQuaternion(Qt);

	}

	void AttackBall::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

		SetDrawActive(false);
		SetUpdateActive(false);
	}

	//--------------------------------------------------------------------------------------
	//	class AttackBall : public GameObject;
	//	用途: 飛んでいくボール
	//--------------------------------------------------------------------------------------
	//構築と破棄
	EnemyAttackBall::EnemyAttackBall(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr) {}

	EnemyAttackBall::~EnemyAttackBall() {}

	void EnemyAttackBall::Weakup(const Vector3& Position, const Quaternion& Rotation, const Vector3& Velocity) {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(0.5f, 0.1f, 0.5f);
		PtrTransform->SetQuaternion(Rotation);
		PtrTransform->ResetPosition(Position);
		auto PtrRedid = GetComponent<Rigidbody>();
		PtrRedid->SetVelocity(Velocity);
		SetDrawActive(true);
		SetUpdateActive(true);
	}


	//初期化
	void EnemyAttackBall::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(0.1f, 0.1f, 0.1f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(0, 0, 0);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();

		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionSphere>();

		auto PtrDraw = AddComponent<BcPNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");

		//描画するテクスチャを設定
		PtrDraw->SetTextureResource(L"BULLET_TX");
		SetAlphaActive(true);
		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyAttackBall");
		Group->IntoGroup(GetThis<EnemyAttackBall>());
		PtrDraw->SetDepthStencilState(DepthStencilState::Read);

		//弾同士での
		AddTag(L"Bullet");
		PtrCol->AddExcludeCollisionTag(L"Bullet");


		//最初は無効にしておく
		SetDrawActive(false);
		SetUpdateActive(false);

	}

	void EnemyAttackBall::OnUpdate() {

		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto PtrTransform = GetComponent<Transform>();
		//カメラを得る
		//auto PtrCamera = dynamic_pointer_cast<MainCamera>(OnGetDrawCamera());
		auto PtrCamera = GetStage()->GetView()->GetTargetCamera();

		Quaternion Qt;
		//向きをビルボードにする
		Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());
		PtrTransform->SetQuaternion(Qt);

	}

	void EnemyAttackBall::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

		SetDrawActive(false);
		SetUpdateActive(false);
	}



	//--------------------------------------------------------------------------------------
	//	class AttackLaser : public GameObject;
	//	用途: 飛んでいくレーザー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	AttackLaser::AttackLaser(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr) {}

	AttackLaser::~AttackLaser() {}

	void AttackLaser::Weakup(const Vector3& Position, const Quaternion& Rotation, const Vector3& Velocity) {
		auto PtrPlayer = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerRot = PtrPlayer->GetComponent<Transform>()->GetRotation();

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(0.3f, 0.1f, 0.3f);
		PtrTransform->SetQuaternion(Rotation);
		PtrTransform->ResetPosition(Position);
		auto PtrRedid = GetComponent<Rigidbody>();
		PtrRedid->SetVelocity(Velocity);
		SetDrawActive(true);
		SetUpdateActive(true);
	}


	//初期化
	void AttackLaser::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(0.25f, 0.25f, 0.1f);
		//PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(0, 0, 0);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();


		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetDrawActive(true);

		auto PtrDraw = AddComponent<BcPNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");

		auto Group = GetStage()->GetSharedObjectGroup(L"AttackLaser");
		Group->IntoGroup(GetThis<AttackLaser>());

		AddTag(L"Bullet");
		PtrCol->AddExcludeCollisionTag(L"Bullet");


		//最初は無効にしておく
		SetDrawActive(false);
		SetUpdateActive(false);

	}

	void AttackLaser::OnUpdate() {
	}

	void AttackLaser::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		SetDrawActive(false);
		SetUpdateActive(false);
	}


	//--------------------------------------------------------------------------------------
	//	class AttackLaser : public GameObject;
	//	用途: 飛んでいくレーザー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	EnemyAttackLaser::EnemyAttackLaser(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr) {}

	EnemyAttackLaser::~EnemyAttackLaser() {}

	void EnemyAttackLaser::Weakup(const Vector3& Position, const Quaternion& Rotation, const Vector3& Velocity) {
		auto PtrPlayer = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerRot = PtrPlayer->GetComponent<Transform>()->GetRotation();

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(0.25f, 0.1f, 0.1f);
		PtrTransform->SetQuaternion(Rotation);
		PtrTransform->ResetPosition(Position);
		auto PtrRedid = GetComponent<Rigidbody>();
		PtrRedid->SetVelocity(Velocity);
		SetDrawActive(true);
		SetUpdateActive(true);
	}


	//初期化
	void EnemyAttackLaser::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(0.25f, 0.25f, 0.1f);
		//PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(0, 0, 0);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();


		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetDrawActive(true);

		auto PtrDraw = AddComponent<BcPNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");

		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyAttackLaser");
		Group->IntoGroup(GetThis<EnemyAttackLaser>());

		//弾同士での
		AddTag(L"Bullet");
		PtrCol->AddExcludeCollisionTag(L"Bullet");


		//最初は無効にしておく
		SetDrawActive(false);
		SetUpdateActive(false);

	}

	void EnemyAttackLaser::OnUpdate() {
	}

	void EnemyAttackLaser::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		SetDrawActive(false);
		SetUpdateActive(false);
	}

	//--------------------------------------------------------------------------------------
	//	class AttackMissile : public GameObject;
	//	用途: 飛んでいくボール
	//--------------------------------------------------------------------------------------
	//構築と破棄
	AttackMissile::AttackMissile(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr) {}

	AttackMissile::~AttackMissile() {}

	void AttackMissile::Weakup(const Vector3& Position, const Quaternion& Rotation, const Vector3& Velocity) {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(0.25f, 0.1f, 0.25f);
		PtrTransform->SetQuaternion(Rotation);
		PtrTransform->ResetPosition(Position);
		auto PtrRedid = GetComponent<Rigidbody>();
		PtrRedid->SetVelocity(Velocity);
		SetDrawActive(true);
		SetUpdateActive(true);
		auto PtrCol = GetComponent<CollisionSphere>();
		PtrCol->SetFixed(false);

		SeOneShot = true;

		//描画コンポーネント
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		//爆発中は赤
		PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		m_Scale = Vector3(3.0f, 3.0f, 3.0f);
		m_ExplodeTime = 1.0f;
		m_FlareTime = 0.5f;
	}


	//爆発する演出
	void AttackMissile::HitAndExplosion() {
		//Transform取得
		auto Ptr = GetComponent<Transform>();
		auto PtrPos = Ptr->GetPosition();
		auto PtrRigid = GetComponent<Rigidbody>();
		PtrRigid->SetVelocityZero();
		//描画コンポーネント
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		//爆発中は赤
		PtrDraw->SetDiffuse(Color4(1.0f, 0.0f, 0.0f, 1.0f));


		auto Col = GetComponent<CollisionSphere>();
		Col->SetUpdateActive(true);
		Col->SetFixed(true);

		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		m_FlareTime -= ElapsedTime;

		//m_Scale.x *= 0.95f; m_Scale.y *= 0.95f; m_Scale.z *= 0.95f;
		m_Scale *= 0.99f;

		if (m_FlareTime <= 0.0f) {
			m_Scale *= 0.75f;
		}

		Ptr->SetPosition(PtrPos);
		Ptr->SetScale(m_Scale);
		if (m_Scale.x <= 0.01f) {
			SetDrawActive(false);
			SetUpdateActive(false);
		}
	}


	//爆発するか否か
	bool AttackMissile::IsExplosion() {
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		m_ExplodeTime -= ElapsedTime;
		if (m_ExplodeTime <= 0.0f) {
			if (SeOneShot) {
				auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Explosion", 0, 0.2f);
				SeOneShot = false;
			}
			return true;
		}
		return false;
	}


	//初期化
	void AttackMissile::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(0.25f, 0.1f, 0.25f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(0, 0, 0);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();
		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionSphere>();
		PtrCol->SetFixed(false);
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		auto Group = GetStage()->GetSharedObjectGroup(L"AttackMissile");
		Group->IntoGroup(GetThis<AttackMissile>());
		//最初は無効にしておく
		SetDrawActive(false);
		SetUpdateActive(false);
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Explosion");

		AddTag(L"Bullet");
		PtrCol->AddExcludeCollisionTag(L"Bullet");

		//
		PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		m_ExplodeTime = 1.0f;
		m_FlareTime = 0.5f;

	}

	void AttackMissile::OnUpdate() {
		if (IsExplosion()) {
			HitAndExplosion();
		}
	}

	void AttackMissile::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		m_ExplodeTime = 0.0f;
	}

	LaserBallEffect::LaserBallEffect(const shared_ptr<Stage>& StagePtr, bool Trace,
		const Vector2& StartScale, const Vector3 & StartPos, const wstring& Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_Media(Media)


	{}
	//破棄
	LaserBallEffect::~LaserBallEffect() {}

	void LaserBallEffect::OnCreate() {

		//Ptr->SetTextureResource(m_Media);
		//画像同士を重ならないようにする
		for (int i = 0; i < 31; i++)
		{
			//頂点配列
			vector<VertexPositionNormalTexture> vertices;
			//インデックスを作成するための配列
			vector<uint16_t> indices;
			//Squareの作成(ヘルパー関数を利用)
			MeshUtill::CreateSquare(1.0f, vertices, indices);
			//UV値の変更
			float from = i / 31.0f;
			float to = from + (1.0f / 31.0f);
			//左上頂点
			vertices[0].textureCoordinate = Vector2(from, 0);
			//右上頂点
			vertices[1].textureCoordinate = Vector2(to, 0);
			//左下頂点
			vertices[2].textureCoordinate = Vector2(from, 1.0f);
			//右下頂点
			vertices[3].textureCoordinate = Vector2(to, 1.0f);
			//頂点の型を変えた新しい頂点を作成
			vector<VertexPositionColorTexture> new_vertices;
			for (auto& v : vertices) {
				VertexPositionColorTexture nv;
				nv.position = v.position;
				nv.color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
				nv.textureCoordinate = v.textureCoordinate;
				new_vertices.push_back(nv);
			}
			//メッシュ作成
			m_Mesh.push_back(MeshResource::CreateMeshResource<VertexPositionColorTexture>(new_vertices, indices, true));
		}



		//スプライトをつける
		auto DrawP = AddComponent<PCTStaticDraw>();
		DrawP->SetDepthStencilState(DepthStencilState::Read);

		DrawP->SetTextureResource(L"LASERBALL_TX");
		DrawP->SetMeshResource(m_Mesh[30]);
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(m_StartPos);
		PtrTrans->SetScale(m_StartScale.x, m_StartScale.y, 1);
		SetAlphaActive(true);
		SetDrawActive(false);

		SetDrawLayer(4);
		//左上頂点
		//PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftTopZeroPlusDownY);

		//スプライト中のメッシュから数字の画像を取得

		/*	auto Group = GetStage()->GetSharedObjectGroup(L"Boost");
		Group->IntoGroup(GetThis<BoosterSprite>());*/


	}

	void LaserBallEffect::OnUpdate() {


		auto Trans = GetComponent<Transform>();
		//ローテーション持ってくる
		auto Rot = Trans->GetRotation();
		//ポジション持ってくる
		auto Pos = Trans->GetPosition();
		//ターゲットカメラ持ってくる
		auto PtrCamera = GetStage()->GetView()->GetTargetCamera();

		//シーン呼ぶ
		auto ScenePtr = App::GetApp()->GetScene<Scene>();

		//auto PtrBoost = GetStage()->GetSharedObjectGroup(L"Boost", false);
		//	Quaternion Qt;
		//向きをビルボードにする
		//	Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());

		//向きをフェイシングにする場合は以下のようにする
		// Qt.Facing(Pos - PtrCamera->GetEye());
		//向きをフェイシングYにする場合は以下のようにする
		// Qt.FacingY(Pos - PtrCamera->GetEye());
		//向きをシークオブジェクトと同じにする場合は以下のようにする
		// Qt = SeekTransPtr->GetQuaternion();



		m_time += App::GetApp()->GetElapsedTime();
		//画像切り替え
		if (m_time > 0.05)
		{
			m_time = 0;
			GetComponent<PCTStaticDraw>()->SetMeshResource(m_Mesh[m_count]);
			m_count++;
			if (m_count > 30)
			{
				m_count = 0;
			}
		}

		auto PtrPlayer = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTrans = PtrPlayer->GetComponent<Transform>();
		auto PlayerPos = PlayerTrans->GetPosition();
		auto PlayerRot = PlayerTrans->GetRotation();
		PlayerRot = Vector3(1.5, PlayerRot.y, 0.0f);
		Pos = Vector3(PlayerPos.x, PlayerPos.y, PlayerPos.z);
		//Enemyにポジション合わせる
		/*Pos.x -= sin(LaserRot.y) ;
		Pos.z -= cos(LaserRot.y);*/
		//Enemyにポジション合わせる
		Pos.x += sin(PlayerRot.y) / 1.15;
		Pos.z += cos(PlayerRot.y) / 1.15;
		//ローテーションをEnemyに合わせる
		Rot = PlayerRot;
		////Enemy死んだらBoost消す
		//if (m_EnemyPtr->DestroyFLG)
		//{
		//	SetDrawActive(false);
		//	SetUpdateActive(false);
		//}
		//if (m_EnemyPtr->Catch)
		//{
		//	SetDrawActive(false);
		//	SetUpdateActive(false);
		//}





		Trans->SetPosition(Pos);
		//Trans->SetQuaternion(Qt);
		Trans->SetRotation(Rot);

		////レーザー発射の時だけ描画する
		if (PtrPlayer->m_isFire && App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType == 1)
		{
			SetDrawActive(true);
		}
		else
		{
			SetDrawActive(false);
		}




	}

}