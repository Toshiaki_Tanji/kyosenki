#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//弾を発射する敵(生田目 2017.04.20

	Enemy1::Enemy1(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	Enemy1::~Enemy1() {}

	//初期化
	void Enemy1::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetIsHitAction(IsHitAction::Auto);
		PtrObb->SetFixed(true);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.75f, 0.75f, 0.75f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
		);


		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"ZAKOENEMY1_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);

		auto PtrDraw = AddComponent<BcPNTStaticDraw>();
		PtrDraw->SetFogEnabled(true);
		PtrDraw->SetMeshResource(L"ZAKOENEMY1_MESH");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"ZAKOENEMY1_TX");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);


		//ステートマシンの構築
		m_StateMachine.reset(new StateMachine<Enemy1>(GetThis<Enemy1>()));
		//最初のステートをPlayerDefaultにリセット
		m_StateMachine->ChangeState(EnemyDefaultState::Instance());

	}

	void Enemy1::OnUpdate() {
		//コントローラチェックして入力があればコマンド呼び出し
		m_InputHandler.PushHandle(GetThis<Enemy1>());
		//ステートマシン更新
		m_StateMachine->Update();
		auto PtrTransform = GetComponent<Transform>();

		auto ThisPos = PtrTransform->GetPosition();
		ThisPos.z -= m_MoveZ;
		PtrTransform->SetPosition(ThisPos);
	}

	//Xボタン
	void  Enemy1::OnPushX() {
		GetStateMachine()->ChangeState(EnemyAttackState::Instance());

	}

	//エネミー２の作成

	Enemy2::Enemy2(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_IsXMove(false)
	{
	}
	Enemy2::~Enemy2() {}

	//初期化
	void Enemy2::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetIsHitAction(IsHitAction::Auto);
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"ZAKOENEMY1_MESH");

		auto PtrDraw = AddComponent<BcPNTStaticDraw>();
		PtrDraw->SetFogEnabled(true);
		PtrDraw->SetMeshResource(L"ZAKOENEMY1_MESH");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"ZAKOENEMY1_TX");

		//ステートマシンの構築
		m_StateMachine.reset(new StateMachine<Enemy2>(GetThis<Enemy2>()));
		//最初のステートをPlayerDefaultにリセット
		//m_StateMachine->ChangeState(EnemyDefaultState::Instance());

	}

	void Enemy2::OnUpdate() {
		//コントローラチェックして入力があればコマンド呼び出し
		//m_InputHandler.PushHandle(GetThis<Enemy2>());
		//ステートマシン更新
		m_StateMachine->Update();
		auto PtrTransform = GetComponent<Transform>();
		auto ThisPos = PtrTransform->GetPosition();
		if (m_IsXMove) {
			ThisPos.x += m_MoveX;
		}
		else {
			ThisPos.z -= m_MoveZ;
		}
		if (abs(m_Position.z - ThisPos.z) >= 15.0f) {
			m_IsXMove = true;
		}
		PtrTransform->SetPosition(ThisPos);
	}

	//--------------------------------------------------------------------------------------
	//	class AttackEnemyBall : public GameObject;
	//	用途: エネミーが飛ばすボール
	//--------------------------------------------------------------------------------------
	//構築と破棄
	AttackEnemyBall::AttackEnemyBall(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr) {}

	AttackEnemyBall::~AttackEnemyBall() {}

	void AttackEnemyBall::Weakup(const Vector3& Position, const Vector3& Velocity) {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(0.1f, 0.1f, 0.1f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->ResetPosition(Position);
		auto PtrRedid = GetComponent<Rigidbody>();
		PtrRedid->SetVelocity(Velocity);
		SetDrawActive(true);
		SetUpdateActive(true);
	}


	//初期化
	void AttackEnemyBall::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(0.1f, 0.1f, 0.1f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(0, 0, 0);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();


		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionSphere>();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		auto PtrDraw = AddComponent<BcPNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");

		auto Group = GetStage()->GetSharedObjectGroup(L"AttackBall");
		Group->IntoGroup(GetThis<AttackBall>());

		//最初は無効にしておく
		SetDrawActive(false);
		SetUpdateActive(false);
	}

	void AttackEnemyBall::OnUpdate() {
		Rect2D<float> rect(-10.f, 0.0f, 10.0f, 37.0f);
		Point2D<float> point;
		auto PtrTransform = GetComponent<Transform>();
		point.x = PtrTransform->GetPosition().x;
		point.y = PtrTransform->GetPosition().z;
		if (!rect.PtInRect(point) || abs(PtrTransform->GetPosition().y) > 10.0f) {
			PtrTransform->SetScale(0.1f, 0.1f, 0.1f);
			PtrTransform->SetRotation(0, 0, 0);
			PtrTransform->SetPosition(0, 0, 0);
			SetDrawActive(false);
			SetUpdateActive(false);
		}
	}

	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	IMPLEMENT_SINGLETON_INSTANCE(EnemyDefaultState)

		void EnemyDefaultState::Enter(const shared_ptr<Enemy1>& Obj) {
	}

	void EnemyDefaultState::Execute(const shared_ptr<Enemy1>& Obj) {
	}

	void EnemyDefaultState::Exit(const shared_ptr<Enemy1>& Obj) {
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	///	アタックステート
	//--------------------------------------------------------------------------------------
	IMPLEMENT_SINGLETON_INSTANCE(EnemyAttackState)

		void EnemyAttackState::Enter(const shared_ptr<Enemy1>& Obj) {
		auto PtrBehavior = Obj->GetBehavior<EnemyBehavior>();
		PtrBehavior->FireAttackEnemyBall();
	}

	void EnemyAttackState::Execute(const shared_ptr<Enemy1>& Obj) {
		//すぐにステートを戻す
		Obj->GetStateMachine()->ChangeState(EnemyDefaultState::Instance());
	}

	void EnemyAttackState::Exit(const shared_ptr<Enemy1>& Obj) {
		//何もしない
	}

	//--------------------------------------------------------------------------------------
//	敵		小松 2017.04.20
//--------------------------------------------------------------------------------------
//構築と破棄
	Enemy_A::Enemy_A(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<StageCellMap>& CellMap,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_CelMap(CellMap),
		m_Scale(Scale),
		m_StartRotation(Rotation),
		m_StartPosition(Position),
		m_CellIndex(-1),
		m_NextCellIndex(-1),
		m_TargetCellIndex(-1)
	{
	}
	Enemy_A::~Enemy_A() {}

	bool Enemy_A::SearchPlayer() {
		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			auto PathPtr = GetComponent<PathSearch>();
			auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
			auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
			m_CellPath.clear();
			//パス検索をかける
			if (PathPtr->SearchCell(PlayerPos, m_CellPath)) {
				//検索が成功した
				m_CellIndex = 0;
				m_TargetCellIndex = m_CellPath.size() - 1;
				if (m_CellIndex == m_TargetCellIndex) {
					//すでに同じセルにいる
					m_NextCellIndex = m_CellIndex;
				}
				else {
					//離れている
					m_NextCellIndex = m_CellIndex + 1;

				}
				return true;
			}
			else {
				//失敗した
				m_CellIndex = -1;
				m_NextCellIndex = -1;
				m_TargetCellIndex = -1;
			}
		}
		return false;
	}

	bool Enemy_A::DefaultBehavior() {
		auto PtrRigid = GetComponent<Rigidbody>();
		auto Velo = PtrRigid->GetVelocity();
		Velo *= 0.85f;
		PtrRigid->SetVelocity(Velo);
		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
			auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
			CellIndex PlayerCell;
			if (MapPtr->FindCell(PlayerPos, PlayerCell)) {
				return false;
			}
		}
		return true;
	}


	bool Enemy_A::SeekBehavior() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto MyPos = GetComponent<Transform>()->GetPosition();


		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			if (SearchPlayer()) {
				auto PtrSeek = GetBehavior<SeekSteering>();
				if (m_NextCellIndex == 0) {
					auto PtrRigid = GetComponent<Rigidbody>();
					auto Velo = PtrRigid->GetVelocity();
					Velo *= 0.85f;
					PtrRigid->SetVelocity(Velo);
					PlayerPos.y = m_StartPosition.y;
					PtrSeek->Execute(PlayerPos);
				}
				else {
					if (Vector3EX::Length(MyPos - PlayerPos) <= 3.0f) {
						auto PtrRigid = GetComponent<Rigidbody>();
						auto Velo = PtrRigid->GetVelocity();
						Velo *= 0.85f;
						PtrRigid->SetVelocity(Velo);
					}
					AABB ret;
					MapPtr->FindAABB(m_CellPath[m_NextCellIndex], ret);
					auto Pos = ret.GetCenter();
					Pos.y = m_StartPosition.y;
					PtrSeek->Execute(Pos);
				}
				return true;
			}
			else {
				auto PtrSeek = GetBehavior<SeekSteering>();
				CellIndex PlayerCell;
				if (MapPtr->FindCell(PlayerPos, PlayerCell)) {
					AABB ret;
					MapPtr->FindAABB(PlayerCell, ret);
					auto Pos = ret.GetCenter();
					Pos.y = m_StartPosition.y;
					PtrSeek->Execute(Pos);
					return true;
				}
			}
		}
		return false;
	}


	//初期化
	void Enemy_A::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_StartPosition);
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_StartRotation);
		//Rigidbodyをつける
		auto PtrRigid = AddComponent<Rigidbody>();
		//パス検索
		auto MapPtr = m_CelMap.lock();
		if (!MapPtr) {
			throw BaseException(
				L"セルマップが不定です",
				L"if (!MapPtr) ",
				L" Enemy::OnCreate()"
			);
		}
		auto PathPtr = AddComponent<PathSearch>(MapPtr);

		//SPの衝突判定をつける
		auto PtrColl = AddComponent<CollisionSphere>();
		PtrColl->SetIsHitAction(IsHitAction::Auto);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		auto PtrDraw = AddComponent<BcPNTStaticDraw>();
		PtrDraw->SetFogEnabled(true);
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(L"TRACE2_TX");
		//透明処理をする
		SetAlphaActive(true);

		m_StateMachine = make_shared<StateMachine<Enemy_A>>(GetThis<Enemy_A>());
		m_StateMachine->ChangeState(Enemy_ADefault::Instance());
	}

	//更新
	void Enemy_A::OnUpdate() {
		//ステートによって変わらない行動を実行
		auto PtrGrav = GetBehavior<Gravity>();
		PtrGrav->Execute();
		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();
	}

	void Enemy_A::OnUpdate2() {
		//進行方向を向くようにする
		RotToHead();
	}


	//進行方向を向くようにする
	void Enemy_A::RotToHead() {
		auto PtrRigidbody = GetComponent<Rigidbody>();
		//回転の更新
		//Velocityの値で、回転を変更する
		//これで進行方向を向くようになる
		auto PtrTransform = GetComponent<Transform>();
		Vector3 Velocity = PtrRigidbody->GetVelocity();
		if (Velocity.Length() > 0.0f) {
			Vector3 Temp = Velocity;
			Temp.Normalize();
			float ToAngle = atan2(Temp.x, Temp.z);
			Quaternion Qt;
			Qt.RotationRollPitchYaw(0, ToAngle, 0);
			Qt.Normalize();
			//現在の回転を取得
			Quaternion NowQt = PtrTransform->GetQuaternion();
			//現在と目標を補間（10分の1）
			NowQt.Slerp(NowQt, Qt, 0.1f);
			PtrTransform->SetQuaternion(NowQt);
		}
	}




	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	IMPLEMENT_SINGLETON_INSTANCE(Enemy_ADefault)

		void Enemy_ADefault::Enter(const shared_ptr<Enemy_A>& Obj) {
	}

	void Enemy_ADefault::Execute(const shared_ptr<Enemy_A>& Obj) {
		if (!Obj->DefaultBehavior()) {
			Obj->GetStateMachine()->ChangeState(Enemy_ASeek::Instance());
		}
	}
	void Enemy_ADefault::Exit(const shared_ptr<Enemy_A>& Obj) {
	}

	//--------------------------------------------------------------------------------------
	///	プレイヤーを追いかけるステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	IMPLEMENT_SINGLETON_INSTANCE(Enemy_ASeek)

		void Enemy_ASeek::Enter(const shared_ptr<Enemy_A>& Obj) {
	}

	void Enemy_ASeek::Execute(const shared_ptr<Enemy_A>& Obj) {
		if (!Obj->SeekBehavior()) {
			Obj->GetStateMachine()->ChangeState(Enemy_ADefault::Instance());
		}
	}

	void Enemy_ASeek::Exit(const shared_ptr<Enemy_A>& Obj) {
	}

	IMPLEMENT_SINGLETON_INSTANCE(Enemy_A_AttackState)

		void Enemy_A_AttackState::Enter(const shared_ptr<Enemy_A>& Obj) {
		auto PtrBehavior = Obj->GetBehavior<PlayerBehavior>();
		PtrBehavior->FireAttackBall();
	}

	void Enemy_A_AttackState::Execute(const shared_ptr<Enemy_A>& Obj) {
		//すぐにステートを戻す
		//Obj->GetStateMachine()->Pop();
	}

	void Enemy_A_AttackState::Exit(const shared_ptr<Enemy_A>& Obj) {
		//何もしない
	}


	//--------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------
	//	敵
	//--------------------------------------------------------------------------------------
	//構築と破棄
	GameBoss::GameBoss(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<StageCellMap>& CellMap,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const int& Durability
	) :
		GameObject(StagePtr),
		m_CelMap(CellMap),
		m_Scale(Scale),
		m_StartRotation(Rotation),
		m_StartPosition(Position),
		m_CellIndex(-1),
		m_NextCellIndex(-1),
		m_TargetCellIndex(-1),
		m_Durability(Durability)
	{
	}
	//破棄
	GameBoss::~GameBoss() {}

	bool GameBoss::SearchPlayer() {
		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			auto PathPtr = GetComponent<PathSearch>();
			auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
			auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
			m_CellPath.clear();
			//パス検索をかける
			if (PathPtr->SearchCell(PlayerPos, m_CellPath)) {
				//検索が成功した
				m_CellIndex = 0;
				m_TargetCellIndex = m_CellPath.size() - 1;
				if (m_CellIndex == m_TargetCellIndex) {
					//すでに同じセルにいる
					m_NextCellIndex = m_CellIndex;
				}
				else {
					//離れている
					m_NextCellIndex = m_CellIndex + 1;

				}
				return true;
			}
			else {
				//失敗した
				m_CellIndex = -1;
				m_NextCellIndex = -1;
				m_TargetCellIndex = -1;
			}
		}
		return false;
	}

	bool GameBoss::DefaultBehavior() {

		//if (MapPtr) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTrans = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerTrans->GetPosition();
		auto PlayerRot = PlayerTrans->GetRotation();
		//自分のポジション
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		auto Rot = Trans->GetRotation();

		auto PtrRigid = GetComponent<Rigidbody>();
		PtrRigid->SetReflection(0.0f);
		auto Velo = PtrRigid->GetVelocity();

		//cosθ
		auto dx = PlayerPos.x - Pos.x;
		//sinθ
		auto dz = PlayerPos.z - Pos.z;
		//tan
		float tWork = dz / dx;
		//目的の角度取得
		float angle = atan2(dx, dz);

		//目的の角度（Player）へ向ける
		Rot = Vector3(0.0f, angle, 0.0f);

		Trans->SetPosition(Pos);
		Trans->SetRotation(Rot);

		if (AttackLength() && m_Durability > m_DefoArmor / 2) {
			BossAttackBullet();
			//BossAttackLaser();
			AroundPL();
		}
		else if (m_Durability < m_DefoArmor / 2) {
			BossAttackMissile();
			AroundPL();
		}
		else {
			Velo *= 0.95f;
			PtrRigid->SetVelocity(Velo);
			auto MapPtr = m_CelMap.lock();
			if (MapPtr) {
				auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
				auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
				CellIndex PlayerCell;
				if (MapPtr->FindCell(PlayerPos, PlayerCell)) {
					//プレイヤーがセルマップ上に入った。
					return false;
				}
				else {
					//プレイヤーはマップ上にいない
					//マップをプレイヤーの周りに再設定
					Vector3  CellmapStart;
					CellmapStart.x = float((int)(PlayerPos.x - 2.0f));
					CellmapStart.z = float((int)(PlayerPos.z - 2.0f));
					CellmapStart.y = 0.0f;
					MapPtr->RefleshCellMap(CellmapStart, 1.0f, 4, 4);
					auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
					GameStagePtr->SetCellMapCost(L"CellMap2");
				}
			}
		}
		return true;

	}

	void GameBoss::SetDameged() {
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		//こいつのステータス
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetWorldPosition();

		App::GetApp()->GetScene<Scene>()->GetGameParametors().m_BossHP--;
		this->m_Durability--;
		IsDamaged = true;

		if (this->m_Durability <= 0) {
			ScenePtr->DestroyFLG = true; DestroyFLG = true;
			ScenePtr->DestroyCount += 1;
			auto StagePtr = GetStage();
			if (ScenePtr->DestroyFLG = true) {
				StagePtr->AddGameObject<ExplosionSprite>(true, Vector2(1, 1), Vector3(Pos), L"Explosion_TX");
			}
			this->SetDrawActive(false);
			this->SetUpdateActive(false);
		}
	}

	//ダメージモーション
	void GameBoss::DamegeMotion() {

		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		if (IsDamaged) {
			m_flashinterval += ElapsedTime;
			m_invisibleTime += ElapsedTime;
			if (m_invisibleTime > 0.5f) {
				IsDamaged = false;
				m_invisibleTime = 0.0f;
				ClearChanger = 1;
			}
			else if (m_flashinterval > 0.02f) {
				m_flashinterval = 0;
				ClearChanger *= -1;
			}
			auto PtrDraw = AddComponent<PNTBoneModelDraw>();
			PtrDraw->SetDiffuse(Color4(1, 1, 1, ClearChanger));
		}
	}


	bool GameBoss::SeekBehavior() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto MyPos = GetComponent<Transform>()->GetPosition();

		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			if (SearchPlayer()) {
				auto PtrSeek = GetBehavior<SeekSteering>();
				if (m_NextCellIndex == 0) {
					auto PtrRigid = GetComponent<Rigidbody>();
					auto Velo = PtrRigid->GetVelocity();
					Velo *= 0.5f;
					PtrRigid->SetVelocity(Velo);
					PlayerPos.y = m_StartPosition.y;
					PtrSeek->Execute(PlayerPos);
				}
				else {
					if (Vector3EX::Length(MyPos - PlayerPos) <= 3.0f) {
						auto PtrRigid = GetComponent<Rigidbody>();
						auto Velo = PtrRigid->GetVelocity();
						Velo *= 0.55f;
						PtrRigid->SetVelocity(Velo);
					}
					AABB ret;
					MapPtr->FindAABB(m_CellPath[m_NextCellIndex], ret);
					auto Pos = ret.GetCenter();
					Pos.y = m_StartPosition.y;
					PtrSeek->Execute(Pos);
				}
				return true;
			}
			else {
				auto PtrSeek = GetBehavior<SeekSteering>();
				CellIndex PlayerCell;
				if (MapPtr->FindCell(PlayerPos, PlayerCell)) {
					AABB ret;
					MapPtr->FindAABB(PlayerCell, ret);
					auto Pos = ret.GetCenter();
					Pos.y = m_StartPosition.y;
					PtrSeek->Execute(Pos);
					return true;
				}
			}
		}
		return false;
	}

	void GameBoss::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		for (auto &v : OtherVec) {
			//シーン
			auto shBullet = dynamic_pointer_cast<AttackBall>(v);
			auto shLaser = dynamic_pointer_cast<AttackLaser>(v);
			auto shMissile = dynamic_pointer_cast<AttackMissile>(v);
			//ダメージ食らう
			if (shBullet || shLaser || shMissile) {
				this->SetDameged();
			}
		}
	}

	//初期化
	void GameBoss::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		auto Pos = PtrTransform->GetPosition();
		PtrTransform->SetPosition(m_StartPosition);
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_StartRotation);

		m_DefoArmor = m_Durability;

		AddTag(L"BOSS");

		//Rigidbodyをつける
		auto PtrRigid = AddComponent<Rigidbody>();
		//パス検索
		auto MapPtr = m_CelMap.lock();
		if (!MapPtr) {
			throw BaseException(
				L"セルマップが不定です",
				L"if (!MapPtr) ",
				L" GameBoss::OnCreate()"
			);
		}
		auto PathPtr = AddComponent<PathSearch>(MapPtr);

		//SPの衝突判定をつける
		auto PtrColl = AddComponent<CollisionObb>();
		PtrColl->SetIsHitAction(IsHitAction::Auto);
		//PtrColl->SetDrawActive(true);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.75f, 0.75f, 0.75f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
		);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();


		auto PtrModelDraw = AddComponent<PNTBoneModelDraw>();

		SpanMat.DefTransformation(
			Vector3(0.75f, 0.75f, 0.75f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -1.5f, 0.0f)
		);

		PtrRigid->SetReflection(0.5f);
		ShadowPtr->SetMeshResource(L"BOSS_MESH");
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);
		//PtrModelDraw->SetFogEnabled(true);
		PtrModelDraw->SetMeshResource(L"BOSS_MESH");
		PtrModelDraw->SetTextureResource(L"BOSS_TX");
		PtrModelDraw->SetMeshToTransformMatrix(SpanMat);

		PtrModelDraw->AddAnimation(L"Default", 1, 0, false, 30.0f);
		PtrModelDraw->AddAnimation(L"Morfing", 0, 180, false, 30.0f);

		PtrModelDraw->ChangeCurrentAnimation(L"Default");

		App::GetApp()->GetScene<Scene>()->GetGameParametors().m_BossHP = m_Durability;

		//auto Group = GetStage()->GetSharedObjectGroup(L"Enemys");
		//Group->IntoGroup(GetThis<GameBoss>());
		//透明処理をする
		SetAlphaActive(true);


		//弾同士での
		AddTag(L"EnemyCol");
		PtrColl->AddExcludeCollisionTag(L"EnemyCol");


		m_StateMachine = make_shared<StateMachine<GameBoss>>(GetThis<GameBoss>());
		m_StateMachine->ChangeState(BossDefault::Instance());
	}

	//更新
	void GameBoss::OnUpdate() {

		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		Pos.y = 0;
		Trans->SetPosition(Pos);

		auto Rigid = GetComponent<Rigidbody>();
		auto Velo = Rigid->GetVelocity();
		Velo.y = 0;
		Rigid->SetVelocity(Velo);


		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		//Playerが死んだときEnemy停止
		if (PlayerPtr->m_Armor <= 0)
		{
			SetUpdateActive(false);
		}

		//アニメーションを更新する
		auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		PtrDraw->UpdateAnimation(ElapsedTime);

		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();

		//ダメージを食らった時のモーション
		DamegeMotion();

	}

	void GameBoss::OnUpdate2() {

		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		//シーン呼ぶ
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto BoostPtr = GetStage()->GetSharedGameObject<BoosterSprite>(L"Boost", false);
		auto PtrBoost = GetStage()->GetSharedObjectGroup(L"Boost", false);
		auto PtrDraw = GetComponent<PNTBoneModelDraw>();
		if (m_Durability < m_DefoArmor / 2 && PtrDraw->GetCurrentAnimation() ==L"Default") {
			PtrDraw->ChangeCurrentAnimation(L"Morfing");
		}
		//進行方向を向くようにする
		RotToHead();
	}


	//進行方向を向くようにする
	void GameBoss::RotToHead() {
		auto PtrRigidbody = GetComponent<Rigidbody>();
		//回転の更新
		//Velocityの値で、回転を変更する
		//これで進行方向を向くようになる
		auto PtrTransform = GetComponent<Transform>();
		Vector3 Velocity = PtrRigidbody->GetVelocity();
		if (Velocity.Length() > 0.0f) {
			Vector3 Temp = Velocity;
			Temp.Normalize();
			float ToAngle = atan2(Temp.x, Temp.z);
			Quaternion Qt;
			Qt.RotationRollPitchYaw(0, ToAngle, 0);
			Qt.Normalize();
			//現在の回転を取得
			Quaternion NowQt = PtrTransform->GetQuaternion();
			//現在と目標を補間（10分の1）
			NowQt.Slerp(NowQt, Qt, 0.1f);
			PtrTransform->SetQuaternion(NowQt);
		}
	}


	//プレイヤーに弾を撃つ距離にいるかどうか
	bool GameBoss::AttackLength() {
		auto ThisTrans = GetComponent<Transform>();
		auto ThisPos = ThisTrans->GetPosition();
		auto Rigid = GetComponent<Rigidbody>();

		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();

		auto LengthPos = Vector3EX::Length(PlayerPos - ThisPos);;

		if (LengthPos <= 10) {
			Rigid->SetVelocityZero();
			BulletFire = true;
			return true;
		}
		return false;
	}

	//弾を撃つ関数
	void GameBoss::BossAttackBullet() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto Trans = GetComponent<Transform>();
		//弾の発射
		if (BulletFire)
		{
			auto Group = GetStage()->GetSharedObjectGroup(L"AttackBall");
			for (auto& v : Group->GetGroupVector()) {
				auto shptr = v.lock();
				if (shptr) {
					auto AttackPtr = dynamic_pointer_cast<AttackBall>(shptr);
					if (AttackPtr && !AttackPtr->IsUpdateActive()) {
						//回転の計算
						auto RotY = Trans->GetRotation().y;
						auto Angle = Vector3(sin(RotY), 0, cos(RotY));
						Quaternion Qt;
						Qt.RotationRollPitchYaw(0, RotY, 0);
						Qt.Normalize();
						Angle.Normalize();
						auto Span = Angle * 1.5f;
						auto Pos = Trans->GetPosition();
						Pos.y = PlayerPtr->GetComponent<Transform>()->GetPosition().y;
						AttackPtr->Weakup(Pos + Span, Qt, Angle * 15.0f);
						//弾を撃った
						BulletFire = false;
						return;
					}
				}
			}
		}
	}

	//-----------------
	//レーザーを撃つの
	void GameBoss::BossAttackLaser() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto Trans = GetComponent<Transform>();
		//弾の発射
		if (BulletFire)
		{
			auto Group = GetStage()->GetSharedObjectGroup(L"AttackLaser");
			for (auto& v : Group->GetGroupVector()) {
				auto shptr = v.lock();
				if (shptr) {
					auto AttackPtr = dynamic_pointer_cast<AttackLaser>(shptr);
					if (AttackPtr && !AttackPtr->IsUpdateActive()) {
						//回転の計算
						auto RotY = Trans->GetRotation().y;
						auto Angle = Vector3(sin(RotY), 0, cos(RotY));
						Quaternion Qt;
						Qt.RotationRollPitchYaw(0, RotY, 0);
						Qt.Normalize();
						Angle.Normalize();
						auto Span = Angle * 1.5f;
						auto Pos = Trans->GetPosition();
						Pos.y = PlayerPtr->GetComponent<Transform>()->GetPosition().y;
						AttackPtr->Weakup(Pos + Span, Qt, Angle * 20.0f);
						//弾を撃った
						BulletFire = false;
						return;
					}
				}
			}
		}
	}

	//---------------------------------------
	//ミサイル
	void GameBoss::BossAttackMissile() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto Trans = GetComponent<Transform>();
		//弾の発射
		if (BulletFire)
		{
			auto Group = GetStage()->GetSharedObjectGroup(L"AttackMissile");
			for (auto& v : Group->GetGroupVector()) {
				auto shptr = v.lock();
				if (shptr) {
					auto AttackPtr = dynamic_pointer_cast<AttackMissile>(shptr);
					if (AttackPtr && !AttackPtr->IsUpdateActive()) {
						//回転の計算
						auto RotY = Trans->GetRotation().y;
						auto Angle = Vector3(sin(RotY), 0, cos(RotY));
						Quaternion Qt;
						Qt.RotationRollPitchYaw(0, RotY, 0);
						Qt.Normalize();
						Angle.Normalize();
						auto Span = Angle * 1.5f;
						auto Pos = Trans->GetPosition();
						Pos.y = PlayerPtr->GetComponent<Transform>()->GetPosition().y;
						AttackPtr->Weakup(Pos + Span, Qt, Angle * 10.0f);
						//弾を撃った
						BulletFire = false;
						return;
					}
				}
			}
		}
	}

	void GameBoss::AttackMove() {

	}

	void GameBoss::AroundPL() {
		auto PLPtr = GetStage()->GetSharedGameObject<Player>(L"Player");

		auto PLTrans = PLPtr->GetComponent<Transform>();
		auto PLRot = PLTrans->GetRotation();

		auto Ptr = GetComponent<Transform>();
		auto MyPos = Ptr->GetPosition();

		MyPos.x += sin(PLRot.y) /2;
		MyPos.z += cos(PLRot.y) /2;

	//	Ptr->SetPosition(MyPos);

	}


	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	IMPLEMENT_SINGLETON_INSTANCE(BossDefault)

		void BossDefault::Enter(const shared_ptr<GameBoss>& Obj) {
	}

	void BossDefault::Execute(const shared_ptr<GameBoss>& Obj) {
		if (!Obj->DefaultBehavior()) {
			Obj->GetStateMachine()->ChangeState(BossSeek::Instance());
		}

	}
	void BossDefault::Exit(const shared_ptr<GameBoss>& Obj) {
	}

	//--------------------------------------------------------------------------------------
	///	プレイヤーを追いかけるステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	IMPLEMENT_SINGLETON_INSTANCE(BossSeek)

		void BossSeek::Enter(const shared_ptr<GameBoss>& Obj) {
	}

	void BossSeek::Execute(const shared_ptr<GameBoss>& Obj) {
		if (!Obj->SeekBehavior()) {
			Obj->GetStateMachine()->ChangeState(BossDefault::Instance());
		}
	}

	void BossSeek::Exit(const shared_ptr<GameBoss>& Obj) {
	}
}
//end basecross
