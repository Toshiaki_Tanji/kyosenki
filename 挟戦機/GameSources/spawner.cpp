#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//生成
	TestEnemy::TestEnemy(const shared_ptr<Stage>& StagePtr,
		const Vector3& Position, const Color4& col):
		GameObject(StagePtr),
		m_Position(Position),
		m_Col(col)
	{}
	//初期化
	void TestEnemy::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(Vector3(0.5f,0.5f,0.5f));
		PtrTransform->SetRotation(Vector3(0.0f, 0.0f, 0.0f));
		PtrTransform->SetPosition(m_Position);
		auto PtrDraw = AddComponent<BcPNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetDiffuse(m_Col);

	}
	//アップデート
	void TestEnemy::OnUpdate() {
		auto PtrTransform = GetComponent<Transform>();
		auto Pos = PtrTransform->GetPosition();
		Pos.z -= 0.05f;
		PtrTransform->SetPosition(Pos);
		if (Pos.z <= 10.0f) {
			SetUpdateActive(false);
			SetDrawActive(false);
		}
	}
	Spawner::Spawner(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		UINT Probability,
		const Color4 Col
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_Probability(Probability),
		m_Col(Col)
	{
	}
	Spawner::~Spawner() {}

	//初期化
	void Spawner::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		//PtrTransform->SetScale(m_Scale);
		//PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//auto PtrDraw = AddComponent<BcPNTStaticDraw>();
		//PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
	}
	void Spawner::OnUpdate() {
		for (auto it = m_EnemyVec.begin(); it != m_EnemyVec.end();it++) {
			auto ShPtr = (*it).lock();
			if (ShPtr) {
				if ((!ShPtr->IsUpdateActive()) || (!ShPtr->IsDrawActive())) {
					GetStage()->RemoveGameObject<TestEnemy>(ShPtr);
					m_EnemyVec.erase(it);
					break;
				}
			}
		}
		bool b = Util::DivProbability(m_Probability);
		if (b) {
			Vector3 Pos = GetComponent<Transform>()->GetPosition();
			float Span = Util::RandZeroToOne(true);
			Span -= 0.5f;
			Pos.x += Span;
			GetStage()->AddGameObject<TestEnemy>(Pos, m_Col);
		}
		
	}
  }