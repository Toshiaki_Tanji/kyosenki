#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	///	仮エネミー	//弾を発射する敵(生田目2017.04.20
	//--------------------------------------------------------------------------------------
	class Enemy1 : public GameObject {
		//入力ハンドラー
		InputHandler<Enemy1> m_InputHandler;
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		const float m_MoveZ = 0.06f;
		unique_ptr<StateMachine<Enemy1>>  m_StateMachine;
	public:
		unique_ptr<StateMachine<Enemy1>>& GetStateMachine() {
			return m_StateMachine;
		}
		//構築と破棄
		Enemy1(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);
		virtual ~Enemy1();
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;

		//Aボタン
		void OnPushA() {}
		void OutReleaseA() {}
		//Bボタン
		void OnPushB() {}
		void OutReleaseB() {}
		//Xボタン
		void OnPushX();

	};

	//--------------------------------------------------------------------------------------
	///	仮エネミー2
	//--------------------------------------------------------------------------------------
	class Enemy2 : public GameObject {
		//入力ハンドラー
		InputHandler<Enemy1> m_InputHandler;
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		const float m_MoveZ = 0.06f;
		const float m_MoveX = 0.06f;
		bool m_IsXMove;
		unique_ptr<StateMachine<Enemy2>>  m_StateMachine;
	public:
		unique_ptr<StateMachine<Enemy2>>& GetStateMachine() {
			return m_StateMachine;
		}
		//構築と破棄
		Enemy2(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);
		virtual ~Enemy2();
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;

	};


	//--------------------------------------------------------------------------------------
	///	飛んでいくボール
	//--------------------------------------------------------------------------------------
	class AttackEnemyBall : public GameObject {
	public:
		//構築と破棄
		AttackEnemyBall(const shared_ptr<Stage>& StagePtr);
		virtual ~AttackEnemyBall();
		void Weakup(const Vector3& Position, const Vector3& Velocity);
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
	};


	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	class EnemyDefaultState : public ObjState<Enemy1>
	{
		EnemyDefaultState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(EnemyDefaultState)
		virtual void Enter(const shared_ptr<Enemy1>& Obj)override;
		virtual void Execute(const shared_ptr<Enemy1>& Obj)override;
		virtual void Exit(const shared_ptr<Enemy1>& Obj)override;
	};


	//--------------------------------------------------------------------------------------
	///	アタックステート
	//--------------------------------------------------------------------------------------
	class EnemyAttackState : public ObjState<Enemy1>
	{
		EnemyAttackState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(EnemyAttackState)
		virtual void Enter(const shared_ptr<Enemy1>& Obj)override;
		virtual void Execute(const shared_ptr<Enemy1>& Obj)override;
		virtual void Exit(const shared_ptr<Enemy1>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	敵
	//--------------------------------------------------------------------------------------
	class Enemy_A : public GameObject {
	protected:
		weak_ptr<StageCellMap> m_CelMap;
		Vector3 m_Scale;
		Vector3 m_StartRotation;
		Vector3 m_StartPosition;
		vector<CellIndex> m_CellPath;
		//現在の自分のセルインデックス
		int m_CellIndex;
		//めざす（次の）のセルインデックス
		int m_NextCellIndex;
		//ターゲットのセルインデックス
		int m_TargetCellIndex;
		shared_ptr<StateMachine<Enemy_A>> m_StateMachine;
		//進行方向を向くようにする
		void RotToHead();
	public:
		//構築と破棄
		Enemy_A(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~Enemy_A();
		//プレイヤーの検索
		bool SearchPlayer();

		//デフォルト行動
		virtual bool DefaultBehavior();
		//Seek行動
		bool SeekBehavior();
		//アクセサ
		shared_ptr< StateMachine<Enemy_A> > GetStateMachine() const {
			return m_StateMachine;
		}
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
	};


	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	class Enemy_ADefault : public ObjState<Enemy_A>
	{
		Enemy_ADefault() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(Enemy_ADefault)
		virtual void Enter(const shared_ptr<Enemy_A>& Obj)override;
		virtual void Execute(const shared_ptr<Enemy_A>& Obj)override;
		virtual void Exit(const shared_ptr<Enemy_A>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	///	プレイヤーを追いかけるステート
	//--------------------------------------------------------------------------------------
	class Enemy_ASeek : public ObjState<Enemy_A>
	{
		Enemy_ASeek() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(Enemy_ASeek)
		virtual void Enter(const shared_ptr<Enemy_A>& Obj)override;
		virtual void Execute(const shared_ptr<Enemy_A>& Obj)override;
		virtual void Exit(const shared_ptr<Enemy_A>& Obj)override;

	};


	class Enemy_A_AttackState : public ObjState<Enemy_A>
	{
		Enemy_A_AttackState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(Enemy_A_AttackState)
		virtual void Enter(const shared_ptr<Enemy_A>& Obj)override;
		virtual void Execute(const shared_ptr<Enemy_A>& Obj)override;
		virtual void Exit(const shared_ptr<Enemy_A>& Obj)override;
	};





	//--------------------------------------------------------------------------------------
	//	敵
	//--------------------------------------------------------------------------------------
	class GameBoss : public GameObject {
	protected:
		weak_ptr<StageCellMap> m_CelMap;
		Vector3 m_Scale;
		Vector3 m_StartRotation;
		Vector3 m_StartPosition;
		vector<CellIndex> m_CellPath;
		//現在の自分のセルインデックス
		int m_CellIndex;
		//めざす（次の）のセルインデックス
		int m_NextCellIndex;
		//ターゲットのセルインデックス
		int m_TargetCellIndex;
		shared_ptr<StateMachine<GameBoss>> m_StateMachine;
		//進行方向を向くようにする
		void RotToHead();
		//耐久度
		int m_Durability;

		int m_DefoArmor;
		//
		bool Move = true;
		int MoveTime;
		int DestroyTime;
		//
		bool Search = true;
		bool BulletFire = false;
		//プレイヤーを追う時間
		float SearchTime = 60;
		//クールタイム
		float CoolTime = 60;
		//攻撃
		float AttackCoolTime = 10;

		float dx;
		float dz;
		float angle;


	public:
		//構築と破棄
		GameBoss(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const int& Durability
		);
		virtual ~GameBoss();
		//プレイヤーの検索
		bool SearchPlayer();
		//無敵時間
		float m_invisibleTime;
		//点滅間隔
		float m_flashinterval;
		//透明度切り替え
		int ClearChanger = 1;
		//被弾したかどうか
		bool IsDamaged;
		//ダメージモーション
		void DamegeMotion();


		//攻撃距離を測る
		bool AttackLength();
		//弾を撃つ
		void BossAttackBullet();
		//弾を撃つ
		void BossAttackLaser();
		//弾を撃つ
		void BossAttackMissile();
		//動く
		void AttackMove();
		//プレイヤーの周りをぐるぐる
		void AroundPL();
		//撃破モーション
		void BreakMotion();


		//ダメージを食らうときの処理
		void SetDameged();

		//デフォルト行動
		virtual bool DefaultBehavior();
		//Seek行動
		bool SeekBehavior();
		//アクセサ
		shared_ptr< StateMachine<GameBoss> > GetStateMachine() const {
			return m_StateMachine;
		}
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		//ブーストエフェクト作ったかどうか
		bool BoostCreate = false;
		bool DestroyFLG = false;
	};
	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	class BossDefault : public ObjState<GameBoss>
	{
		BossDefault() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(BossDefault)
		virtual void Enter(const shared_ptr<GameBoss>& Obj)override;
		virtual void Execute(const shared_ptr<GameBoss>& Obj)override;
		virtual void Exit(const shared_ptr<GameBoss>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	///	プレイヤーを追いかけるステート
	//--------------------------------------------------------------------------------------
	class BossSeek : public ObjState<GameBoss>
	{
		BossSeek() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(BossSeek)
		virtual void Enter(const shared_ptr<GameBoss>& Obj)override;
		virtual void Execute(const shared_ptr<GameBoss>& Obj)override;
		virtual void Exit(const shared_ptr<GameBoss>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	class BossFire : public ObjState<GameBoss>

	{
			BossFire() {}
	public:
		DECLARE_SINGLETON_INSTANCE(BossFire)
		virtual void Enter(const shared_ptr<GameBoss>& Obj) override;
		virtual void Execute(const shared_ptr<GameBoss>& Obj)override;
		virtual void Exit(const shared_ptr<GameBoss>& Obj) override;
			
	};


}