#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class SSAnime :  public SS5ssae;
	//	用途: spritestudio5画像のアニメーション
	//--------------------------------------------------------------------------------------
	//構築と消滅
	SSAnime::SSAnime(const shared_ptr<Stage>& StagePtr, const wstring& BaseDir,
		const wstring& SSAEDir, const wstring& AnimDir,
		const Vector3& Position,const Vector3& Scale) :
		SS5ssae(StagePtr,BaseDir,SSAEDir,AnimDir, true),
		m_Position(Position),
		m_Scale(Scale)
	{}
	SSAnime::~SSAnime() {}
	//初期化
	void SSAnime::OnCreate() {

		//元となるオブジェクトからアニメーションオブジェクトへの行列の設定
		//作成されたグラフィックとの調整に使用
		Matrix4X4 mat;
		mat.DefTransformation(
			Vector3(1.0f, 1.0f, 1.0f),
			Vector3(0, 0, 0),
			Vector3(0, 0, 0)
		);
		SetToAnimeMatrix(mat);

		//位置の初期化
		auto PtrT = AddComponent<Transform>();
		//スプライト版のSpriteStdioは、サイズ32.0f基準のスプライトを作成するので
		//それに合わせてスケーリングする
		PtrT->SetScale(m_Scale);
		//ポジションはピクセル単位センター合わせ（固定）
		PtrT->SetPosition(m_Position);
		//親クラスのOnCreateを必ず呼ぶ
		SS5ssae::OnCreate();
		//値は秒あたりのフレーム数
		SetFps(30.0f);
	}
	void SSAnime::OnUpdate() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		UpdateAnimeTime(ElapsedTime);
	}

	//フェードイン・フェードアウトの作成

	//構築と破棄
	Fade::Fade(const shared_ptr<Stage>& StagePtr,
		const wstring& WorB, const wstring& INorOUT) :
		GameObject(StagePtr),
		m_WorB(WorB),
		m_INorOUT(INorOUT)
	{}

	//初期化
	void Fade::OnCreate() {

		//画面のサイズを取得する
		float width = static_cast<float>(App::GetApp()->GetGameWidth());
		float height = static_cast<float>(App::GetApp()->GetGameHeight());
		float HelfSize = 0.5f;

		//頂点配列(縦横1個ずつ表示・1枚の板ポリゴンの頂点）
		vector<VertexPositionColorTexture> vertices = {
			{ VertexPositionColorTexture(Vector3(-HelfSize, HelfSize,  0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, HelfSize,  0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(1.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vector3(-HelfSize, -HelfSize, 0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(0.0f, 1.0f)) },
			{ VertexPositionColorTexture(Vector3(HelfSize, -HelfSize, 0), Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2(1.0f, 1.0f)) },
		};

		//インデックス配列（1枚の板ポリゴンを表示するために必要）
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(width, height, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(0, 0, 0);

		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);

		//ゲームオブジェクト作成時に定義した文字列によってフェードの方法を決める
		//W = ホワイト　B = ブラック　IN = フェード院　OUT = フェードアウト
		if (m_WorB == L"W") {
			if (m_INorOUT == L"IN") {
				PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 1.0f));
			}
			else if (m_INorOUT == L"OUT") {
				PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 0.0f));
			}
		}
		else if (m_WorB == L"B") {
			if (m_INorOUT == L"IN") {
				PtrDraw->SetDiffuse(Color4(0.0f, 0.0f, 0.0f, 1.0f));
			}
			else if (m_INorOUT == L"OUT") {
				PtrDraw->SetDiffuse(Color4(0.0f, 0.0f, 0.0f, 0.0f));
			}
		}

		//FADEというテクスチャリソースを登録する
		PtrDraw->SetTextureResource(L"FADE_TX");
		//アルファをアクティブにすることで透過する
		SetAlphaActive(true);
		//描画するレイヤー順を指定　大きいほど手前に写る
		SetDrawLayer(50);
	}

	void Fade::OnUpdate() {
		//頂点とインデックスを指定してスプライト作成
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		auto Fader = PtrDraw->GetDiffuse();

		//実際にフェードを動かす部分
		if (m_WorB == L"W") {
			Fader.x = 1.0f; Fader.y = 1.0f, Fader.z = 1.0f;
			if (m_INorOUT == L"IN") {
				Fader.w -= ElapsedTime;
			}
			else if (m_INorOUT == L"OUT") {
				Fader.w += ElapsedTime;
			}
			if (Fader.w < 0.0f) {
				Fader.w = 0.0f;
			}
			else if (Fader.w > 1.0f) {
				Fader.w = 1.0f;
			}
			PtrDraw->SetDiffuse(Fader);
		}
		else if (m_WorB == L"B") {
			Fader.x = 0.0f; Fader.y = 0.0f, Fader.z = 0.0f;
			if (m_INorOUT == L"IN") {
				Fader.w -= ElapsedTime;
			}
			else if (m_INorOUT == L"OUT") {
				Fader.w += ElapsedTime;
			}
			if (Fader.w < 0.0f) {
				Fader.w = 0.0f;
			}
			else if (Fader.w > 1.0f) {
				Fader.w = 1.0f;
			}
			PtrDraw->SetDiffuse(Fader);
		}

	}

	//インかアウトを取得
	wstring Fade::GetFader()const {
		return m_INorOUT;
	}

	//これを設定することで一つのフェーダーでインとアウトを演出できる
	void Fade::SetFader(const wstring& INorOUT) {
		m_INorOUT = INorOUT;
	}


	//スプライトの作成構築
	Sprite::Sprite(const shared_ptr<Stage>& StagePtr, bool Trace,
		const Vector2& StartScale, const Vector3 & StartPos, const wstring& Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_Media(Media)
	{}
	//破棄
	Sprite::~Sprite() {}

	void Sprite::OnCreate() {
		float HalfSize = 0.5f;
		//頂点の配列
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1 ,3 ,2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos);

		//サウンド登録
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Decide");
		pMultiSoundEffect->AddAudioResource(L"Cursor");
		pMultiSoundEffect->AddAudioResource(L"Starting");

		//頂点とインデックスを指定してスプライト作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
	}

	void Sprite::OnUpdate() {
	}

	//-------------------------------------------------------------------------------------------------------
	//Spriteの作成構築
	BulletTypeSp::BulletTypeSp(const shared_ptr<Stage>& StagePtr, bool Trace,
		const Vector2& StartScale, const Vector3 & StartPos, const wstring& Media) :
		Sprite(StagePtr, Trace, StartScale, StartPos, Media)

		{}
	//破棄
	BulletTypeSp::~BulletTypeSp() {}

	void BulletTypeSp::OnUpdate() {

		B_TypeChanger();
	}

	void BulletTypeSp::B_TypeChanger() {
		auto Ptr = GetComponent<PTSpriteDraw>();

		//エネミー取った画像を
		switch (App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType)
		{
		case 0:
			Ptr->SetTextureResource(L"EMPTY_TX");
			break;
		case 1:
			Ptr->SetTextureResource(L"ENEMY_L_TX");
			break;
		case 2:
			Ptr->SetTextureResource(L"ENEMY_B_TX");
			break;
		case 3:
			Ptr->SetTextureResource(L"ENEMY_M_TX");
			break;
		default:
			break;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	//SelectSpriteの作成構築
	SelectSprite::SelectSprite(const shared_ptr<Stage>& StagePtr, bool Trace,
		const Vector2& StartScale, const Vector3 & StartPos, const wstring& Media) :
		Sprite(StagePtr, Trace, StartScale, StartPos, Media), m_Select(true), m_Select1(true),
		m_Param(SelectStgeParam::ToStage1)
	{}
	//破棄
	SelectSprite::~SelectSprite() {}

	void SelectSprite::OnUpdate() {

		//コントローラ取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].fThumbLX != 0) {
			//コントローラの向き計算
			float MoveX = CntlVec[0].fThumbLX;
			Vector2 MoveVec(MoveX, 0);

			if (CntlVec[0].bConnected) {
				//スティック
				if (CntlVec[0].fThumbLX > 0.5f && m_Select) {
					m_Select = false;
					switch (m_Param) {
					case SelectStgeParam::ToStage1:
						m_Param = SelectStgeParam::ToStage2;
						m_Media = L"SELECT2_TX";
						break;
					case SelectStgeParam::ToStage2:
						m_Param = SelectStgeParam::ToStage3;
						m_Media = L"SELECT3_TX";
						break;
					case SelectStgeParam::ToStage3:
						m_Param = SelectStgeParam::ToStage1;
						m_Media = L"SELECT_TX";
						break;
					default:
						break;
					}
					auto Ptr = GetComponent<PTSpriteDraw>();
					Ptr->SetTextureResource(m_Media);
				}
				else if (CntlVec[0].fThumbLX < 0.5f) {
					m_Select = true;
				}

				if (CntlVec[0].fThumbLX < -0.5f && m_Select1) {
					m_Select1 = false;
					switch (m_Param) {
					case SelectStgeParam::ToStage1:
						m_Param = SelectStgeParam::ToStage3;
						m_Media = L"SELECT3_TX";
						break;
					case SelectStgeParam::ToStage2:
						m_Param = SelectStgeParam::ToStage1;
						m_Media = L"SELECT_TX";
						break;
					case SelectStgeParam::ToStage3:
						m_Param = SelectStgeParam::ToStage2;
						m_Media = L"SELECT2_TX";
						break;
					default:
						break;
					}
					auto Ptr = GetComponent<PTSpriteDraw>();
					Ptr->SetTextureResource(m_Media);
				}
				else if (CntlVec[0].fThumbLX > -0.5) {
					m_Select1 = true;
				}
			}
		}
	}

	BoosterSprite::BoosterSprite(const shared_ptr<Stage>& StagePtr, bool Trace,
		const Vector2& StartScale, const Vector3 & StartPos, const wstring& Media, const shared_ptr<Enemy>& EnemyPtr) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_Media(Media),
		m_EnemyPtr(EnemyPtr)

	{}
	//破棄
	BoosterSprite::~BoosterSprite() {}

	void BoosterSprite::OnCreate() {
		//float HalfSize = 0.5f;
		////頂点の配列
		//m_BackupVertices = {
		//	{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
		//	{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
		//	{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
		//	{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		//};

		////インデックス配列
		//vector<uint16_t> indices = { 0, 1, 2, 1 ,3 ,2 };
		//SetAlphaActive(m_Trace);
		//auto PtrTransform = GetComponent<Transform>();
		//PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		//PtrTransform->SetRotation(0, 0, 0);
		//PtrTransform->SetPosition(m_StartPos);

		////頂点とインデックスを指定してスプライト作成
		//auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		//Ptr->SetSamplerState(SamplerState::LinearWrap);
		//Ptr->SetTextureResource(m_Media);

		for (int i = 0; i < 11; i++)
		{
			//頂点配列
			vector<VertexPositionNormalTexture> vertices;
			//インデックスを作成するための配列
			vector<uint16_t> indices;
			//Squareの作成(ヘルパー関数を利用)
			MeshUtill::CreateSquare(1.0f, vertices, indices);
			//UV値の変更
			float from = i / 11.0f;
			float to = from + (1.0f / 11.0f);
			//左上頂点
			vertices[0].textureCoordinate = Vector2(from, 0);
			//右上頂点
			vertices[1].textureCoordinate = Vector2(to, 0);
			//左下頂点
			vertices[2].textureCoordinate = Vector2(from, 1.0f);
			//右下頂点
			vertices[3].textureCoordinate = Vector2(to, 1.0f);
			//頂点の型を変えた新しい頂点を作成
			vector<VertexPositionColorTexture> new_vertices;
			for (auto& v : vertices) {
				VertexPositionColorTexture nv;
				nv.position = v.position;
				nv.color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
				nv.textureCoordinate = v.textureCoordinate;
				new_vertices.push_back(nv);
			}
			//メッシュ作成
			m_Mesh.push_back(MeshResource::CreateMeshResource<VertexPositionColorTexture>(new_vertices, indices, true));
		}



		//スプライトをつける
		auto DrawP = AddComponent<PCTStaticDraw>();

		DrawP->SetTextureResource(L"Boost_TX");
		DrawP->SetMeshResource(m_Mesh[9]);
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(m_StartPos);
		PtrTrans->SetScale(m_StartScale.x, m_StartScale.y, 1);
		SetAlphaActive(true);
		//左上頂点
		//PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftTopZeroPlusDownY);

		//スプライト中のメッシュから数字の画像を取得

		auto Group = GetStage()->GetSharedObjectGroup(L"Boost");
		Group->IntoGroup(GetThis<BoosterSprite>());


	}

	void BoosterSprite::OnUpdate() {


		auto Trans = GetComponent<Transform>();
		//ローテーション持ってくる
		auto Rot = Trans->GetRotation();
		//ポジション持ってくる
		auto Pos = Trans->GetPosition();
		//ターゲットカメラ持ってくる
		auto PtrCamera = GetStage()->GetView()->GetTargetCamera();

		//シーン呼ぶ
		auto ScenePtr = App::GetApp()->GetScene<Scene>();

		//auto PtrBoost = GetStage()->GetSharedObjectGroup(L"Boost", false);
		//	Quaternion Qt;
		//向きをビルボードにする
		//	Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());

		//向きをフェイシングにする場合は以下のようにする
		// Qt.Facing(Pos - PtrCamera->GetEye());
		//向きをフェイシングYにする場合は以下のようにする
		// Qt.FacingY(Pos - PtrCamera->GetEye());
		//向きをシークオブジェクトと同じにする場合は以下のようにする
		// Qt = SeekTransPtr->GetQuaternion();



		m_time += App::GetApp()->GetElapsedTime();
		//画像切り替え
		if (m_time > 0.05)
		{
			m_time = 0;
			GetComponent<PCTStaticDraw>()->SetMeshResource(m_Mesh[m_count]);
			m_count++;
			if (m_count > 10)
			{
				m_count = 0;
			}
		}

		auto EnemyTrans = m_EnemyPtr->GetComponent<Transform>();
		auto EnemyPos = EnemyTrans->GetPosition();
		auto EnemyRot = EnemyTrans->GetRotation();
		EnemyRot = Vector3(1.5, EnemyRot.y, 0.0f);
		Pos = EnemyPos;

		//Enemyにポジション合わせる
		Pos.x -= sin(EnemyRot.y) / 2;
		Pos.z -= cos(EnemyRot.y) / 2;
		//ローテーションをEnemyに合わせる
		Rot = EnemyRot;
		//Enemy死んだらBoost消す
		if (m_EnemyPtr->DestroyFLG)
		{
			SetDrawActive(false);
			SetUpdateActive(false);
		}
		if (m_EnemyPtr->Catch)
		{
			SetDrawActive(false);
			SetUpdateActive(false);
		}

		Trans->SetPosition(Pos);
		//Trans->SetQuaternion(Qt);
		Trans->SetRotation(Rot);




	}





	ExplosionSprite::ExplosionSprite(const shared_ptr<Stage>& StagePtr, bool Trace,
		const Vector2& StartScale, const Vector3 & StartPos, const wstring& Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_Media(Media)

	{}
	//破棄
	ExplosionSprite::~ExplosionSprite() {}

	void ExplosionSprite::OnCreate() {
		////描画コンポーネントの追加
		//auto DrawComp = AddComponent<PNTStaticDraw>();
		////描画コンポーネントに形状（メッシュ）を設定
		//DrawComp->SetMeshResource(L"DEFAULT_SQUARE");

		//auto DrawComp = Ptr->AddComponent<BcPNTStaticDraw>();
		//DrawComp->SetFogEnabled(true);
		////描画コンポーネントに形状（メッシュ）を設定
		//DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		for (int i = 0; i < 9; i++)
		{
			//頂点配列
			vector<VertexPositionNormalTexture> vertices;
			//インデックスを作成するための配列
			vector<uint16_t> indices;
			//Squareの作成(ヘルパー関数を利用)
			MeshUtill::CreateSquare(1.0f, vertices, indices);
			//UV値の変更
			float from = i / 9.0f;
			float to = from + (1.0f / 9.0f);
			//左上頂点
			vertices[0].textureCoordinate = Vector2(from, 0);
			//右上頂点
			vertices[1].textureCoordinate = Vector2(to, 0);
			//左下頂点
			vertices[2].textureCoordinate = Vector2(from, 1.0f);
			//右下頂点
			vertices[3].textureCoordinate = Vector2(to, 1.0f);
			//頂点の型を変えた新しい頂点を作成
			vector<VertexPositionColorTexture> new_vertices;
			for (auto& v : vertices) {
				VertexPositionColorTexture nv;
				nv.position = v.position;
				nv.color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
				nv.textureCoordinate = v.textureCoordinate;
				new_vertices.push_back(nv);
			}
			//メッシュ作成
			m_Mesh.push_back(MeshResource::CreateMeshResource<VertexPositionColorTexture>(new_vertices, indices, true));
		}



		//スプライトをつける
		auto DrawP = AddComponent<PCTStaticDraw>();
		DrawP->SetTextureResource(L"Explosion_TX");
		DrawP->SetMeshResource(m_Mesh[8]);
		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetPosition(m_StartPos);
		PtrTrans->SetScale(m_StartScale.x, m_StartScale.y, 1);
		SetAlphaActive(true);
		SetDrawLayer(2);

		//左上頂点
		//PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftTopZeroPlusDownY);

		//スプライト中のメッシュから数字の画像を取得
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		m_Down = ScenePtr->EnemyPos;

	}

	void ExplosionSprite::OnUpdate() {
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto PtrTransform = GetComponent<Transform>();
		//カメラを得る
		//auto PtrCamera = dynamic_pointer_cast<MainCamera>(OnGetDrawCamera());
		auto PtrCamera = GetStage()->GetView()->GetTargetCamera();

		Quaternion Qt;
		//向きをビルボードにする
		Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());
		PtrTransform->SetQuaternion(Qt);

		m_time += App::GetApp()->GetElapsedTime();
		if (m_time > 0.05)
		{
			m_time = 0;
			GetComponent<PCTStaticDraw>()->SetMeshResource(m_Mesh[m_count]);
			m_count++;
			if (m_count > 8)
			{
				m_count = 0;
			}
		}

		if (ScenePtr->DestroyFLG)
		{
			DestroyTime--;

			if (DestroyTime <= 0)
			{
				SetDrawActive(false);
				SetUpdateActive(false);
			}

		}
	}



	//-------------------------
	//　分割表示のスプライト
	//	UV値を変化させることで画像の表示させたいところを表示させる
	//	
	//-----------------------
	SplitSprite::SplitSprite(const shared_ptr<Stage>& StagePtr,
		const wstring& TextureKey, const bool& Trace,
		const Vector2& Scale, const Vector2& Position,
		const UINT& VerticalCount, const UINT& HorizontalCount) :
		GameObject(StagePtr),
		//テクスチャ登録
		m_TextureKey(TextureKey),
		//透過するか
		m_Trace(Trace),
		//スプライトの大きさ
		m_Scale(Scale),
		//画像の場所
		m_Pos(Position),
		//垂直方向の分割数
		m_VerticalCount(VerticalCount),
		//水平方向の分割数
		m_HorizontalCount(HorizontalCount)
	{}
	SplitSprite::~SplitSprite() {}

	void SplitSprite::OnCreate() {

		//分割数が0以下の時に例外処理を投げる
		if (m_VerticalCount <= 0 || m_HorizontalCount <= 0) {
			throw BaseException(
				L"VerticalCountか",
				L"HorizontalCountが不正な値です。",
				L"1以上で設定してください。"
			);
		}

		auto PtrTrans = AddComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		//スプライトを付加
		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		//テクスチャのキーを登録
		PtrSprite->SetTextureResource(m_TextureKey);
		//透過処理
		SetAlphaActive(m_Trace);
		//左上を頂点とする
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//型変換をする
		float VerticalSize = (float)m_VerticalCount;
		float HorizontalSize = (float)m_HorizontalCount;

		//左からカウントし始め、右端まで行ったら下に移動してカウント始める
		//カウントしたものを頂点データとして配列化
		for (size_t i = 0; i < m_VerticalCount; i++) {
			for (size_t j = 0; j < m_HorizontalCount; j++) {
				//X座標
				float fromX = ((float)j) / HorizontalSize;
				float toX = fromX + (1.0f / HorizontalSize);
				//Y座標
				float fromY = ((float)i) / VerticalSize;
				float toY = fromY + (1.0f / VerticalSize);
				//頂点データ登録
				vector<VertexPositionColorTexture> NumVertex =
				{
					//左上頂点データ
					VertexPositionColorTexture(
						SpVertexVec[0].position,
						Color4(1.0f,1.0f,1.0f,1.0f),
						Vector2(fromX,fromY)
					),
					//右上頂点データ
					VertexPositionColorTexture(
						SpVertexVec[1].position,
						Color4(1.0f,1.0f,1.0f,1.0f),
						Vector2(toX,fromY)
					),
					//左下頂点データ
					VertexPositionColorTexture(
						SpVertexVec[2].position,
						Color4(1.0f,1.0f,1.0f,1.0f),
						Vector2(fromX,toY)
					),
					//右下頂点データ
					VertexPositionColorTexture(
						SpVertexVec[3].position,
						Color4(1.0f,1.0f,1.0f,1.0f),
						Vector2(toX,toY)
					),
				};
				m_BackupVertices.push_back(NumVertex);
			}
		}
	}

	void SplitSprite::SelectNumPos() {
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//コントローラの向き計算
		float MoveX = CntlVec[0].fThumbLX;
		float MoveY = CntlVec[0].fThumbLY;
		//動かす
		if (CntlVec[0].bConnected) {
			if (CntlVec[0].fThumbLX != 0 || CntlVec[0].fThumbLY != 0) {
				if (MoveX > 0.5f&& IsStick == false) {
					m_Num++;
					IsStick = true;
				}
				else if (MoveX < -0.5f&& IsStick == false) {
					m_Num--;
					IsStick = true;
				}
				else if (MoveY > 0.5f&& IsStick == false) {
					m_Num -= m_HorizontalCount;
					IsStick = true;
				}
				else if (MoveY < -0.5f&& IsStick == false) {
					m_Num += m_HorizontalCount;
					IsStick = true;
				}
			}
			else {
				IsStick = false;
			}
		}
	}

	void SplitSprite::OnUpdate() {
		//画像の場所を選択する
		SelectNumPos();

		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		//数字が一定範囲を超えた時にエラーを起こさないように数字を指定する
		if (m_Num < 0) {
			m_Num = (int)(m_VerticalCount*m_HorizontalCount) - 1;
		}
		else if (m_Num >(int)(m_VerticalCount*m_HorizontalCount) - 1) {
			m_Num = 0;
		}
		//動的にUV値が変わるよう設定
		MeshRes->UpdateVirtexBuffer(m_BackupVertices[m_Num]);
	}


	//HPスプライトの作成
	HPSprite::HPSprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	HPSprite::~HPSprite() {}

	void HPSprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
	}

	void HPSprite::OnUpdate() {
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		m_TotalTime += ElapsedTime;
		auto PtrTrans = GetComponent<Transform>();

		auto NowPos = PtrTrans->GetPosition();
		auto NowScale = PtrTrans->GetScale();
		if (Flag_Hit) {
			float SpanX = m_Scale.x / 6.0f;
			float NewScaleX = NowScale.x - SpanX;
			PtrTrans->SetScale(NewScaleX, m_Scale.y, 0.1f);
			Vector3 Pos = m_Pos;
			Pos.x -= m_Scale.x / 2.0f;
			Pos.x += NewScaleX / 2.0f;

			PtrTrans->SetPosition(Pos);
		}
		Flag_Hit = false;
	}

	//弾の弾数UI

	RemaingBullet::RemaingBullet(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	RemaingBullet::~RemaingBullet() {}

	void RemaingBullet::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
	}

	void RemaingBullet::OnUpdate() {

		auto PtrTrans = GetComponent<Transform>();
		auto NowPos = PtrTrans->GetPosition();
		auto NowScale = PtrTrans->GetScale();
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");

		//プレイヤーの現在の残弾数を持ち込む
		auto BulletNow = PlayerPtr->GetRemainsBullet();
		//スケールを変える
		auto GageSize = m_Scale.x;
		if (BulletNow != 0) {
		GageSize -= m_Scale.x / BulletNow;
		}
		else {
			GageSize = 0;
		}
		//
		Vector3 Pos = NowPos;
		Pos.x -= m_Scale.x / 2.0f;
		Pos.x += GageSize / 2.0f;
		//
		PtrTrans->SetScale(GageSize, m_Scale.y, 0.1f);
		PtrTrans->SetPosition(Pos);

		////敵のタイプによってテクスチャリソースを変更する
		//switch (App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType)
		//{
		//case 0:
		//	Max = 0;
		//	break;
		//case 1:
		//	Max = 100;
		//	break;
		//case 2:
		//	Max = 100;
		//	break;
		//case 3:
		//	Max = 50;
		//	break;
		//default:
		//	break;
		//}
	}




	//--------------------------------------------------------------------------------------
	///	連番アニメーションクラス
	//--------------------------------------------------------------------------------------
	SerialAnimeObject::SerialAnimeObject(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos,
		const wstring& TexResKey, size_t XPieceCount, size_t YPieceCount) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_TexResKey(TexResKey),
		m_XPieceCount(XPieceCount),
		m_YPieceCount(YPieceCount),
		m_PieceNowNumber(0),
		m_PieceBySecond(20),
		m_PieceTotalTime(0),
		m_IsRun(false)
	{}

	void SerialAnimeObject::CreateSquareMesh() {
		//頂点配列
		vector<VertexPositionNormalTexture> vertices;

		//インデックスを作成するための配列
		vector<uint16_t> indices;
		//Squareの作成(ヘルパー関数を利用)
		MeshUtill::CreateSquare(1.0f, vertices, indices);
		//UV値の変更
		float PicX = 1.0f / (float)m_XPieceCount;
		float PicY = 1.0f / (float)m_YPieceCount;
		//左上頂点
		vertices[0].textureCoordinate = Vector2(0, 0);
		//右上頂点
		vertices[1].textureCoordinate = Vector2(PicX, 0);
		//左下頂点
		vertices[2].textureCoordinate = Vector2(0, PicY);
		//右下頂点
		vertices[3].textureCoordinate = Vector2(PicX, PicY);
		//頂点の型を変えた新しい頂点を作成
		m_BackupVertices.clear();
		for (auto& v : vertices) {
			VertexPositionColorTexture nv;
			nv.position = v.position;
			nv.color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
			nv.textureCoordinate = v.textureCoordinate;
			m_BackupVertices.push_back(nv);
		}
		//新しい頂点を使ってメッシュリソースの作成
		m_SquareMeshResource
			= MeshResource::CreateMeshResource<VertexPositionColorTexture>(m_BackupVertices, indices, true);
	}


	void SerialAnimeObject::OnCreate() {
		CreateSquareMesh();

		//初期位置などの設定
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(2.0f, 2.0f, 2.0f);	//直径25センチの球体
		Ptr->SetPosition(m_StartPos);

		auto PtrCamera = GetStage()->GetView()->GetTargetCamera();
		Quaternion Qt;
		//向きをビルボードにする
		Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());

		//向きをフェイシングにする場合は以下のようにする
		// Qt.Facing(Pos - PtrCamera->GetEye());
		//向きをフェイシングYにする場合は以下のようにする
		// Qt.FacingY(Pos - PtrCamera->GetEye());
		//向きをシークオブジェクトと同じにする場合は以下のようにする
		// Qt = SeekTransPtr->GetQuaternion();

		Ptr->SetQuaternion(Qt);




		auto DrawComp = AddComponent<PCTStaticDraw>();
		DrawComp->SetMeshResource(m_SquareMeshResource);
		DrawComp->SetTextureResource(m_TexResKey);
		DrawComp->SetBlendState(BlendState::Additive);
		SetDrawLayer(1);
		//	SetAlphaActive(true);
		Start();
	}

	void SerialAnimeObject::OnUpdate() {
		if (m_IsRun) {
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			//1ピース当たりの再生時間を計算
			float OnPieceTime = 1.0f / m_PieceBySecond;
			m_PieceTotalTime += ElapsedTime;
			if (m_PieceTotalTime >= OnPieceTime) {
				m_PieceTotalTime = 0;
				size_t maxNu = m_XPieceCount * m_YPieceCount;
				m_PieceNowNumber++;
				if (m_PieceNowNumber >= maxNu) {
					m_PieceNowNumber = 0;
				}
			}
			float PicX = 1.0f / (float)m_XPieceCount;
			float PicY = 1.0f / (float)m_YPieceCount;
			size_t UFrom = m_PieceNowNumber % m_XPieceCount;
			size_t VFrom = m_PieceNowNumber / m_XPieceCount;
			float UFromF = PicX * (float)UFrom;
			float VFromF = PicY * (float)VFrom;
			float UToF = UFromF + PicX;
			float VToF = VFromF + PicY;

			//左上頂点
			m_BackupVertices[0].textureCoordinate = Vector2(UFromF, VFromF);
			//右上頂点
			m_BackupVertices[1].textureCoordinate = Vector2(UToF, VFromF);
			//左下頂点
			m_BackupVertices[2].textureCoordinate = Vector2(UFromF, VToF);
			//右下頂点
			m_BackupVertices[3].textureCoordinate = Vector2(UToF, VToF);

			auto PtrDraw = GetComponent<PCTStaticDraw>();
			m_SquareMeshResource->UpdateVirtexBuffer(m_BackupVertices);

			//初期位置などの設定
			auto Ptr = GetComponent<Transform>();
			auto PlayerPos = GetStage()->GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetPosition();
			//位置変更する場合はここで変更
			Ptr->SetPosition(PlayerPos);
			auto PtrCamera = GetStage()->GetView()->GetTargetCamera();
			Quaternion Qt;
			//向きをビルボードにする
			Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());
			Ptr->SetQuaternion(Qt);


		}
	}


}
//end basecross
