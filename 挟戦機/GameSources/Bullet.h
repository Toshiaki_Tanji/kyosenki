#pragma once
#include"stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	///	飛んでいくボール
	//--------------------------------------------------------------------------------------
	class AttackBall : public GameObject {
	public:
		//構築と破棄
		AttackBall(const shared_ptr<Stage>& StagePtr);
		virtual ~AttackBall();
		void Weakup(const Vector3& Position, const Quaternion& Rotation, const Vector3& Velocity);
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
	};

	//--------------------------------------------------------------------------------------
	///	飛んでいくレーザー
	//--------------------------------------------------------------------------------------
	class AttackLaser : public GameObject {
	public:
		//構築と破棄
		AttackLaser(const shared_ptr<Stage>& StagePtr);
		virtual ~AttackLaser();
		void Weakup(const Vector3& Position, const Quaternion& Rotation, const Vector3& Velocity);
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
	};

	//--------------------------------------------------------------------------------------
	///	飛んでいくミサイル
	//--------------------------------------------------------------------------------------
	class AttackMissile : public GameObject {
		float m_ExplodeTime = 1.0f;
		float m_FlareTime = 0.5f;
		bool SeOneShot = true;
		Vector3 m_Scale = Vector3(3.0f, 3.0f, 3.0f);
	public:
		//構築と破棄
		AttackMissile(const shared_ptr<Stage>& StagePtr);
		virtual ~AttackMissile();
		void Weakup(const Vector3& Position, const Quaternion& Rotation, const Vector3& Velocity);
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		//爆発演出
		void HitAndExplosion();
		//爆発状態になる判定
		bool IsExplosion();
		//
	};


	//--------------------------------------------------------------------------------------
	///	飛んでいくボール
	//--------------------------------------------------------------------------------------
	class EnemyAttackBall : public GameObject {
	public:
		//構築と破棄
		EnemyAttackBall(const shared_ptr<Stage>& StagePtr);
		virtual ~EnemyAttackBall();
		void Weakup(const Vector3& Position, const Quaternion& Rotation, const Vector3& Velocity);
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		float dx = 0;
		float dy = 0;
		float dz = 0;
		float angle = 0;
	};
	//--------------------------------------------------------------------------------------
	///	飛んでいくレーザー
	//--------------------------------------------------------------------------------------
	class EnemyAttackLaser : public GameObject {
	public:
		//構築と破棄
		EnemyAttackLaser(const shared_ptr<Stage>& StagePtr);
		virtual ~EnemyAttackLaser();
		void Weakup(const Vector3& Position, const Quaternion& Rotation, const Vector3& Velocity);
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
	};



	class LaserBallEffect : public GameObject {

	protected:
		//透過処理をするか
		bool m_Trace;
		Vector2 m_StartScale;
		Vector3 m_StartPos;
		//画像のテクスチャのキー登録
		wstring m_Media;
		//頂点配列の設定
		vector<vector<VertexPositionColorTexture>> m_BackupVertices;
		vector<shared_ptr<MeshResource>> m_Mesh;

	public:
		//
		LaserBallEffect(const shared_ptr<Stage> & StagePtr, bool Trace,
			const Vector2& StartScale, const Vector3& StartPos, const wstring& Media);
		virtual ~LaserBallEffect();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		float m_time = 0;
		int m_count = 0;

		bool DestroyFLG = false;


	};



}