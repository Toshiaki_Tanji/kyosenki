/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {





	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player::Player(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_RemainsBullets(0),
		m_FireInterval(0.0f),
		m_isFire(false),
		m_Armor(6)
	{}

	//初期化
	void Player::OnCreate() {
		//・ｽT・ｽE・ｽ・ｽ・ｽh
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"MachineGun");


		//初期位置などの設定
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(0.25f, 0.25f, 0.25f);	//直径25センチの球体
		Ptr->SetRotation(0.0f, 0.0f, 0.0f);
		Ptr->SetPosition(m_StartPos);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();
		PtrRedid->SetReflection(0.0f);
		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::Auto);
		//PtrCol->SetDrawActive(true);
		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(32.0f, 32.0f, 640.0f, 480.0f));


		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.75f, 0.75f, 0.75f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.0f, 0.0f)
		);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"PLAYER_MESH");
		//描画するテクスチャを設定
		PtrDraw->SetTextureResource(L"PLAYER_TX");
		PtrDraw->SetMeshToTransformMatrix(SpanMat);

		//透明処理
		SetAlphaActive(true);

		//パラメータにプレイヤーの初期体力を入れる
		App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PlayerHP = m_Armor;

		//カメラを得る
		auto PtrCamera = dynamic_pointer_cast<MainCamera>(OnGetDrawCamera());
		if (PtrCamera) {
			//GameCameraである
			//GameCameraに注目するオブジェクト（プレイヤー）の設定
			PtrCamera->SetTargetObject(GetThis<GameObject>());

		}
		//ステートマシンの構築
		m_StateMachine.reset(new LayeredStateMachine<Player>(GetThis<Player>()));
		//最初のステートをPlayerDefaultにリセット
		m_StateMachine->Reset(PlayerDefaultState::Instance());
	}

	//更新
	void Player::OnUpdate() {
		//コントローラチェックして入力があればコマンド呼び出し
		m_InputHandler.PushHandle(GetThis<Player>());
		//ステートマシン更新
		m_StateMachine->Update();


		//パラメータにプレイヤーの初期体力を入れる
		m_Armor = App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PlayerHP;



		//App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType = 1;


		//弾を撃ち続ける処理
		FireLoop();

		auto PtrFunnelR = GetStage()->GetSharedGameObject<CatchFunnel>(L"FUNNEL_R");
		auto PtrFunnelL = GetStage()->GetSharedGameObject<CatchFunnel>(L"FUNNEL_L");

		//体力が0になった時にファンネルもろとも描画と更新停止
		if (m_Armor <= 0) {
			this->SetDrawActive(false);
			this->SetUpdateActive(false);
			PtrFunnelR->SetDrawActive(false);
			PtrFunnelR->SetUpdateActive(false);
			PtrFunnelL->SetDrawActive(false);
			PtrFunnelL->SetUpdateActive(false);
		}

		//ダメージを食らった時のモーション
		DamageMotion();
	}

	//後更新
	void Player::OnUpdate2() {

		//エフェクト
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		auto Trans = GetComponent<Transform>();
		auto PlayerRot = Trans->GetRotation();
		auto PlayerPos = GetComponent<Transform >()->GetPosition();
		auto EffectPos = Vector3(PlayerPos.x, PlayerPos.y, PlayerPos.z);
		EffectPos.x -= sin(PlayerRot.y) / 2;
		EffectPos.z -= cos(PlayerRot.y) / 2;
		//EffectPos.z += 0.5;
		auto PtrEffect = GetStage()->GetSharedGameObject<KirakiraEffect>(L"PLEFFECT", false);
		PtrEffect->InsertEffect(EffectPos);

		DrawStrings();

	}


	void Player::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		for (auto &v : OtherVec) {
			auto shEnemy = dynamic_pointer_cast<Enemy>(v);
			auto shBullet = dynamic_pointer_cast<AttackBall>(v);
			auto shLaser = dynamic_pointer_cast<AttackLaser>(v);
			auto shMissile = dynamic_pointer_cast<AttackMissile>(v);

			auto HPUIPtr = GetStage()->GetSharedGameObject<HPSprite>(L"HPUI", false);
			if ((shEnemy || shBullet || shLaser || shMissile) && !IsDamaged) {
				IsDamaged = true;
				App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PlayerHP--;
				HPUIPtr->Flag_Hit = true;
			}
		}
	}

	void Player::DamageMotion() {
		//描画コンポーネントの設定
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		//auto PtrCol = GetComponent<CollisionObb>();
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
	
		if (IsDamaged) {
			m_flashinterval += ElapsedTime;
			m_invisibleTime += ElapsedTime;
			if (m_invisibleTime > 2.0f) {
				IsDamaged = false;
				m_invisibleTime = 0.0f;
				ClearChanger = 1;
			}
			else if (m_flashinterval > 0.02f) {
				m_flashinterval = 0;
				ClearChanger *= -1;
			}
			//PtrCol->AddExcludeCollisionTag(L"EnemyCol");
			PtrDraw->SetDiffuse(Color4(1, 1, 1, ClearChanger));
		}
	}


	//Bボタン
	void  Player::OnPushA() {
		if (GetStateMachine()->GetTopState() == PlayerDefaultState::Instance()) {
			//弾がなかった時
			if (m_RemainsBullets <= 0) return;
			m_FireInterval = 1.0f;
			m_isFire = true;
		}
	}

	void Player::OutReleaseA() {
		m_isFire = false;
		m_FireInterval = 0.0f;
	}

	void Player::FireLoop() {

		if (m_RemainsBullets <= 0)return;

		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		switch (App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType)
		{
		case 1:
			if (m_isFire == true) {
				m_FireInterval += ElapsedTime;
				if (m_FireInterval > 0.01f) {
					GetStateMachine()->Push(PlayerAttackState::Instance());
					m_RemainsBullets--;
					m_FireInterval = 0.0f;
				}
			}
			break;
		case 2:
			if (m_isFire == true) {
				m_FireInterval += ElapsedTime;
				if (m_FireInterval > 0.1f) {
					GetStateMachine()->Push(PlayerAttackState::Instance());
					m_RemainsBullets--;
					m_FireInterval = 0.0f;
				}
			}
			break;
		case 3:
			if (m_isFire == true) {
				m_FireInterval += ElapsedTime;
				if (m_FireInterval > 0.75f) {
					GetStateMachine()->Push(PlayerAttackState::Instance());
					m_RemainsBullets--;
					m_FireInterval = 0.0f;
				}
			}
			break;
		default:
			break;
		}
	}

	//文字列の表示
	void Player::DrawStrings() {



		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring FPS(L"FPS: ");
		FPS += Util::UintToWStr(fps);
		FPS += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		FPS += Util::FloatToWStr(ElapsedTime);
		FPS += L"\n";

		wstring RemainsBullet(L"Bullet:\t");
		auto Remainds = m_RemainsBullets;//App::GetApp()->GetScene<Scene>()->GetGameParametors().m_BulletGain;
		RemainsBullet += L"Remainds =" + Util::IntToWStr(Remainds, Util::NumModify::Dec) + L"\n";

		wstring Enemy(L"Enemy:\t");
		auto EnemyType = App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType;
		Enemy += L"Type =" + Util::IntToWStr(EnemyType, Util::NumModify::Dec) + L"\n";


		auto ScenePtr = App::GetApp()->GetScene<Scene>();

		wstring Exist(L"EnemyExist:\t");
		auto EnNum = ScenePtr->DestroyCount;//App::GetApp()->GetScene<Scene>()->GetGameParametors().m_BossHP;
		Exist += L"EnNum =" + Util::IntToWStr(EnNum, Util::NumModify::Dec) + L"\n";

		wstring Boss(L"BOSS:\t");
		auto BossHP = App::GetApp()->GetScene<Scene>()->GetGameParametors().m_BossHP;
		Boss += L"HP =" + Util::IntToWStr(BossHP, Util::NumModify::Dec) + L"\n";


		wstring str = FPS + RemainsBullet + Enemy + Exist + Boss;
		//文字列をつける
		auto PtrString = GetComponent<StringSprite>();
		//PtrString->SetText(str);
	}

	int Player::GetRemainsBullet() {
		return m_RemainsBullets;
	}
	void Player::SetRemainsBullet(int bullets) {
		m_RemainsBullets = bullets;
	}


	//--------------------------------------------------------------------------------------
	///	通常ステート
	//--------------------------------------------------------------------------------------

	IMPLEMENT_SINGLETON_INSTANCE(PlayerDefaultState)

		void PlayerDefaultState::Enter(const shared_ptr<Player>& Obj) {
		//何もしない
	}

	void PlayerDefaultState::Execute(const shared_ptr<Player>& Obj) {
		auto PtrBehavior = Obj->GetBehavior<PlayerBehavior>();
		PtrBehavior->MovePlayer();
		auto PtrGrav = Obj->GetBehavior<Gravity>();
		PtrGrav->Execute();
	}

	void PlayerDefaultState::Exit(const shared_ptr<Player>& Obj) {
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	///	アタックステート
	//--------------------------------------------------------------------------------------
	IMPLEMENT_SINGLETON_INSTANCE(PlayerAttackState)
		void PlayerAttackState::Enter(const shared_ptr<Player>& Obj) {
		auto PtrBehavior = Obj->GetBehavior<PlayerBehavior>();
		auto pMultiSoundEffect = Obj->GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"MachineGun", 0, 0.1f);
		PtrBehavior->MovePlayer();
		switch (App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType)
		{
		case 1:
			PtrBehavior->FireAttackLaser();
			break;
		case 2:
			PtrBehavior->FireAttackBall();
			break;
		case 3:
			PtrBehavior->FireAttackMissile();
			break;
		default:
			break;
		}

	}

	void PlayerAttackState::Execute(const shared_ptr<Player>& Obj) {
		//すぐにステートを戻す
		Obj->GetStateMachine()->Pop();
	}

	void PlayerAttackState::Exit(const shared_ptr<Player>& Obj) {
		//何もしない
	}



	//--------------------------------------------------------------------------------------
	//	class CatchFunnel : public GameObject;
	//	用途: 挟んで捕獲するための子機	2017.04.20
	//--------------------------------------------------------------------------------------
	//構築と破棄
	CatchFunnel::CatchFunnel(const shared_ptr<Stage>& StagePtr,
		const wstring& tag) :
		GameObject(StagePtr),
		m_OpenClose(XM_PIDIV4 / 4),
		m_MoveDirect(1),
		m_Tag(tag),
		m_Bullet(0),
		IsCatch(true),
		m_IsMovement(false),
		IsPressA(false)
	{}
	CatchFunnel::~CatchFunnel() {}

	bool CatchFunnel::IsCatcher() {
		return IsCatch;
	}

	bool CatchFunnel::PressedA() {
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		auto Catced = App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PLGetEnemy;
		if (CntlVec[0].bConnected) {
			if (Catced == false) {
				//Aボタンが押されたら.
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
					m_IsMovement = true;
					IsPressA = true;
				}
				else if (CntlVec[0].wReleasedButtons & XINPUT_GAMEPAD_A) {
					IsPressA = false;
					m_IsMovement = true;
				}
			}
		}
		return IsPressA;
	}

	void CatchFunnel::PressedB() {
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			//Aボタンが押されたら.
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B) {
				App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PLGetEnemy = 0;
				App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType = 0;
				auto PLPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
				PLPtr->SetRemainsBullet(0);
			}
		}
	}

	void CatchFunnel::OpenCloseMove() {

		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		//Aボタンが押されたら
		if (PressedA()) {
			//離した
			IsCatch = false;
			//投げた
			IsThrow = true;
			m_OpenClose -= -ElapsedTime * 5;
		}
		//Aボタンを離すと
		else {
			//捕まえた
			IsCatch = true;
			//投げてない
			IsThrow = false;
			m_OpenClose -= ElapsedTime * 5;
		}
		//開閉の上限下限
		if (m_OpenClose > XM_PIDIV4) {
			m_IsMovement = false;
			m_OpenClose = XM_PIDIV4;
		}
		else if (m_OpenClose < XM_PIDIV4 / 4) {
			m_IsMovement = false;
			m_OpenClose = XM_PIDIV4 / 4;
			IsCatch = false;
		}

		auto PlayerTrans = GetStage()->GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>();
		auto PlayerPos = PlayerTrans->GetPosition();
		auto PlayerRot = PlayerTrans->GetRotation();
		auto PlayerQt = PlayerTrans->GetQuaternion();

		auto MyTrans = GetComponent<Transform>();
		auto MyPos = PlayerPos;
		if (m_Tag == L"RIGHTFUNNEL") {
			MyPos.x += sin(PlayerRot.y + m_OpenClose);
			MyPos.z += cos(PlayerRot.y + m_OpenClose);
		}
		else if (m_Tag == L"LEFTFUNNEL") {
			MyPos.x += sin(PlayerRot.y - m_OpenClose);
			MyPos.z += cos(PlayerRot.y - m_OpenClose);
		}

		MyPos.y = PlayerPos.y;

		MyTrans->SetQuaternion(PlayerQt);
		MyTrans->SetPosition(MyPos);

	}

	//敵を捕まえた時に弾を補充する演出を加える関数
	void CatchFunnel::CatchEnemy() {
		auto PLPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		m_Bullet += 500;
		PLPtr->SetRemainsBullet(m_Bullet);
	}

	//敵を投げるときの判定を取る関数
	bool CatchFunnel::ThrowEnemy() {
		if (IsThrow) {
			App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType = 0;
			return true;
		}
		else {
			return false;
		}
	}

	//初期化
	void CatchFunnel::OnCreate() {

		//初期位置などの設定
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(0.5f, 0.5f, 0.5f);	//直径10センチの立方体
		Ptr->SetRotation(0.0f, 0.0f, 0.0f);
		Ptr->SetPosition(0,0,0);

		//タグをつける
		AddTag(m_Tag);

		//Rigidbodyをつける
		//auto PtrRedid = AddComponent<Rigidbody>();
		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetIsHitAction(IsHitAction::Auto);
		PtrCol->SetFixed(true);
		//PtrCol->SetDrawActive(true);

		//影をつける（シャドウマップを描画する）
		//auto ShadowPtr = AddComponent<Shadowmap>();

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<BcPNTStaticDraw>();

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列

		if (FindTag(L"LEFTFUNNEL")) {
			SpanMat.DefTransformation(
				Vector3(0.75f, 0.75f, 0.75f),
				Vector3(0, XM_PI, 0),
				Vector3(0.0f, -0.0f, 0.0f)
			);
			////影の形（メッシュ）を設定
			//ShadowPtr->SetMeshResource(L"FUNNEL_LEFT_MESH");
			//ShadowPtr->SetMeshToTransformMatrix(SpanMat);

			//描画するメッシュを設定
			PtrDraw->SetMeshResource(L"FUNNEL_LEFT_MESH");
			//描画するテクスチャを設定
			PtrDraw->SetTextureResource(L"FUNNEL_TX");
			PtrDraw->SetMeshToTransformMatrix(SpanMat);

		}
		else if (FindTag(L"RIGHTFUNNEL")) {
			SpanMat.DefTransformation(
				Vector3(0.75f, 0.75f, 0.75f),
				Vector3(0, XM_PI, 0),
				Vector3(0.0f, -0.0f, 0.0f)
			);
			////影の形（メッシュ）を設定
			//ShadowPtr->SetMeshResource(L"FUNNEL_RIGHT_MESH");
			//ShadowPtr->SetMeshToTransformMatrix(SpanMat);
			//描画するメッシュを設定
			PtrDraw->SetMeshResource(L"FUNNEL_RIGHT_MESH");
			//描画するテクスチャを設定
			PtrDraw->SetTextureResource(L"FUNNEL_TX");
			PtrDraw->SetMeshToTransformMatrix(SpanMat);
		}
		//透明処理
		SetAlphaActive(true);
		
	}

	//更新
	void CatchFunnel::OnUpdate() {
		IsCatcher();
		OpenCloseMove();
		PressedB();
	}


}


//end basecross

