#pragma once
#include "stdafx.h"
#include "Project.h"
namespace basecross {


	//血のエフェクト
	//構築と破棄
	JaggyEffect::JaggyEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	JaggyEffect::~JaggyEffect() {}

	void JaggyEffect::InsertEffect(const Vector3& Pos) {
		auto ParticlePtr = InsertParticle(1);
		ParticlePtr->SetEmitterPos(Pos);
		//ParticlePtr->SetTextureResource(L"CIRCLE_TX");
		ParticlePtr->SetTextureResource(L"Electric_shock_TX");
		ParticlePtr->SetMaxTime(1.15f);
		//ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0;
			//rParticleSprite.m_LocalPos.y = 3;
			//rParticleSprite.m_LocalScale.x *= 5.5f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * -0.1f - 0.05f;

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0,
				rParticleSprite.m_LocalPos.y * 0,
				rParticleSprite.m_LocalPos.z * 50.0f
			);
			//	Quaternion Qt;
			///*
			//角度を変える場合、↑でParticlePtr->SetDrawOption(Particle::DrawOption::Normal);と宣言し
			//描画オプションを標準に切り替える必要がある。
			//また、XYZでは無く、ロールピッチヨーで角度を指定する必要がある。その際、角度はディグリーではなく
			//ラジアン角なので、<「目的の角度」/2π>で角度の変換を行わなければならない
			//*/
			//float Pitch = (90.0f / 180.0f * XM_PI);
			//// rParticleSprite.m_LocalQt.RotationRollPitchYaw(90.0f/180.0f * XM_PI, 0.0f, 0.0f);
			//rParticleSprite.m_LocalQt.RotationRollPitchYaw(Pitch, 0.0f, 0.0f);
			//
			////色の指定
			//rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	//サークルのエフェクト
	//構築と破棄
	CircleEffect::CircleEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	CircleEffect::~CircleEffect() {}

	void CircleEffect::InsertEffect(const Vector3& Pos) {
		SetAlphaActive(true);
		SetDrawLayer(0);


		auto ParticlePtr = InsertParticle(1);
		ParticlePtr->SetEmitterPos(Pos);
		//ParticlePtr->SetTextureResource(L"CIRCLE_TX");
		ParticlePtr->SetTextureResource(L"LineCircleC_TX");
		ParticlePtr->SetMaxTime(0.5f);
		ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.0f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1 - 0.5f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.0f;
			//	rParticleSprite.m_LocalScale *= -2.0f;
			rParticleSprite.m_LocalQt.AddRotation(Vector3(0.0f, 2.0f*-ElapsedTime, 0.0f));
			rParticleSprite.m_LocalQt.FacingY(Vector3(0.0f, 2.0f*ElapsedTime, 0.0f));
			rParticleSprite.m_LocalQt.RotationRollPitchYaw(1.0f, 1.0f, 2.0f*ElapsedTime);
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0,
				rParticleSprite.m_LocalPos.y + 2.0f,
				rParticleSprite.m_LocalPos.z * 0

			);
			Quaternion Qt;
			/*
			角度を変える場合、↑でParticlePtr->SetDrawOption(Particle::DrawOption::Normal);と宣言し
			描画オプションを標準に切り替える必要がある。
			また、XYZでは無く、ロールピッチヨーで角度を指定する必要がある。その際、角度はディグリーではなく
			ラジアン角なので、<「目的の角度」/2π>で角度の変換を行わなければならない
			*/
			float Pitch = (90.0f / 180.0f * XM_PI);
			// rParticleSprite.m_LocalQt.RotationRollPitchYaw(90.0f/180.0f * XM_PI, 0.0f, 0.0f);
			//前回のターンからの時間

			rParticleSprite.m_LocalQt.RotationRollPitchYaw(Pitch, Pitch*ElapsedTime, 0.0f);

			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}





	//血のエフェクト
	//構築と破棄
	KirakiraEffect::KirakiraEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	KirakiraEffect::~KirakiraEffect() {}

	void KirakiraEffect::InsertEffect(const Vector3& Pos) {
		auto ParticlePtr = InsertParticle(1);
		ParticlePtr->SetEmitterPos(Pos);
		//ParticlePtr->SetTextureResource(L"CIRCLE_TX");
		ParticlePtr->SetTextureResource(L"KIRAKIRA_TX");
		ParticlePtr->SetMaxTime(0.05f);
		//ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0;
			//rParticleSprite.m_LocalPos.y = 3;
			//rParticleSprite.m_LocalScale.x *= 5.5f;
			//rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0,
				rParticleSprite.m_LocalPos.y * 0,
				rParticleSprite.m_LocalPos.z * -50.0f
			);
			//	Quaternion Qt;
			///*
			//角度を変える場合、↑でParticlePtr->SetDrawOption(Particle::DrawOption::Normal);と宣言し
			//描画オプションを標準に切り替える必要がある。
			//また、XYZでは無く、ロールピッチヨーで角度を指定する必要がある。その際、角度はディグリーではなく
			//ラジアン角なので、<「目的の角度」/2π>で角度の変換を行わなければならない
			//*/
			//float Pitch = (90.0f / 180.0f * XM_PI);
			//// rParticleSprite.m_LocalQt.RotationRollPitchYaw(90.0f/180.0f * XM_PI, 0.0f, 0.0f);
			//rParticleSprite.m_LocalQt.RotationRollPitchYaw(Pitch, 0.0f, 0.0f);
			//
			////色の指定
			//rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}


	//113
	TorchEffect::TorchEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	TorchEffect::~TorchEffect() {}

	void TorchEffect::InsertEffect(const Vector3& Pos) {
		SetAlphaActive(true);
		SetDrawLayer(1);

		auto ParticlePtr = InsertParticle(1);
		ParticlePtr->SetEmitterPos(Pos);
		//ParticlePtr->SetTextureResource(L"CIRCLE_TX");
		ParticlePtr->SetTextureResource(L"MARU_TX");
		ParticlePtr->SetMaxTime(0.01f);
		//ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			//	rParticleSprite.m_LocalPos.x = Util::RandZeroToOne();
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0;
			//rParticleSprite.m_LocalPos.y = 3;
			//rParticleSprite.m_LocalScale.x *= 5.5f;
			//rParticleSprite.m_LocalPos.z = Util::RandZeroToOne();

			//各パーティクルの移動速度を指定
			/*rParticleSprite.m_Velocity = Vector3(
			rParticleSprite.m_LocalPos.x ,
			rParticleSprite.m_LocalPos.y ,
			rParticleSprite.m_LocalPos.z
			);*/
			//	Quaternion Qt;
			///*
			//角度を変える場合、↑でParticlePtr->SetDrawOption(Particle::DrawOption::Normal);と宣言し
			//描画オプションを標準に切り替える必要がある。
			//また、XYZでは無く、ロールピッチヨーで角度を指定する必要がある。その際、角度はディグリーではなく
			//ラジアン角なので、<「目的の角度」/2π>で角度の変換を行わなければならない
			//*/
			//float Pitch = (90.0f / 180.0f * XM_PI);
			//// rParticleSprite.m_LocalQt.RotationRollPitchYaw(90.0f/180.0f * XM_PI, 0.0f, 0.0f);
			//rParticleSprite.m_LocalQt.RotationRollPitchYaw(Pitch, 0.0f, 0.0f);
			//
			////色の指定
			rParticleSprite.m_LocalScale = Vector2(0.6f, 0.6f);
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);

		}
	}


	//血のエフェクト
	//構築と破棄
	HitEffect::HitEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	HitEffect::~HitEffect() {}

	void HitEffect::InsertEffect(const Vector3& Pos) {
		SetAlphaActive(true);
		SetDrawLayer(3);
		auto ParticlePtr = InsertParticle(4);
		ParticlePtr->SetEmitterPos(Pos);
		//ParticlePtr->SetTextureResource(L"CIRCLE_TX");
		ParticlePtr->SetTextureResource(L"WALL_PTX");
		ParticlePtr->SetMaxTime(0.25f);
		//ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;
			//rParticleSprite.m_LocalPos.y = 3;
			rParticleSprite.m_LocalScale.x -= 0.7f;
			rParticleSprite.m_LocalScale.y -= 0.7f;

			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 50.0f,
				rParticleSprite.m_LocalPos.y * 50.0f,
				rParticleSprite.m_LocalPos.z * 50.0f
			);
			//	Quaternion Qt;
			///*
			//角度を変える場合、↑でParticlePtr->SetDrawOption(Particle::DrawOption::Normal);と宣言し
			//描画オプションを標準に切り替える必要がある。
			//また、XYZでは無く、ロールピッチヨーで角度を指定する必要がある。その際、角度はディグリーではなく
			//ラジアン角なので、<「目的の角度」/2π>で角度の変換を行わなければならない
			//*/
			//float Pitch = (90.0f / 180.0f * XM_PI);
			//// rParticleSprite.m_LocalQt.RotationRollPitchYaw(90.0f/180.0f * XM_PI, 0.0f, 0.0f);
			//rParticleSprite.m_LocalQt.RotationRollPitchYaw(Pitch, 0.0f, 0.0f);
			//
			////色の指定
			//rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}






	//構築と破棄
	LaserEffect::LaserEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	LaserEffect::~LaserEffect() {}

	void LaserEffect::InsertEffect(const Vector3& Pos) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTransform = PlayerPtr->GetComponent<Transform>();
		auto PlayerRot = PlayerTransform->GetRotation();
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto PlayerVelo = PlayerPtr->GetComponent<Rigidbody>()->GetVelocity();
		auto Group = GetStage()->GetSharedObjectGroup(L"Laser");
		Group->IntoGroup(GetThis<LaserEffect>());
		//回転の計算
		auto RotY = PlayerTransform->GetRotation().y;
		auto Angle = Vector3(sin(RotY), 0, cos(RotY));
		Angle.Normalize();
		Quaternion Qt;
		Qt.RotationRollPitchYaw(0, RotY, 0);
		Qt.Normalize();
		auto Span = Angle * 1.5f;
		///auto Coll = AddComponent<CollisionRect>();
		//Coll->SetDrawActive(true);
		SetDrawLayer(3);
		auto ParticlePtr = InsertParticle(10, Particle::DrawOption::Normal);
		m_PtrLaser = ParticlePtr;
		ParticlePtr->SetEmitterPos(Pos);
		//ParticlePtr->SetTextureResource(L"CIRCLE_TX");
		ParticlePtr->SetTextureResource(L"LASER_TX");
		ParticlePtr->SetMaxTime(1.0f);
		ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			//	Rang += ElapsedTime;


			//レーザーの大きさ
			rParticleSprite.m_LocalScale.Set(0.5, 1.0);







			//	//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(


				//rParticleSprite.m_LocalScale = Vector3(1.0f, 1.0f, 1.0f)
				//	rParticleSprite.m_LocalScale *= Rang
				//rParticleSprite.m_LocalScale.y *= 1.0f*Rang,
				//回転の計算


				//PlayerVelo*3.5
				PlayerVelo + Angle*25.5f
				/*rParticleSprite.m_LocalPos.x * 0.0f,
				rParticleSprite.m_LocalPos.y * 0.0f,
				rParticleSprite.m_LocalPos.z * 25.0f*/
			);
			//Quaternion Qt;
			/*
			角度を変える場合、↑でParticlePtr->SetDrawOption(Particle::DrawOption::Normal);と宣言し
			描画オプションを標準に切り替える必要がある。
			また、XYZでは無く、ロールピッチヨーで角度を指定する必要がある。その際、角度はディグリーではなく
			ラジアン角なので、<「目的の角度」/2π>で角度の変換を行わなければならない
			*/
			//float Pitch = (90.0f / 180.0f * XM_PI);
			// rParticleSprite.m_LocalQt.RotationRollPitchYaw(90.0f/180.0f * XM_PI, 0.0f, 0.0f);
			//前回のターンからの時間
			//	auto RotX = Vector3(Pitch, 0.0f, 0.0f);
			//	auto RotateY = Vector3(0.0f, RotY, 0.0f);
			//	auto Rot = RotX*RotateY;

			//X軸回転
			Quaternion Qt(Vector3(1.0, 0.0f, 0), XM_PIDIV2);
			//Quaternion QtZ(Vector3(0, 0, 1), XM_PIDIV2);
			//Y軸回転
			Quaternion Qt2(Vector3(0, 1.0f, 0), RotY);
			//Quaternion同士をかける
			Qt *= Qt2;
			rParticleSprite.m_LocalQt = Qt;;
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	void LaserEffect::OnUpdate() {

		//最初に親のUpdateを呼んであげる必要がある
		MultiParticle::OnUpdate();




		auto Group = GetStage()->GetSharedObjectGroup(L"Enemys");
		for (size_t i = 0; i < Group->size(); i++) {
			if (Group->at(i)->IsUpdateActive()) {
				auto EnemyPtr = dynamic_pointer_cast<Enemy>(Group->at(i));
				auto EnemyPos = Group->at(i)->GetComponent<Transform>()->GetPosition();
				for (auto& rParticle : GetParticleVec()) {
					Vector3 EmitterPos = rParticle->GetEmitterPos();
					for (auto& Sp : rParticle->GetParticleSpriteVec()) {
						Vector3 Wpos = EmitterPos + Sp.m_LocalPos;
						float length = Vector3EX::Length(Wpos - EnemyPos);

						if (Group->at(i)->FindTag(L"BOSS") && length <= 1.5f)
						{
							EnemyPtr->SetDameged();
							return;
						}
						else if (length <= 0.6f)
						{
							EnemyPtr->SetDameged();
							return;
						}
					}
				}
			}
		}

		auto Boss = GetStage()->GetSharedGameObject<GameBoss>(L"AREABOSS");
		if (Boss->IsUpdateActive()) {
			auto BossPos = Boss->GetComponent<Transform>()->GetPosition();

			for (auto& rParticle : GetParticleVec()) {
				Vector3 EmitterPos = rParticle->GetEmitterPos();
				for (auto& Sp : rParticle->GetParticleSpriteVec()) {
					Vector3 Wpos = EmitterPos + Sp.m_LocalPos;
					float bossLength = Vector3EX::Length(Wpos - BossPos);
					if (bossLength <= 1.5f) {
						Boss->SetDameged();
						return;
					}
				}
			}
		}
	}


	//構築と破棄
	EnemyLaserEffect::EnemyLaserEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	EnemyLaserEffect::~EnemyLaserEffect() {}

	void EnemyLaserEffect::InsertEffect(const Vector3& Pos) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTransform = PlayerPtr->GetComponent<Transform>();
		auto PlayerRot = PlayerTransform->GetRotation();
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto PlayerVelo = PlayerPtr->GetComponent<Rigidbody>()->GetVelocity();
		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyLaser");
		Group->IntoGroup(GetThis<EnemyLaserEffect>());
		//回転の計算
		auto RotY = PlayerTransform->GetRotation().y;
		auto Angle = Vector3(sin(RotY), 0, cos(RotY));
		Angle.Normalize();
		Quaternion Qt;
		Qt.RotationRollPitchYaw(0, RotY, 0);
		Qt.Normalize();
		auto Span = Angle * 1.5f;
		///auto Coll = AddComponent<CollisionRect>();
		//Coll->SetDrawActive(true);
		SetDrawLayer(3);
		auto ParticlePtr = InsertParticle(10, Particle::DrawOption::Normal);
		m_PtrLaser = ParticlePtr;
		ParticlePtr->SetEmitterPos(Pos);
		//ParticlePtr->SetTextureResource(L"CIRCLE_TX");
		ParticlePtr->SetTextureResource(L"LASER_TX");
		ParticlePtr->SetMaxTime(1.0f);
		ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);

		/*auto Group = GetStage()->GetSharedObjectGroup(L"EnemyLaser");
		Group->IntoGroup(GetThis<EnemyLaserEffect>());*/
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			//	Rang += ElapsedTime;


			//レーザーの大きさ
			rParticleSprite.m_LocalScale.Set(0.5, 1.0);







			//	//各パーティクルの移動速度を指定

			//Quaternion Qt;
			/*
			角度を変える場合、↑でParticlePtr->SetDrawOption(Particle::DrawOption::Normal);と宣言し
			描画オプションを標準に切り替える必要がある。
			また、XYZでは無く、ロールピッチヨーで角度を指定する必要がある。その際、角度はディグリーではなく
			ラジアン角なので、<「目的の角度」/2π>で角度の変換を行わなければならない
			*/
			//float Pitch = (90.0f / 180.0f * XM_PI);
			// rParticleSprite.m_LocalQt.RotationRollPitchYaw(90.0f/180.0f * XM_PI, 0.0f, 0.0f);
			//前回のターンからの時間
			//	auto RotX = Vector3(Pitch, 0.0f, 0.0f);
			//	auto RotateY = Vector3(0.0f, RotY, 0.0f);
			//	auto Rot = RotX*RotateY;

			//X軸回転
			Quaternion Qt(Vector3(1.0, 0.0f, 0), XM_PIDIV2);
			//Quaternion QtZ(Vector3(0, 0, 1), XM_PIDIV2);
			//Y軸回転
			Quaternion Qt2(Vector3(0, 1.0f, 0), RotY);
			//Quaternion同士をかける
			Qt *= Qt2;



			//rParticleSprite.m_LocalQt.RotationRollPitchYaw();
			//rParticleSprite.m_LocalQt.RotationRollPitchYaw(0, RotY, 0);
			/*	Qt.RotationRollPitchYaw(0, RotY, 0);*/
			rParticleSprite.m_LocalQt = Qt;;
			//Qt.Normalize();

			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	void EnemyLaserEffect::OnUpdate() {

		//最初に親のUpdateを呼んであげる必要がある
		MultiParticle::OnUpdate();

		auto Group = GetStage()->GetSharedObjectGroup(L"Enemys");
		//auto ELaserGroup=GetStage()->GetSharedObjectGroup(L"EnmeyLaser");
		//auto GroupVec = Group->GetGroupVector();
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		//for (size_t i = 0; i < ELaserGroup->size(); i++) {
		for (size_t i = 0; i < Group->size(); i++) {
			//	if (ELaserGroup->at(i)->IsUpdateActive()) {
			if (Group->at(i)->IsUpdateActive()) {
				//auto EnemyPtr = dynamic_pointer_cast<Enemy>(Group->at(i));
				auto EnemyPos = Group->at(i)->GetComponent<Transform>()->GetPosition();
				auto RotY = Group->at(i)->GetComponent<Transform>()->GetRotation().y;
				auto Angle = Vector3(sin(RotY), 0, cos(RotY));
				auto Span = Angle * 1.5f;
				auto EnemyVelo = Group->at(i)->GetComponent<Rigidbody>()->GetVelocity();
				for (auto& rParticle : GetParticleVec()) {
					//Vector3 EmitterPos = rParticle->GetEmitterPos();
					for (auto& Sp : rParticle->GetParticleSpriteVec()) {


						Sp.m_Velocity = Vector3(


							//rParticleSprite.m_LocalScale = Vector3(1.0f, 1.0f, 1.0f)
							//	rParticleSprite.m_LocalScale *= Rang
							//rParticleSprite.m_LocalScale.y *= 1.0f*Rang,
							//回転の計算


							//PlayerVelo*3.5
							//EnemyVelo +
							Angle*25.5f

						);



						/*	Vector3 Wpos = EmitterPos + Sp.m_LocalPos;
						float length = Vector3EX::Length(Wpos - EnemyPos);*/
						//if (length <= 0.6f)
						//{

						//	ScenePtr->DestroyFLG = true;
						//	//ブーストのエフェクト消す
						//	EnemyPtr->DestroyFLG = true;
						//	//倒した数＋
						//	ScenePtr->DestroyCount += 1;
						//	//爆発のエフェクト表示
						//	GetStage()->AddGameObject<ExplosionSprite>(true, Vector2(1, 1), Vector3(EnemyPos), L"Explosion_TX");

						//	Group->at(i)->SetUpdateActive(false);
						//	Group->at(i)->SetDrawActive(false);
						//	return;
						//}
					}
				}
			}
		}
	}



	//構築と破棄
	LaserRingEffect::LaserRingEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	LaserRingEffect::~LaserRingEffect() {}

	void LaserRingEffect::InsertEffect(const Vector3& Pos) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTransform = PlayerPtr->GetComponent<Transform>();
		auto PlayerRot = PlayerTransform->GetRotation();
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto PlayerVelo = PlayerPtr->GetComponent<Rigidbody>()->GetVelocity();
		//	auto Group = GetStage()->GetSharedObjectGroup(L"Laser");
		//	Group->IntoGroup(GetThis<LaserEffect>());
		//回転の計算
		auto RotY = PlayerTransform->GetRotation().y;
		auto Angle = Vector3(sin(RotY), 0, cos(RotY));
		Angle.Normalize();
		Quaternion Qt;
		Qt.RotationRollPitchYaw(0, RotY, 0);
		Qt.Normalize();
		auto Span = Angle * 1.5f;
		auto Coll = AddComponent<CollisionRect>();
		Coll->SetDrawActive(true);
		SetDrawLayer(3);
		auto ParticlePtr = InsertParticle(10, Particle::DrawOption::Normal);
		ParticlePtr->SetEmitterPos(Pos);
		//ParticlePtr->SetTextureResource(L"CIRCLE_TX");
		ParticlePtr->SetTextureResource(L"LASERRING_TX");
		ParticlePtr->SetMaxTime(1.0f);
		ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			Rang += ElapsedTime;


			//レーザーの大きさ
			rParticleSprite.m_LocalScale.Set(1.0, 1.0);






			if (Rang >= 2)
			{

				Rang = 0;
				/*rParticleSprite.m_LocalScale.x *= 1.0f*Rang;
				rParticleSprite.m_LocalScale.y *= 1.0f*Rang;*/
			}

			//rParticleSprite.m_LocalScale=Vector2( 5.0f*Rang,5.0f*Rang);
			//rParticleSprite.m_LocalScale.y *= 1.2f;

			//	rParticleSprite.m_LocalPos.z = 1.2f;
			/*	rParticleSprite.m_LocalScale.x += 5;
			rParticleSprite.m_LocalScale.y += 5;*/

			//	auto RotY = PlayerTransform->GetRotation().y;
			//	auto Angle = Vector3(sin(RotY), 0, cos(RotY));
			//	Angle.Normalize();
			//	Quaternion Qt;
			//	Qt.RotationRollPitchYaw(0, RotY, 0);
			//	Qt.Normalize();
			//	auto Span = Angle * 1.5f;
			////	rParticleSprite.m_LocalPos = PlayerTransform->GetPosition() + Span, Qt, Angle * 12.5f,
			//	//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(


				//rParticleSprite.m_LocalScale = Vector3(1.0f, 1.0f, 1.0f)
				//	rParticleSprite.m_LocalScale *= Rang
				//rParticleSprite.m_LocalScale.y *= 1.0f*Rang,
				//回転の計算


				//PlayerVelo*3.5
				PlayerVelo + Angle*50.5f
				/*rParticleSprite.m_LocalPos.x * 0.0f,
				rParticleSprite.m_LocalPos.y * 0.0f,
				rParticleSprite.m_LocalPos.z * 25.0f*/
			);
			//Quaternion Qt;
			/*
			角度を変える場合、↑でParticlePtr->SetDrawOption(Particle::DrawOption::Normal);と宣言し
			描画オプションを標準に切り替える必要がある。
			また、XYZでは無く、ロールピッチヨーで角度を指定する必要がある。その際、角度はディグリーではなく
			ラジアン角なので、<「目的の角度」/2π>で角度の変換を行わなければならない
			*/
			float Pitch = (90.0f / 180.0f * XM_PI);
			// rParticleSprite.m_LocalQt.RotationRollPitchYaw(90.0f/180.0f * XM_PI, 0.0f, 0.0f);
			//前回のターンからの時間
			//	auto RotX = Vector3(Pitch, 0.0f, 0.0f);
			//	auto RotateY = Vector3(0.0f, RotY, 0.0f);
			//	auto Rot = RotX*RotateY;

			Quaternion Qt(Vector3(1.0, 0.0f, 0), XM_PIDIV2);
			//Quaternion QtZ(Vector3(0, 0, 1), XM_PIDIV2);
			Quaternion Qt2(Vector3(0, 1.0, 0), RotY);
			Qt *= Qt2;



			//rParticleSprite.m_LocalQt.RotationRollPitchYaw();
			//rParticleSprite.m_LocalQt.RotationRollPitchYaw(0, RotY, 0);
			/*	Qt.RotationRollPitchYaw(0, RotY, 0);*/
			rParticleSprite.m_LocalQt = Qt;;
			//Qt.Normalize();

			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}





}