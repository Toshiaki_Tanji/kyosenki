/*!
@file Character.h
@brief キャラクターなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{


	class TileFloor : public GameObject {
		UINT m_WidthCount;
		UINT m_HeightCount;
		Vector3 m_Scale;
		Quaternion m_Qt;
		Vector3 m_Position;
	public :
		TileFloor(const shared_ptr<Stage>& StagePtr,
			UINT WidthTileCount,
			UINT HeightTileCount,
			const Vector3& Scale,
			const Quaternion& Qt,
			const Vector3& Position
		);
		virtual ~TileFloor();
		//初期化
		virtual void OnCreate()override;
		//操作
		//virtual void OnUpdate()override;

	};

	//
	//空のモデルオブジェクト
	//

	class EmptyModel : public GameObject {
	protected:
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		wstring m_Modelkey;
	public:
		//構築と破棄
		EmptyModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& ModelKey
		);
		virtual ~EmptyModel();
		//初期化
		virtual void OnCreate()override;
		//操作
		virtual void OnUpdate()override;
	};


	//隕石
	class MeteoRite : public GameObject {
	protected:
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		wstring m_Modelkey;
	public:
		//構築と破棄
		MeteoRite(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& ModelKey
		);
		virtual ~MeteoRite();
		//初期化
		virtual void OnCreate()override;
		//操作
		virtual void OnUpdate()override;

	};

	//--------------------------------------------------------------------------------------
	///	固定のボックス
	//--------------------------------------------------------------------------------------
	class FixedBox : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		FixedBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~FixedBox();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	敵
	//--------------------------------------------------------------------------------------
	class Enemy : public GameObject {
	protected:
		weak_ptr<StageCellMap> m_CelMap;
		Vector3 m_Scale;
		Vector3 m_StartRotation;
		Vector3 m_StartPosition;
		vector<CellIndex> m_CellPath;
		//現在の自分のセルインデックス
		int m_CellIndex;
		//めざす（次の）のセルインデックス
		int m_NextCellIndex;
		//ターゲットのセルインデックス
		int m_TargetCellIndex;
		shared_ptr<StateMachine<Enemy>> m_StateMachine;
		//進行方向を向くようにする
		void RotToHead();
		//耐久度
		int m_Durability;

		wstring m_Tag;

	public:
		//構築と破棄
		Enemy(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& Tag,
			const int& Durability
		);
		virtual ~Enemy();
		//プレイヤーの検索
		bool SearchPlayer();

		//捕まった状態
		void CatchState();

		//捨てられた状態
		void ThrownMotion();

		//無敵時間
		float m_invisibleTime;
		//点滅間隔
		float m_flashinterval;
		//透明度切り替え
		int ClearChanger = 1;
		//被弾したかどうか
		bool IsDamaged;
		//ダメージモーション
		void DamegeMotion();

		//ダメージを食らうときの処理
		void SetDameged();

		//デフォルト行動
		virtual bool DefaultBehavior();
		//Seek行動
		bool SeekBehavior();
		//アクセサ
		shared_ptr< StateMachine<Enemy> > GetStateMachine() const {
			return m_StateMachine;
		}
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		//ブーストエフェクト作ったかどうか
		bool BoostCreate = false;
		bool DestroyFLG = false;
		bool Catch = false;
	};

	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	class EnemyDefault : public ObjState<Enemy>
	{
		EnemyDefault() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(EnemyDefault)
		virtual void Enter(const shared_ptr<Enemy>& Obj)override;
		virtual void Execute(const shared_ptr<Enemy>& Obj)override;
		virtual void Exit(const shared_ptr<Enemy>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	///	プレイヤーを追いかけるステート
	//--------------------------------------------------------------------------------------
	class EnemySeek : public ObjState<Enemy>
	{
		EnemySeek() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(EnemySeek)
		virtual void Enter(const shared_ptr<Enemy>& Obj)override;
		virtual void Execute(const shared_ptr<Enemy>& Obj)override;
		virtual void Exit(const shared_ptr<Enemy>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	///	プレイヤーにつかまるステート
	//--------------------------------------------------------------------------------------
	class EnemyGetPlayer : public ObjState<Enemy>
	{
		EnemyGetPlayer() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(EnemyGetPlayer)
		virtual void Enter(const shared_ptr<Enemy>& Obj)override;
		virtual void Execute(const shared_ptr<Enemy>& Obj)override;
		virtual void Exit(const shared_ptr<Enemy>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	セルマップを再設定する敵
	//--------------------------------------------------------------------------------------
	class TestCellChangeEnemy : public Enemy {
	public:
		//構築と破棄
		TestCellChangeEnemy(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& Tag,
			const int& Durability
		);
		virtual ~TestCellChangeEnemy();
		//デフォルト行動
		virtual bool DefaultBehavior() override;
	};


	//--------------------------------------------------------------------------------------
	//	boss
	//--------------------------------------------------------------------------------------
	class BossEnemy : public Enemy {
	private:
		bool Move = true;
		int MoveTime;
		//bool Destroy = false;
		int DestroyTime;
		bool Search = true;
		bool BulletFire = false;
		//プレイヤーを追う時間
		float SearchTime = 60;
		//クールタイム
		float CoolTime = 60;
		//攻撃
		float AttackCoolTime = 10;

		float dx;
		float dz;
		float angle;

	public:
		//構築と破棄
		BossEnemy(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& Tag,
			const int& Durability
		);
		virtual ~BossEnemy();
		//デフォルト行動
		virtual bool DefaultBehavior() override;

		//攻撃距離を測る
		bool AttackLength();
		//弾を撃つ
		void BossAttackBullet();
		//弾を撃つ
		void BossAttackLaser();
		//弾を撃つ
		void BossAttackMissile();
		//動く
		void AttackMove();
		//プレイヤーの周りをぐるぐる
		void AroundPL();
		//撃破モーション
		void BreakMotion();
	};


	//田代作
	//プレイヤーに一直線に向かってくる敵
	class WeakEnemy : public Enemy {
	public:
		//構築と破棄
		WeakEnemy(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& Tag,
			const int& Durability
		);
		virtual ~WeakEnemy();
		//デフォルト行動
		//virtual bool DefaultBehavior() override;
		virtual void OnUpdate() override;
		bool Move = true;
		//プレイヤーを追う時間
		float MoveTime = 2;
		//bool Destroy = false;
		int DestroyTime;
		bool Search = true;
		float dx;
		float dz;
		float angle;
		void PlayrSearch();
		//止まる時間
		float StopTime = 3.0f;
		int BulletFireCount = 180;
		//止まっているかどうか
		bool Stop = false;

		float SearchTime = 2;

		bool BulletFire = false;

	};

	//田代作
	//プレイヤーを狙って攻撃してくる敵
	class AimingEnemys : public Enemy {

	public:
		//構築と破棄
		AimingEnemys(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& Tag,
			const int& Durability
		);
		virtual ~AimingEnemys();
		//デフォルト行動
		virtual void OnUpdate() override;

		bool Move = true;
		int MoveTime;
		//bool Destroy = false;
		int DestroyTime;
		bool Search = true;
		bool BulletFire = false;
		//プレイヤーを追う時間
		float SearchTime = 60;
		//クールタイム
		float CoolTime = 60;
		//攻撃
		float AttackCoolTime = 10;

		float dx;
		float dz;
		float angle;
		void PlayrSearch();
	};


	//田代作
	//プレイヤーを狙って攻撃してくる敵
	class TeleportEnemys : public Enemy {


		//階層化ステートマシーン
		unique_ptr<LayeredStateMachine<TeleportEnemys>>  m_TeleportStateMachine;

	public:
		//構築と破棄teleport
		TeleportEnemys(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& Tag,
			const int& Durability
		);
		virtual ~TeleportEnemys();




		unique_ptr<LayeredStateMachine<TeleportEnemys>>& GetTeleportStateMachine() {
			return m_TeleportStateMachine;
		}


		virtual void OnCreate() override;

		//デフォルト行動
		virtual void OnUpdate() override;
		virtual void OnUpdate2()override;

		bool Move = true;
		int MoveTime;
		//bool Destroy = false;
		int DestroyTime;
		bool Search = true;
		bool BulletFire = false;
		//プレイヤーを追う時間
		float SearchTime = 60;
		//クールタイム
		float CoolTime = 120;
		//攻撃
		float AttackCoolTime = 10;
		//テレポートするかしないか
		bool Teleport = true;
		int TeleportType = 0;
		float TeleportTime = 2;

		float dx;
		float dz;
		float angle;
		void PlayrSearch();
	};


	//--------------------------------------------------------------------------------------
	///	通常ステート
	//--------------------------------------------------------------------------------------
	class TeleportDefaultState : public ObjState<TeleportEnemys>
	{
		TeleportDefaultState() {}

	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(TeleportDefaultState)
		virtual void Enter(const shared_ptr<TeleportEnemys>& Obj)override;
		virtual void Execute(const shared_ptr<TeleportEnemys>& Obj)override;
		virtual void Exit(const shared_ptr<TeleportEnemys>& Obj)override;
	};


	//--------------------------------------------------------------------------------------
	///	アタックステート
	//--------------------------------------------------------------------------------------
	class TeleportAttackState : public ObjState<TeleportEnemys>
	{
		TeleportAttackState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(TeleportAttackState)
		virtual void Enter(const shared_ptr<TeleportEnemys>& Obj)override;
		virtual void Execute(const shared_ptr<TeleportEnemys>& Obj)override;
		virtual void Exit(const shared_ptr<TeleportEnemys>& Obj)override;
	};



	//--------------------------------------------------------------------------------------
	///	レーザーアタックステート
	//--------------------------------------------------------------------------------------
	class TeleportLaserAttackState : public ObjState<TeleportEnemys>
	{
		TeleportLaserAttackState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(TeleportLaserAttackState)
		virtual void Enter(const shared_ptr<TeleportEnemys>& Obj)override;
		virtual void Execute(const shared_ptr<TeleportEnemys>& Obj)override;
		virtual void Exit(const shared_ptr<TeleportEnemys>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	///	アタックステート
	//--------------------------------------------------------------------------------------
	class TeleportMoveState : public ObjState<TeleportEnemys>
	{
		TeleportMoveState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(TeleportMoveState)
		virtual void Enter(const shared_ptr<TeleportEnemys>& Obj)override;
		virtual void Execute(const shared_ptr<TeleportEnemys>& Obj)override;
		virtual void Exit(const shared_ptr<TeleportEnemys>& Obj)override;
	};

	//-------------------------------------------------------------------------------------
	//回転して弾を発射する敵

	class RotationEnemys : public Enemy {

	public:
		//構築と破棄
		RotationEnemys(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& Tag,
			const int& Durability
		);
		virtual ~RotationEnemys();
		//デフォルト行動
		virtual void OnUpdate() override;

		bool Move = true;
		int MoveTime;
		//bool Destroy = false;
		int DestroyTime;
		bool Search = true;
		bool BulletFire = false;
		//プレイヤーを追う時間
		float SearchTime = 60;
		//クールタイム
		float CoolTime = 2;
		//攻撃
		float AttackCoolTime = 10;
		int AttackTime = 0;

		float dx;
		float dz;
		float angle;
		void PlayrSearch();
	};


	//回復アイテム
	class Recovery : public GameObject {
	protected:
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		wstring m_Modelkey;
	public:
		//構築と破棄
		Recovery(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			const wstring& ModelKey
		);
		virtual ~Recovery();
		//初期化
		virtual void OnCreate()override;
		//操作
		virtual void OnUpdate()override;

	};


	//--------------------------------------------------------------------------------------
	///	上のwarpボックス
	//--------------------------------------------------------------------------------------
	class TopSpawner : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		TopSpawner(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~TopSpawner();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};


	//--------------------------------------------------------------------------------------
	///	下のwarpボックス
	//--------------------------------------------------------------------------------------
	class UnderSpawner : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		UnderSpawner(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~UnderSpawner();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};


	//--------------------------------------------------------------------------------------
	///	右のwarpボックス
	//--------------------------------------------------------------------------------------
	class RightSpawner : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		RightSpawner(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~RightSpawner();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};


	//--------------------------------------------------------------------------------------
	///	左のwarpボックス
	//--------------------------------------------------------------------------------------
	class LeftSpawner : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		LeftSpawner(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~LeftSpawner();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};


	//--------------------------------------------------------------------------------------
	///	上のwarpボックス
	//--------------------------------------------------------------------------------------
	class TopWarpBox : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		TopWarpBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~TopWarpBox();
		//初期化
		virtual void OnCreate() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};


	//--------------------------------------------------------------------------------------
	///	下のwarpボックス
	//--------------------------------------------------------------------------------------
	class UnderWarpBox : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		UnderWarpBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~UnderWarpBox();
		//初期化
		virtual void OnCreate() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};


	//--------------------------------------------------------------------------------------
	///	右のwarpボックス
	//--------------------------------------------------------------------------------------
	class RightWarpBox : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		RightWarpBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~RightWarpBox();
		//初期化
		virtual void OnCreate() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};


	//--------------------------------------------------------------------------------------
	///	左のwarpボックス
	//--------------------------------------------------------------------------------------
	class LeftWarpBox : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		LeftWarpBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~LeftWarpBox();
		//初期化
		virtual void OnCreate() override;

		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};




	////////////////////////////////////////////////6/23////////////////////////////////////////////////////////////////
	class StageClearSprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		StageClearSprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~StageClearSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		bool Flag_Hit = false;

	};



	class NextSprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		NextSprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~NextSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		bool Flag_Hit = false;

	};


	class TitleSprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		TitleSprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~TitleSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;


		bool Flag_Hit = false;

	};




	class CursorSprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		CursorSprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~CursorSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		bool Flag_Hit = false;

	};



	class BGSCSprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		BGSCSprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~BGSCSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		bool Flag_Hit = false;

	};


	class ScoreSprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		ScoreSprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~ScoreSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		float Alpha = 0;

	};


	class RankSSprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		RankSSprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~RankSSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		float Alpha = 0;

	};



	class RankSprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		RankSprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~RankSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		float Alpha = 0;

	};



	class GameOverSprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		GameOverSprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~GameOverSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		float Alpha = 0;

	};



	class RetrySprite : public GameObject {
		bool m_Trace;

		Vector2 m_Scale;
		Vector3 m_Pos;
		wstring m_Media;

		float m_TotalTime;

		vector<VertexPositionTexture> m_BackupVertices;

	public:
		RetrySprite(const shared_ptr<Stage> &StagePtr,
			bool Trace,
			const Vector2& Scale,
			const Vector3& Position,
			wstring Media
		);
		virtual ~RetrySprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		float Alpha = 0;

	};


	/////////////////////////////////////////////////////////////////////////////6/23//////////////////////////////////////////////////////////


}
//end basecross
