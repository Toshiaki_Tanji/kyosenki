#pragma once
#include "stdafx.h"

namespace basecross {
	//-------------------------------
	//CSVでパラメータ取得
	//
	//-------------------------------
	class CSVParametor : public GameObject
	{
	private:

		wstring m_FilesName;
		vector<wstring> RowStr;
		bool m_isFinish;

	public:
		CSVParametor(const shared_ptr<Stage>& StagePtr, const wstring& FileName);
		virtual ~CSVParametor();

		//ステージの作成関数
		void CreateLoad();
		//アクセサ
		wstring GetFileName() { return m_FilesName; }
		bool GetIsFinish() { return m_isFinish; }
	};
}