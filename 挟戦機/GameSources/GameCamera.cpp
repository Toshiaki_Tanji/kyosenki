


#include "stdafx.h"
#include "Project.h"

namespace basecross {

	MainCamera::MainCamera() :
		Camera()
	{
	}

	void MainCamera::SetTargetObject(const shared_ptr<GameObject>& Obj) {
		m_TargetObject = Obj;
	}

	shared_ptr<GameObject> MainCamera::GetTargetObject() const {
		if (!m_TargetObject.expired()) {
			return m_TargetObject.lock();
		}
		return nullptr;
	}

	void MainCamera::OnCreate()
	{
		Vector3 At(0, 0, 0);
		SetAt(At);
		At.y += 5.0f;
		SetEye(At);
		SetUp(0, 0, 1.0f);
		
	}

		



	void MainCamera::OnUpdate() {
		auto Ptr = GetTargetObject();

		if (Ptr) {
			//auto Pos = Ptr->GetComponent<Transform>()->GetPosition();
			auto Pos = Ptr->GetComponent<Transform>()->GetPosition();
			SetAt(Pos);
			Pos.y += 12.0f;
			SetEye(Pos);
			//コントローラの取得
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			//Player死んだら切り替えられない
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_Y&&App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PlayerHP >= 1) {
				ChangeCount += 1;
			}
			if (ChangeCount >= 2)
			{
				ChangeCount = 0;
			}
			switch (ChangeCount)
			{
			case 1:
				Pos.y += ChangeNum;
				SetEye(Pos);
				break;
			case 2:
				Pos.y -= ChangeNum;
				SetEye(Pos);
				break;
			default:
				break;
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	struct GameCamera::Impl;
	//	用途: Implクラス
	//--------------------------------------------------------------------------------------
	struct GameCamera::Impl {
		weak_ptr<GameObject> m_TargetObject;	//目標となるオブジェクト
		float m_ToTargetLerp;	//目標を追いかける際の補間値
		Vector3 m_TargetToAt;	//目標から視点を調整する位置ベクトル


		float m_RadY;
		float m_RadXZ;
		//カメラの上下スピード
		float m_CameraUpDownSpeed;
		//カメラを下げる下限角度
		float m_CameraUnderRot;
		//腕の長さの設定
		float m_ArmLen;
		float m_MaxArm;
		float m_MinArm;
		//回転スピード
		float m_RotSpeed;
		//ズームスピード
		float m_ZoomSpeed;


		Impl() :
			m_ToTargetLerp(1.0f),
			m_TargetToAt(0, 0, 0),
			m_RadY(5.5f),
			m_RadXZ(0),
			m_CameraUpDownSpeed(0.02f),
			m_CameraUnderRot(0.1f),
			m_ArmLen(5.0f),
			m_MaxArm(25.0f),
			m_MinArm(2.0f),
			m_RotSpeed(1.0f),
			m_ZoomSpeed(0.1f)
		{}
		~Impl() {}
	};



	//--------------------------------------------------------------------------------------
	//	class GameCamera : public Camera ;
	//	用途: LookAtカメラ（コンポーネントではない）
	//--------------------------------------------------------------------------------------
	//構築と破棄
	GameCamera::GameCamera() :
		Camera(),
		pImpl(new Impl())
	{}

	GameCamera::GameCamera(float ArmLen) :
		Camera(),
		pImpl(new Impl())
	{
		pImpl->m_ArmLen = ArmLen;
	}

	GameCamera::~GameCamera() {}
	//アクセサ
	shared_ptr<GameObject> GameCamera::GetTargetObject() const {
		if (!pImpl->m_TargetObject.expired()) {
			return pImpl->m_TargetObject.lock();
		}
		return nullptr;
	}

	void GameCamera::SetTargetObject(const shared_ptr<GameObject>& Obj) {
		pImpl->m_TargetObject = Obj;
	}

	float GameCamera::GetToTargetLerp() const {
		return pImpl->m_ToTargetLerp;
	}
	void GameCamera::SetToTargetLerp(float f) {
		pImpl->m_ToTargetLerp = f;
	}

	float GameCamera::GetArmLengh() const {
		return pImpl->m_ArmLen;
	}

	void GameCamera::UpdateArmLengh() {
		auto Vec = GetEye() - GetAt();
		pImpl->m_ArmLen = Vec.Length();
		if (pImpl->m_ArmLen >= pImpl->m_MaxArm) {
			//m_MaxArm以上離れないようにする
			pImpl->m_ArmLen = pImpl->m_MaxArm;
		}
		if (pImpl->m_ArmLen <= pImpl->m_MinArm) {
			//m_MinArm以下近づかないようにする
			pImpl->m_ArmLen = pImpl->m_MinArm;
		}
	}

	float GameCamera::GetMaxArm() const {
		return pImpl->m_MaxArm;

	}
	void GameCamera::SetMaxArm(float f) {
		pImpl->m_MaxArm = f;
	}
	float GameCamera::GetMinArm() const {
		return pImpl->m_MinArm;
	}
	void GameCamera::SetMinArm(float f) {
		pImpl->m_MinArm = f;
	}

	float GameCamera::GetRotSpeed() const {
		return pImpl->m_RotSpeed;

	}
	void GameCamera::SetRotSpeed(float f) {
		pImpl->m_RotSpeed = abs(f);
	}

	Vector3 GameCamera::GetTargetToAt() const {
		return pImpl->m_TargetToAt;

	}
	void GameCamera::SetTargetToAt(const Vector3& v) {
		pImpl->m_TargetToAt = v;
	}

	void GameCamera::SetAt(const Vector3& At) {
		Camera::SetAt(At);
		Vector3 ArmVec = GetEye() - GetAt();
		ArmVec.Normalize();
		ArmVec *= pImpl->m_ArmLen;
		Vector3 NewEye = GetAt() + ArmVec;
		SetEye(NewEye);
	}
	void GameCamera::SetAt(float x, float y, float z) {
		Camera::SetAt(x, y, z);
		Vector3 ArmVec = GetEye() - GetAt();
		ArmVec.Normalize();
		ArmVec *= pImpl->m_ArmLen;
		Vector3 NewEye = GetAt() + ArmVec;
		SetEye(NewEye);

	}

	void GameCamera::SetNum() {

		Vector3 NewEye = GetEye();
		Vector3 NewAt = GetAt();
		Vector3 CameraPos(0.0f, 10.0f, 0.0f);
		Vector3 CameraAngle(0.0f, 10.0f, 10.0f);
		SetAt(CameraAngle);
		SetEye(CameraPos);

	}

	void GameCamera::OnUpdate() {
		SetNum();
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		auto KeyData = App::GetApp()->GetInputDevice().GetKeyState();
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		Vector3 NewEye = GetEye();
		Vector3 NewAt = GetAt();
		//計算に使うための腕角度（ベクトル）
		Vector3 ArmVec = NewEye - NewAt;
		//正規化しておく
		ArmVec.Normalize();
		if (CntlVec[0].bConnected) {
			//上下角度の変更
			if (CntlVec[0].fThumbRY >= 0.1f || KeyData.m_bPushKeyTbl[VK_UP]) {
				pImpl->m_RadY += pImpl->m_CameraUpDownSpeed;
			}
			else if (CntlVec[0].fThumbRY <= -0.1f || KeyData.m_bPushKeyTbl[VK_DOWN]) {
				pImpl->m_RadY -= pImpl->m_CameraUpDownSpeed;
			}
			if (pImpl->m_RadY > XM_PI * 4 / 9.0f) {
				pImpl->m_RadY = XM_PI * 4 / 9.0f;
			}
			else if (pImpl->m_RadY <= pImpl->m_CameraUnderRot) {
				//	//カメラが限界下に下がったらそれ以上下がらない
				pImpl->m_RadY = pImpl->m_CameraUnderRot;
			}
			ArmVec.y = sin(pImpl->m_RadY);
			//ここでY軸回転を作成
			if (CntlVec[0].fThumbRX != 0 || KeyData.m_bPushKeyTbl[VK_LEFT] || KeyData.m_bPushKeyTbl[VK_RIGHT]) {
				if (abs(pImpl->m_RadXZ) >= XM_2PI) {
					//1週回ったら0回転にする
					pImpl->m_RadXZ = 0;
				}
			}
			//クオータニオンでY回転（つまりXZベクトルの値）を計算
			Quaternion QtXZ;
			QtXZ.RotationAxis(Vector3(0, 1.0f, 0), pImpl->m_RadXZ);
			QtXZ.Normalize();
			//移動先行の行列計算することで、XZの値を算出
			Matrix4X4 Mat;
			Mat.STRTransformation(
				Vector3(1.0f, 1.0f, 1.0f),
				Vector3(0.0f, 0.0f, -1.0f),
				QtXZ
			);

			Vector3 PosXZ = Mat.PosInMatrixSt();
			//XZの値がわかったので腕角度に代入
			ArmVec.x = PosXZ.x;
			ArmVec.z = PosXZ.z;
			//腕角度を正規化
			ArmVec.Normalize();

			auto TargetPtr = GetTargetObject();
			if (TargetPtr) {
				//目指したい場所
				Vector3 ToAt = TargetPtr->GetComponent<Transform>()->GetWorldMatrix().PosInMatrixSt();
				ToAt += pImpl->m_TargetToAt;
				NewAt = Lerp::CalculateLerp(GetAt(), ToAt, 0, 1.0f, 1.0, Lerp::Linear);
			}
			//アームの変更
			//Dパッド下
			if (CntlVec[0].wButtons & XINPUT_GAMEPAD_DPAD_DOWN) {
				//カメラ位置を引く
				pImpl->m_ArmLen += pImpl->m_ZoomSpeed;
				if (pImpl->m_ArmLen >= pImpl->m_MaxArm) {
					//m_MaxArm以上離れないようにする
					pImpl->m_ArmLen = pImpl->m_MaxArm;
				}
			}
			//Dパッド上
			if (CntlVec[0].wButtons & XINPUT_GAMEPAD_DPAD_UP) {
				//カメラ位置を寄る
				pImpl->m_ArmLen -= pImpl->m_ZoomSpeed;
				if (pImpl->m_ArmLen <= pImpl->m_MinArm) {
					//m_MinArm以下近づかないようにする
					pImpl->m_ArmLen = pImpl->m_MinArm;
				}
			}
			////目指したい場所にアームの値と腕ベクトルでEyeを調整
			Vector3 ToEye = NewAt + ArmVec * pImpl->m_ArmLen;
			NewEye = Lerp::CalculateLerp(GetEye(), ToEye, 0, 1.0f, pImpl->m_ToTargetLerp, Lerp::Linear);
		}
		if (KeyData.m_bPressedKeyTbl[VK_LEFT]) {
			int a = 0;
		}

		//クオータニオンでY回転（つまりXZベクトルの値）を計算
		Quaternion QtXZ;
		QtXZ.RotationAxis(Vector3(0, 0.0f, 1.0f), pImpl->m_RadXZ);
		QtXZ.Normalize();
		//移動先行の行列計算することで、XZの値を算出
		//SetEye(NewEye);
		SetAt(NewAt.x, NewAt.y, NewAt.z);
		SetEye(NewEye.x, NewEye.y, NewEye.z);
		UpdateArmLengh();
		Camera::OnUpdate();
	}
}