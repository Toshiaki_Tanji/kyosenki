/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage {
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		void CreatePlate();
		//セルマップの作成
		void CreateStageCellMap();
		//固定のボックスの作成
		void CreateFixedBox();
		//プレイヤーの作成
		void CreatePlayer();
		//敵の作成
		void CreateEnemy();
		//UIの作成
		void CreateUI();
		//HPUI
		void CreateHPUI();

		//回復アイテム
		void CreateRecovery();
	
		//spritestudioのアニメーション
		void CreateSS5Anime();

		////csvからBOXを作成
		void CreateRoad();
		//effectBoost作成
		void CreateEffectBoost();

		//敵（仮）の作成
		void CreateEnemy1();
		void CreateEnemy2();
		void CreateBoss();
		//supona-
		void CreateSpawner();

		//ボス出現
		float UpDateBoss;
		bool CreateOnce = true;
		bool CreateOnce2 = true;


		//隕石
		void CreateMeteoRite();
		//warpbox作成
		void CreateWarpBox();

		//リザルト？の画像作成
		void CreateResultSprite();
		//クリア時のアクション
		void ClearAction();
		//げーむオーバー時のアクション
		void GameOverAction();
		//イベント発生時間を設定
		float m_EventActionTime = 0.0f;
		//フラグを発生させるための数字
		int m_flgnum = 0;
		//
		bool m_BossSpawn = false;	
		float aa = 240.0f;
		//チュートリアル動かす
		int TutorealCount = 0;

		//エフェクトの作成
		void CreateLaserParticle();

		//Laserのボールエフェクト
		void CreateLaserBall();
		//チュートリアル
		void CreateTutoreal();
		//
		void SpawnEnemy();



		//エフェクトの作成
//		void CreateLaserParticle();

		//Laserのボールエフェクト
		//void CreateLaserBall();

		//ゲームクリア時のスプライト
		void CreateStageClear();

		//ゲームオーバー時のスプライト
		void CreateGameOver();


		///////////////////////////////6/3ついか
		//Spawnerのポジション
		Vector3 TopPos;
		Vector3 UnderPos;
		Vector3 RightPos;
		Vector3 LeftPos;
		//↑↓
		//外壁のポジション
		Vector3 TopWPos;
		Vector3 UnderWPos;
		Vector3 RightWPos;
		Vector3 LeftWPos;

		//csvで読み込んだオブジェクトを出す座標
		Vector3 SpawnPos;

		//角にいるかどうか
		bool CornerFlg = false;
		//////////////////////////////////////////


		//CSV関係--------------------------------------------
		//プレイヤーを生成するCell座標
		Vector2 m_vPlayerSpawnCell;
		//ステージ生成に必要なメンバ変数
		size_t m_sColSize;
		size_t m_sRowSize;
		//ワーク用のベクター配列のメンバ変数
		vector<vector<size_t>>m_MapDateVec;
		vector<vector<size_t>>m_MapDateVec2;

	public:
		//固定のボックスのコストをセルマップに反映
		void SetCellMapCost(const wstring& CellMapGroupName);
		//構築と破棄
		GameStage() :Stage() {}
		virtual ~GameStage() {}
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};


	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class TitleStage : public Stage {
		//ビューの作成
		void CreateViewLight();
		//UIの作成
		void CreateUI();
		//モデルの作成
		void CreateModel();
		//分割スプライト
		void CreateSplitSprite();

		//オープニングイベント
		void OpeningEvent();
		//1度きりのイベント
		bool OnceEvent = true;
		//えべんとを順次発せさせる弾のカウント
		int m_EventCounter = 0;
		//イベント発生からの経過時間
		float m_EventTime = 0.0f;
		//元のα値から減らして透明に
		float m_ClearSprite = 1.0f;
		//ボタンを押したか
		bool PushButton = false;
		//
		float m_Move = 0.0f;
		//
		bool m_CataMove = false;
		//
		bool m_PLMove = false;
		//
		float AnimTime = 0.0f;
		///
		bool m_Select = false;

	public:
		//構築と破棄
		TitleStage() :Stage() {}
		virtual ~TitleStage() {}
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};



	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class SelectStage : public Stage {
		//ビューの作成
		void CreateViewLight();
		//UIの作成
		void CreateUI();

	public:
		//構築と破棄
		SelectStage() :Stage() {}
		virtual ~SelectStage() {}
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

	////-------------------------
	////リザルトステージクラス
	////-------------------------
	//class ResultStage : public Stage {
	//	//ビューの作成
	//	void CreateViewLight();
	//	//UIの作成
	//	void CreateUI();
	//public:
	//	//構築と破棄
	//	ResultStage():Stage(){}
	//	virtual ~ResultStage(){}
	//	//初期化
	//	virtual void OnCreate();
	//	virtual void OnUpdate();
	//};

}
//end basecross

