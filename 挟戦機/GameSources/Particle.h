#pragma once
#include "stdafx.h"

namespace basecross {

	//ジャギーのエフェクト
	class JaggyEffect : public MultiParticle {
	public:
		//構築と破棄
		JaggyEffect(shared_ptr<Stage>& StagePtr);
		virtual ~JaggyEffect();

		void InsertEffect(const Vector3& Pos);

	};

	//サークルのエフェクト
	class CircleEffect : public MultiParticle {
	public:
		//構築と破棄
		CircleEffect(shared_ptr<Stage>& StagePtr);
		virtual ~CircleEffect();

		virtual void InsertEffect(const Vector3& Pos);
		//	virtual void OnUpdate()override;

	};


	//行動抜き取り時のエフェクト
	class KirakiraEffect : public MultiParticle {
	public:
		//構築と破棄
		KirakiraEffect(shared_ptr<Stage>& StagePtr);
		virtual ~KirakiraEffect();

		virtual void InsertEffect(const Vector3& Pos);
		//	virtual void OnUpdate()override;

	};

	//113
	//行動保持エフェクト
	class TorchEffect : public MultiParticle {
	public:
		//構築と破棄
		TorchEffect(shared_ptr<Stage>& StagePtr);
		virtual ~TorchEffect();

		virtual void InsertEffect(const Vector3& Pos);
		//	virtual void OnUpdate()override;

	};



	//衝突時のエフェクト
	class HitEffect : public MultiParticle {
	public:
		//構築と破棄
		HitEffect(shared_ptr<Stage>& StagePtr);
		virtual ~HitEffect();

		virtual void InsertEffect(const Vector3& Pos);
		//	virtual void OnUpdate()override;

	};


	//行動出し入時のエフェクト
	class RedCircleEffect : public MultiParticle {
	public:
		//構築と破棄
		RedCircleEffect(shared_ptr<Stage>& StagePtr);
		virtual ~RedCircleEffect();

		virtual void InsertEffect(const Vector3& Pos);
		//void OnUpdate();
		//virtual void OnUpdate override;
		float Rang;
	};

	//

	//レーザーエフェクト
	class LaserEffect : public MultiParticle {
		shared_ptr<Particle> m_PtrLaser;
	public:
		//構築と破棄
		LaserEffect(shared_ptr<Stage>& StagePtr);
		virtual ~LaserEffect();

		virtual void InsertEffect(const Vector3& Pos);
		//void OnUpdate();
		//virtual void OnUpdate override;
		virtual void OnUpdate()override;
		float Rang;

	};


	//敵のレーザーエフェクト
	class EnemyLaserEffect : public MultiParticle {
		shared_ptr<Particle> m_PtrLaser;
	public:
		//構築と破棄
		EnemyLaserEffect(shared_ptr<Stage>& StagePtr);
		virtual ~EnemyLaserEffect();

		virtual void InsertEffect(const Vector3& Pos);
		//void OnUpdate();
		//virtual void OnUpdate override;
		virtual void OnUpdate()override;
		float Rang;

	};



	//レーザーエフェクト
	class LaserRingEffect : public MultiParticle {
	public:
		//構築と破棄
		LaserRingEffect(shared_ptr<Stage>& StagePtr);
		virtual ~LaserRingEffect();

		virtual void InsertEffect(const Vector3& Pos);
		//void OnUpdate();
		//virtual void OnUpdate override;
		//	virtual void OnUpdate()override;
		float Rang;
	};
}