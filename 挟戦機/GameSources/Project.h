/*!
@file Project.h
@brief コンテンツ用のヘッダをまとめる
*/

#pragma once

#include "ProjectShader.h"
#include "ProjectBehavior.h"
#include "Scene.h"
#include "LoadCSV.h"
#include "GameStage.h"
#include "GameCamera.h"
#include "Character.h"
#include "Enemy.h"
#include "Player.h"
#include "Bullet.h"
#include "Spawner.h"
#include "Particle.h" 
#include "StageEffect.h"

//オブジェクトのデータ
enum DataID
{
	BLOCK = 1,//床と壁
	ENEMY = 2,
	AENEMY = 3,
	TELEPORT = 4,

};

enum SelectDifficult
{
	EASY,NORMAL,HARD,TUTORIAL,
};
