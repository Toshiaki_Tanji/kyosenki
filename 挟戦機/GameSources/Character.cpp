/*!
@file Character.cpp
@brief キャラクターなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {


	TileFloor::TileFloor(const shared_ptr<Stage>& StagePtr,
		UINT WidthTileCount,
		UINT HeightTileCount,
		const Vector3& Scale,
		const Quaternion& Qt,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_WidthCount(WidthTileCount),
		m_HeightCount(HeightTileCount),
		m_Scale(Scale),
		m_Qt(Qt),
		m_Position(Position)
	{
	}
	TileFloor::~TileFloor(){}

	void TileFloor::OnCreate() {
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale);
		PtrTrans->SetQuaternion(m_Qt);
		PtrTrans->SetPosition(m_Position);
		//AddComponent<CollisionRect>();

		vector<VertexPositionNormalTexture> vertices;
		vector<uint16_t> indices;
		MeshUtill::CreateSquare(1.0f, vertices, indices);
		float UCount = m_Scale.x / m_WidthCount;
		float VCount = m_Scale.y / m_HeightCount;
		for (size_t i = 0; i < vertices.size(); i++) {
			if (vertices[i].textureCoordinate.x >= 1.0f) {
				vertices[i].textureCoordinate.x = UCount;
			}
			if (vertices[i].textureCoordinate.y >= 1.0f) {
				vertices[i].textureCoordinate.y = VCount;
			}
		}
		//描画コンポーネントの追加
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		PtrDraw->CreateOriginalMesh(vertices, indices);
		PtrDraw->SetOriginalMeshUse(true);
		//PtrDraw->SetFogEnabled(true);
		//自分に影が映りこむようにする
		PtrDraw->SetOwnShadowActive(true);
		//描画コンポーネントテクスチャの設定
		PtrDraw->SetTextureResource(L"GALACY_TX");
		//タイリング設定
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);

	}


	//
	//モデル
	//
	EmptyModel::EmptyModel(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Modelkey
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_Modelkey(Modelkey)
	{
	}
	EmptyModel::~EmptyModel(){}
	
	void EmptyModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Position);
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		//サウンド登録
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"OpenTheAny");
		pMultiSoundEffect->AddAudioResource(L"Starting");


		//Rigidbodyをつける
		//auto PtrRigid = AddComponent<Rigidbody>();

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.75f, 0.75f, 0.75f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
		);

		if (m_Modelkey == L"Player") {
			auto PtrModelDraw = AddComponent<PNTStaticModelDraw>();
			SpanMat.DefTransformation(
				Vector3(0.75f, 0.75f, 0.75f),
				Vector3(0.0f, XM_PI, 0.0f),
				Vector3(0.0f, -1.5f, 0.0f)
			);	
			//影をつける
			auto ShadowPtr = AddComponent<Shadowmap>();
			ShadowPtr->SetMeshResource(L"PLAYER_MESH");
			ShadowPtr->SetMeshToTransformMatrix(SpanMat);
			PtrModelDraw->SetMeshResource(L"PLAYER_MESH");
			PtrModelDraw->SetTextureResource(L"PLAYER_TX");
			PtrModelDraw->SetMeshToTransformMatrix(SpanMat);


		}
		else if(m_Modelkey == L"Catapult"){
			auto PtrModelDraw = AddComponent<PNTBoneModelDraw>();
			SpanMat.DefTransformation(
				Vector3(0.75f, 0.75f, 0.75f),
				Vector3(0.0f, XM_PI, 0.0f),
				Vector3(0.0f, -1.5f, 0.0f)
			);
			//影をつける
			auto ShadowPtr = AddComponent<Shadowmap>();
			ShadowPtr->SetMeshResource(L"CATAPULT_MESH");
			ShadowPtr->SetMeshToTransformMatrix(SpanMat);
			PtrModelDraw->SetMeshResource(L"CATAPULT_MESH");
			PtrModelDraw->SetTextureResource(L"CATAPULT_TX");
			PtrModelDraw->SetMeshToTransformMatrix(SpanMat);

			PtrModelDraw->AddAnimation(L"Close", 1, 0, false, 30.0f);
			PtrModelDraw->AddAnimation(L"Open", 0, 180, false, 30.0f);

			PtrModelDraw->ChangeCurrentAnimation(L"Close");


		}


		//透明処理をする
		SetAlphaActive(true);
	}

	void EmptyModel::OnUpdate() {
		if (m_Modelkey == L"Catapult") {
			auto PtrDraw = GetComponent<PNTBoneModelDraw>();
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			PtrDraw->UpdateAnimation(ElapsedTime);
		}
	}
	//
	//隕石
	//
	MeteoRite::MeteoRite(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Modelkey
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_Modelkey(Modelkey)
	{
	}
	MeteoRite::~MeteoRite() {}

	void MeteoRite::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Position);
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);


		//Rigidbodyをつける
		auto PtrRigid = AddComponent<Rigidbody>();

		auto PtrCol = AddComponent<CollisionSphere>();
		PtrCol->SetFixed(true);
		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.25f, 0.25f, 0.25f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
		);

		auto PtrModelDraw = AddComponent<PNTStaticModelDraw>();
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(m_Modelkey);
		ShadowPtr->SetMeshToTransformMatrix(SpanMat);
		PtrModelDraw->SetMeshResource(m_Modelkey);
		PtrModelDraw->SetTextureResource(L"METEORITE_TX");
		PtrModelDraw->SetMeshToTransformMatrix(SpanMat);

		//透明処理をする
		//SetAlphaActive(true);
	}

	void MeteoRite::OnUpdate() {

		//auto PtrTrans = GetComponent<Transform>();
		//auto Pos = PtrTrans->GetPosition();
		//PtrTrans->SetPosition(Pos.x,0,Pos.z);
		//auto PtrRigid = GetComponent<Rigidbody>();
		//auto PtrVelo = PtrRigid->GetVelocity();
		//PtrRigid->SetVelocity(PtrVelo.x*0.98,0,PtrVelo.z*0.98);

	}

	//--------------------------------------------------------------------------------------
	///	固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FixedBox::FixedBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	FixedBox::~FixedBox() {}

	//初期化
	void FixedBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		/*auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");
*/
		//auto PtrDraw = AddComponent<PNTStaticDraw>();
		//PtrDraw->SetFogEnabled(true);
		//PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//PtrDraw->SetOwnShadowActive(true);
		//PtrDraw->SetTextureResource(L"BGTILE_TX");
	}

	//--------------------------------------------------------------------------------------
	//	敵
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Enemy::Enemy(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<StageCellMap>& CellMap,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Tag,
		const int& Durability
		//	const shared_ptr<BoosterSprite>& BoostPtr)
	) :
		GameObject(StagePtr),
		m_CelMap(CellMap),
		m_Scale(Scale),
		m_StartRotation(Rotation),
		m_StartPosition(Position),
		m_CellIndex(-1),
		m_NextCellIndex(-1),
		m_TargetCellIndex(-1),
		m_Tag(Tag),
		m_Durability(Durability)
		//m_BoostPtr(BoostPtr)
	{
	}
	Enemy::~Enemy() {}

	bool Enemy::SearchPlayer() {
		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			auto PathPtr = GetComponent<PathSearch>();
			auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
			auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
			m_CellPath.clear();
			//パス検索をかける
			if (PathPtr->SearchCell(PlayerPos, m_CellPath)) {
				//検索が成功した
				m_CellIndex = 0;
				m_TargetCellIndex = m_CellPath.size() - 1;
				if (m_CellIndex == m_TargetCellIndex) {
					//すでに同じセルにいる
					m_NextCellIndex = m_CellIndex;
				}
				else {
					//離れている
					m_NextCellIndex = m_CellIndex + 1;

				}
				return true;
			}
			else {
				//失敗した
				m_CellIndex = -1;
				m_NextCellIndex = -1;
				m_TargetCellIndex = -1;
			}
		}
		return false;
	}

	bool Enemy::DefaultBehavior() {
		auto PtrRigid = GetComponent<Rigidbody>();
		auto Velo = PtrRigid->GetVelocity();
		Velo *= 0.95f;
		PtrRigid->SetVelocity(Velo);
		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
			auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
			CellIndex PlayerCell;
			if (MapPtr->FindCell(PlayerPos, PlayerCell)) {
				return false;
			}
		}
		return true;
	}


	void Enemy::CatchState() {
		Catch = true;

		auto PLPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PLTrans = PLPtr->GetComponent<Transform>();
		auto PLPos = PLTrans->GetPosition();
		auto PLRot = PLTrans->GetRotation();

		auto MyTrans = GetComponent<Transform>();

		PLPos.x += sin(PLRot.y);
		PLPos.z += cos(PLRot.y);
		MyTrans->SetPosition(PLPos);

		auto ThisCol = GetComponent<CollisionObb>();
		ThisCol->SetUpdateActive(false);

		auto PtrRigidbody = PLPtr->GetComponent<Rigidbody>();
		//回転の更新
		//Velocityの値で、回転を変更する
		//これで進行方向を向くようになる
		Vector3 Velocity = PtrRigidbody->GetVelocity();
		if (Velocity.Length() > 0.0f) {
			Vector3 Temp = Velocity;
			Temp.Normalize();
			float ToAngle = atan2(Temp.x, Temp.z);
			Quaternion Qt;
			Qt.RotationRollPitchYaw(0, ToAngle, 0);
			Qt.Normalize();
			//現在の回転を取得
			Quaternion NowQt = PLTrans->GetQuaternion();
			//現在と目標を補間（10分の1）
			NowQt.Slerp(NowQt, Qt, 0.1f);
			MyTrans->SetQuaternion(NowQt);
		}



	}

	//投げられた時の状態
	void Enemy::ThrownMotion() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerRigid = PlayerPtr->GetComponent<Rigidbody>();
		auto PlayerRot = PlayerPtr->GetComponent<Transform>()->GetRotation().y;
		auto Rigid = GetComponent<Rigidbody>();
		auto Velo = Rigid->GetVelocity();

		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		ScenePtr->DestroyCount -= 1;

		Velo = Vector3(sin(PlayerRot),0,cos(PlayerRot));
		Velo * 5.0f;
		Rigid->SetVelocity(Velo);
	}

	void Enemy::SetDameged() {
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		//こいつのステータス
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetWorldPosition();

		if (this->FindTag(L"BOSS"))
			App::GetApp()->GetScene<Scene>()->GetGameParametors().m_BossHP--;
	
		this->m_Durability--;
		IsDamaged = true;

		if (this->m_Durability <= 0) {
			ScenePtr->DestroyFLG = true; DestroyFLG = true;
			ScenePtr->DestroyCount += 1;
			auto StagePtr = GetStage();
			if (ScenePtr->DestroyFLG = true) {
				StagePtr->AddGameObject<ExplosionSprite>(true, Vector2(1, 1), Vector3(Pos), L"Explosion_TX");
			}
			this->SetDrawActive(false);
			this->SetUpdateActive(false);
		}
	}

	//ダメージモーション
	void Enemy::DamegeMotion() {

		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		if (IsDamaged) {
			m_flashinterval += ElapsedTime;
			m_invisibleTime += ElapsedTime;
			if (m_invisibleTime > 0.5f) {
				IsDamaged = false;
				m_invisibleTime = 0.0f;
				ClearChanger = 1;
			}
			else if (m_flashinterval > 0.02f) {
				m_flashinterval = 0;
				ClearChanger *= -1;
			}

			if (!FindTag(L"BOSS")) {
				//描画コンポーネントの設定
				auto PtrDraw = AddComponent<PNTStaticDraw>();
				PtrDraw->SetDiffuse(Color4(1, 1, 1, ClearChanger));

			}
			else {
				auto PtrDraw = AddComponent<PNTBoneModelDraw>();
				PtrDraw->SetDiffuse(Color4(1, 1, 1, ClearChanger));
			}
		}
	}


	bool Enemy::SeekBehavior() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto MyPos = GetComponent<Transform>()->GetPosition();

		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			if (SearchPlayer()) {
				auto PtrSeek = GetBehavior<SeekSteering>();
				if (m_NextCellIndex == 0) {
					auto PtrRigid = GetComponent<Rigidbody>();
					auto Velo = PtrRigid->GetVelocity();
					Velo *= 0.5f;
					PtrRigid->SetVelocity(Velo);
					PlayerPos.y = m_StartPosition.y;
					PtrSeek->Execute(PlayerPos);
				}
				else {
					if (Vector3EX::Length(MyPos - PlayerPos) <= 3.0f) {
						auto PtrRigid = GetComponent<Rigidbody>();
						auto Velo = PtrRigid->GetVelocity();
						Velo *= 0.55f;
						PtrRigid->SetVelocity(Velo);
					}
					AABB ret;
					MapPtr->FindAABB(m_CellPath[m_NextCellIndex], ret);
					auto Pos = ret.GetCenter();
					Pos.y = m_StartPosition.y;
					PtrSeek->Execute(Pos);
				}
				return true;
			}
			else {
				auto PtrSeek = GetBehavior<SeekSteering>();
				CellIndex PlayerCell;
				if (MapPtr->FindCell(PlayerPos, PlayerCell)) {
					AABB ret;
					MapPtr->FindAABB(PlayerCell, ret);
					auto Pos = ret.GetCenter();
					Pos.y = m_StartPosition.y;
					PtrSeek->Execute(Pos);
					return true;
				}
			}
		}
		return false;
	}

	void Enemy::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		for (auto &v : OtherVec) {
			//シーン
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			//こいつのステータス
			auto Trans = GetComponent<Transform>();
			auto Pos = Trans->GetWorldPosition();
			//ファンネル
			auto shFunnel = dynamic_pointer_cast<CatchFunnel>(v);
			//弾
			auto shBullet = dynamic_pointer_cast<AttackBall>(v);
			auto shLaser = dynamic_pointer_cast<AttackLaser>(v);
			auto shMissile = dynamic_pointer_cast<AttackMissile>(v);
			//ダメージ食らう
			if (shBullet || shLaser || shMissile) {
				//if (this->FindTag(L"BOSS"))
					//	App::GetApp()->GetScene<Scene>()->GetGameParametors().m_BossHP--;
					//this->m_Durability--;
					//IsDamaged = true;
					//if (this->m_Durability <= 0) {
					//	ScenePtr->DestroyFLG = true; DestroyFLG = true;
					//	ScenePtr->DestroyCount += 1;
					//	auto StagePtr = GetStage();
					//	if (ScenePtr->DestroyFLG = true) {
					//		StagePtr->AddGameObject<ExplosionSprite>(true, Vector2(1, 1), Vector3(Pos), L"Explosion_TX");
					//	}
					//	this->SetDrawActive(false);
					//	this->SetUpdateActive(false);

					this->SetDameged();
				//}
			}
			if (shFunnel) {
				if (shFunnel->IsCatcher() == true) {
					m_StateMachine->ChangeState(EnemyGetPlayer::Instance());
					shFunnel->CatchEnemy();
					this->CatchState();
					ScenePtr->DestroyCount += 1;
					App::GetApp()->GetScene<Scene>()->GetGameParametors().m_PLGetEnemy = true;
					if (this->FindTag(L"Enemy1")) {
						App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType = 1;
					}
					else if (this->FindTag(L"Enemy2")) {
						App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType = 2;
					}
					else if (this->FindTag(L"Enemy3")) {
						App::GetApp()->GetScene<Scene>()->GetGameParametors().m_EnemyType = 3;
					}
				}
			}
		}
	}

	//初期化
	void Enemy::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		auto Pos = PtrTransform->GetPosition();
		PtrTransform->SetPosition(m_StartPosition);
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_StartRotation);

		AddTag(m_Tag);

		//Rigidbodyをつける
		auto PtrRigid = AddComponent<Rigidbody>();
		//パス検索
		auto MapPtr = m_CelMap.lock();
		if (!MapPtr) {
			throw BaseException(
				L"セルマップが不定です",
				L"if (!MapPtr) ",
				L" Enemy::OnCreate()"
			);
		}
		auto PathPtr = AddComponent<PathSearch>(MapPtr);

		//SPの衝突判定をつける
		auto PtrColl = AddComponent<CollisionObb>();
		PtrColl->SetIsHitAction(IsHitAction::Auto);
		//PtrColl->SetDrawActive(true);

		Matrix4X4 SpanMat; // モデルとトランスフォームの間の差分行列
		SpanMat.DefTransformation(
			Vector3(0.75f, 0.75f, 0.75f),
			Vector3(0.0f, XM_PI, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
		);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();


		if (FindTag(L"BOSS")) {
			auto PtrModelDraw = AddComponent<PNTBoneModelDraw>();

			SpanMat.DefTransformation(
				Vector3(0.75f, 0.75f, 0.75f),
				Vector3(0.0f, XM_PI, 0.0f),
				Vector3(0.0f, -1.5f, 0.0f)
			);

			PtrRigid->SetReflection(0.5f);
			ShadowPtr->SetMeshResource(L"BOSS_MESH");
			ShadowPtr->SetMeshToTransformMatrix(SpanMat);
			//PtrModelDraw->SetFogEnabled(true);
			PtrModelDraw->SetMeshResource(L"BOSS_MESH");
			PtrModelDraw->SetTextureResource(L"BOSS_TX");
			PtrModelDraw->SetMeshToTransformMatrix(SpanMat);

			PtrModelDraw->AddAnimation(L"Default", 1, 0, false, 30.0f);
			PtrModelDraw->AddAnimation(L"Morfing", 0, 180, false, 30.0f);

			PtrModelDraw->ChangeCurrentAnimation(L"Default");

			App::GetApp()->GetScene<Scene>()->GetGameParametors().m_BossHP = m_Durability;

		}
		else if (FindTag(L"Enemy1")) {
			auto PtrDraw = AddComponent<PNTStaticDraw>();

			ShadowPtr->SetMeshResource(L"ZAKOENEMY1_MESH");
			ShadowPtr->SetMeshToTransformMatrix(SpanMat);
			//PtrDraw->SetFogEnabled(true);
			PtrDraw->SetMeshResource(L"ZAKOENEMY1_MESH");
			PtrDraw->SetTextureResource(L"ZAKOENEMY1_TX");
			PtrDraw->SetMeshToTransformMatrix(SpanMat);

		}
		else if (FindTag(L"Enemy2")) {
			auto PtrDraw = AddComponent<PNTStaticDraw>();

			ShadowPtr->SetMeshResource(L"ZAKOENEMY2_MESH");
			ShadowPtr->SetMeshToTransformMatrix(SpanMat);
			//PtrDraw->SetFogEnabled(true);
			PtrDraw->SetMeshResource(L"ZAKOENEMY2_MESH");
			PtrDraw->SetTextureResource(L"ZAKOENEMY2_TX");
			PtrDraw->SetMeshToTransformMatrix(SpanMat);

		}
		else if (FindTag(L"Enemy3")) {
			auto PtrDraw = AddComponent<PNTStaticDraw>();

			ShadowPtr->SetMeshResource(L"ZAKOENEMY3_MESH");
			ShadowPtr->SetMeshToTransformMatrix(SpanMat);
			//PtrDraw->SetFogEnabled(true);
			PtrDraw->SetMeshResource(L"ZAKOENEMY3_MESH");
			PtrDraw->SetTextureResource(L"ZAKOENEMY3_TX");
			PtrDraw->SetMeshToTransformMatrix(SpanMat);

		}

		auto StagePtr = GetStage();
		//auto CreateBoost = StagePtr->AddGameObject<BoosterSprite>(true, Vector2(1, 1), Vector3(Pos), L"Boost_TX", GetThis<Enemy>());
	
		auto Group = GetStage()->GetSharedObjectGroup(L"Enemys");
		Group->IntoGroup(GetThis<Enemy>());
		//透明処理をする
		SetAlphaActive(true);


		//弾同士での
		AddTag(L"EnemyCol");
		PtrColl->AddExcludeCollisionTag(L"EnemyCol");


		m_StateMachine = make_shared<StateMachine<Enemy>>(GetThis<Enemy>());
		m_StateMachine->ChangeState(EnemyDefault::Instance());
	}

	//更新
	void Enemy::OnUpdate() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");

		//Playerが死んだときEnemy停止
		if (PlayerPtr->m_Armor <= 0)
		{
			SetUpdateActive(false);
		}
		


		//ステートによって変わらない行動を実行
		auto PtrGrav = GetBehavior<Gravity>();
		if (!FindTag(L"Enemy1")) {
			//PtrGrav->Execute();
		}

		//アニメーションを更新する
		if (FindTag(L"BOSS")) {
			auto PtrDraw = GetComponent<PNTBoneModelDraw>();
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			PtrDraw->UpdateAnimation(ElapsedTime);
		}

		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();

		//ダメージを食らった時のモーション
		DamegeMotion();

	}

	void Enemy::OnUpdate2() {

		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		//シーン呼ぶ
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto BoostPtr = GetStage()->GetSharedGameObject<BoosterSprite>(L"Boost", false);
		auto PtrBoost = GetStage()->GetSharedObjectGroup(L"Boost", false);

		//進行方向を向くようにする
		RotToHead();

		auto PtrGrav = GetBehavior<Gravity>();
		PtrGrav->Execute();


		auto PtrFunnnel = GetStage()->GetSharedGameObject<CatchFunnel>(L"FUNNEL_R");
		if (PtrFunnnel->ThrowEnemy() && m_StateMachine->GetCurrentState() == EnemyGetPlayer::Instance ()) {
			m_StateMachine->ChangeState(EnemyDefault::Instance());
		}
	}


	//進行方向を向くようにする
	void Enemy::RotToHead() {
		auto PtrRigidbody = GetComponent<Rigidbody>();
		//回転の更新
		//Velocityの値で、回転を変更する
		//これで進行方向を向くようになる
		auto PtrTransform = GetComponent<Transform>();
		Vector3 Velocity = PtrRigidbody->GetVelocity();
		if (Velocity.Length() > 0.0f) {
			Vector3 Temp = Velocity;
			Temp.Normalize();
			float ToAngle = atan2(Temp.x, Temp.z);
			Quaternion Qt;
			Qt.RotationRollPitchYaw(0, ToAngle, 0);
			Qt.Normalize();
			//現在の回転を取得
			Quaternion NowQt = PtrTransform->GetQuaternion();
			//現在と目標を補間（10分の1）
			NowQt.Slerp(NowQt, Qt, 0.1f);
			PtrTransform->SetQuaternion(NowQt);
		}
	}


	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	IMPLEMENT_SINGLETON_INSTANCE(EnemyDefault)

		void EnemyDefault::Enter(const shared_ptr<Enemy>& Obj) {
	}

	void EnemyDefault::Execute(const shared_ptr<Enemy>& Obj) {
		if (!Obj->DefaultBehavior()) {
			Obj->GetStateMachine()->ChangeState(EnemySeek::Instance());
		}

	}
	void EnemyDefault::Exit(const shared_ptr<Enemy>& Obj) {
	}

	//--------------------------------------------------------------------------------------
	///	プレイヤーを追いかけるステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	IMPLEMENT_SINGLETON_INSTANCE(EnemySeek)

		void EnemySeek::Enter(const shared_ptr<Enemy>& Obj) {
	}

	void EnemySeek::Execute(const shared_ptr<Enemy>& Obj) {
		if (!Obj->SeekBehavior()) {
			Obj->GetStateMachine()->ChangeState(EnemyDefault::Instance());
		}
	}

	void EnemySeek::Exit(const shared_ptr<Enemy>& Obj) {
	}

	//--------------------------------------------------------------------------------------
	///	プレイヤーにつかまるステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	IMPLEMENT_SINGLETON_INSTANCE(EnemyGetPlayer)

		void EnemyGetPlayer::Enter(const shared_ptr<Enemy>& Obj) {

	}

	void EnemyGetPlayer::Execute(const shared_ptr<Enemy>& Obj) {

		if (!Obj->FindTag(L"BOSS")) {
			Obj->GetComponent<Rigidbody>()->SetUpdateActive(false);
			Obj->CatchState();
		}
		else if (!Obj->SeekBehavior()) {
			Obj->GetStateMachine()->ChangeState(EnemyDefault::Instance());
		}
		else if (!Obj->DefaultBehavior()) {
			Obj->GetStateMachine()->ChangeState(EnemySeek::Instance());
		}
	}
	void EnemyGetPlayer::Exit(const shared_ptr<Enemy>& Obj) {
		Obj->GetComponent<CollisionObb>()->SetUpdateActive(true);
		Obj->SetUpdateActive(true);
		Obj->ThrownMotion();
	}

	//--------------------------------------------------------------------------------------
	//	強い敵
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TestCellChangeEnemy::TestCellChangeEnemy(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<StageCellMap>& CellMap,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Tag,
		const int& Durability
	) :
		Enemy(StagePtr, CellMap, Scale, Rotation, Position, Tag, Durability)
	{}
	TestCellChangeEnemy::~TestCellChangeEnemy() {}

	bool TestCellChangeEnemy::DefaultBehavior() {
		auto PtrRigid = GetComponent<Rigidbody>();
		auto Velo = PtrRigid->GetVelocity();
		Velo *= 0.95f;
		PtrRigid->SetVelocity(Velo);
		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
			auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
			CellIndex PlayerCell;
			if (MapPtr->FindCell(PlayerPos, PlayerCell)) {
				//プレイヤーがセルマップ上に入った。
				return false;
			}
			else {
				//プレイヤーはマップ上にいない
				//マップをプレイヤーの周りに再設定
				Vector3  CellmapStart;
				CellmapStart.x = float((int)(PlayerPos.x - 2.0f));
				CellmapStart.z = float((int)(PlayerPos.z - 2.0f));
				CellmapStart.y = 0.0f;
				MapPtr->RefleshCellMap(CellmapStart, 1.0f, 4, 4);
				auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
				GameStagePtr->SetCellMapCost(L"CellMap2");
			}
		}
		return true;
	}



	//--------------------------------------------------------------------------------------
	//	boss
	//--------------------------------------------------------------------------------------
	//構築と破棄
	BossEnemy::BossEnemy(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<StageCellMap>& CellMap,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Tag,
		const int& Durability
	) :
		Enemy(StagePtr, CellMap, Scale, Rotation, Position, Tag, Durability)
	{}
	BossEnemy::~BossEnemy() {}

	bool BossEnemy::DefaultBehavior() {
	
		//if (MapPtr) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTrans = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerTrans->GetPosition();
		auto PlayerRot = PlayerTrans->GetRotation();
		//自分のポジション
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		auto Rot = Trans->GetRotation();

		auto PtrRigid = GetComponent<Rigidbody>();
		PtrRigid->SetReflection(0.0f);
		auto Velo = PtrRigid->GetVelocity();

		//cosθ
		auto dx = PlayerPos.x - Pos.x;
		//sinθ
		auto dz = PlayerPos.z - Pos.z;
		//tan
		float tWork = dz / dx;
		//目的の角度取得
		float angle = atan2(dx, dz);

		//目的の角度（Player）へ向ける
		Rot = Vector3(0.0f, angle, 0.0f);

		Trans->SetPosition(Pos);
		Trans->SetRotation(Rot);

		if (true){//AttackLength()) {
			BossAttackBullet();
			BossAttackLaser();
			BossAttackMissile();
		}
		else {
			Velo *= 0.95f;
			PtrRigid->SetVelocity(Velo);
			auto MapPtr = m_CelMap.lock();
			if (MapPtr) {
				auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
				auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
				CellIndex PlayerCell;
				if (MapPtr->FindCell(PlayerPos, PlayerCell)) {
					//プレイヤーがセルマップ上に入った。
					return false;
				}
				else {
					//プレイヤーはマップ上にいない
					//マップをプレイヤーの周りに再設定
					Vector3  CellmapStart;
					CellmapStart.x = float((int)(PlayerPos.x - 2.0f));
					CellmapStart.z = float((int)(PlayerPos.z - 2.0f));
					CellmapStart.y = 0.0f;
					MapPtr->RefleshCellMap(CellmapStart, 1.0f, 4, 4);
					auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
					GameStagePtr->SetCellMapCost(L"CellMap2");
				}
			}
		}
		return true;

	}

	//プレイヤーに弾を撃つ距離にいるかどうか
	bool BossEnemy::AttackLength() {
		auto ThisTrans = GetComponent<Transform>();
		auto ThisPos = ThisTrans->GetPosition();
		auto Rigid = GetComponent<Rigidbody>();

		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();

		auto LengthPos = Vector3EX::Length(PlayerPos - ThisPos);;

		if (LengthPos <= 10) {
			ThisPos = ThisTrans->GetBeforePosition();
			Rigid->SetVelocityZero();
			ThisTrans->SetPosition(ThisPos);
			return true;
		}
		return false;
	}

	//弾を撃つ関数
	void BossEnemy::BossAttackBullet() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto Trans = GetComponent<Transform>();
		//弾の発射
		if (BulletFire)
		{
			auto Group = GetStage()->GetSharedObjectGroup(L"AttackBall");
			for (auto& v : Group->GetGroupVector()) {
				auto shptr = v.lock();
				if (shptr) {
					auto AttackPtr = dynamic_pointer_cast<AttackBall>(shptr);
					if (AttackPtr && !AttackPtr->IsUpdateActive()) {
						//回転の計算
						auto RotY = Trans->GetRotation().y;
						auto Angle = Vector3(sin(RotY), 0, cos(RotY));
						Quaternion Qt;
						Qt.RotationRollPitchYaw(0, RotY, 0);
						Qt.Normalize();
						Angle.Normalize();
						auto Span = Angle * 1.5f;
						auto Pos = Trans->GetPosition();
						Pos.y = PlayerPtr->GetComponent<Transform>()->GetPosition().y;
						AttackPtr->Weakup(Pos + Span, Qt, Angle * 15.0f);
						//弾を撃った
						BulletFire = false;
						return;
					}
				}
			}
		}
	}

	//-----------------
	//レーザーを撃つの
	void BossEnemy::BossAttackLaser() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto Trans = GetComponent<Transform>();
			//弾の発射
		if (BulletFire)
		{
			auto Group = GetStage()->GetSharedObjectGroup(L"AttackLaser");
			for (auto& v : Group->GetGroupVector()) {
				auto shptr = v.lock();
				if (shptr) {
					auto AttackPtr = dynamic_pointer_cast<AttackLaser>(shptr);
					if (AttackPtr && !AttackPtr->IsUpdateActive()) {
						//回転の計算
						auto RotY = Trans->GetRotation().y;
						auto Angle = Vector3(sin(RotY), 0, cos(RotY));
						Quaternion Qt;
						Qt.RotationRollPitchYaw(0, RotY, 0);
						Qt.Normalize();
						Angle.Normalize();
						auto Span = Angle * 1.5f;
						auto Pos = Trans->GetPosition();
						Pos.y = PlayerPtr->GetComponent<Transform>()->GetPosition().y;
						AttackPtr->Weakup(Pos + Span, Qt, Angle * 20.0f);
						//弾を撃った
						BulletFire = false;
						return;
					}
				}
			}
		}
	}

	//---------------------------------------
	//ミサイル
	void BossEnemy::BossAttackMissile() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto Trans = GetComponent<Transform>();
			//弾の発射
			if (BulletFire)
			{
				auto Group = GetStage()->GetSharedObjectGroup(L"AttackMissile");
				for (auto& v : Group->GetGroupVector()) {
					auto shptr = v.lock();
					if (shptr) {
						auto AttackPtr = dynamic_pointer_cast<AttackMissile>(shptr);
						if (AttackPtr && !AttackPtr->IsUpdateActive()) {
							//回転の計算
							auto RotY = Trans->GetRotation().y;
							auto Angle = Vector3(sin(RotY), 0, cos(RotY));
							Quaternion Qt;
							Qt.RotationRollPitchYaw(0, RotY, 0);
							Qt.Normalize();
							Angle.Normalize();
							auto Span = Angle * 1.5f;
							auto Pos = Trans->GetPosition();
							Pos.y = PlayerPtr->GetComponent<Transform>()->GetPosition().y;
							AttackPtr->Weakup(Pos + Span, Qt, Angle * 10.0f);
							//弾を撃った
							BulletFire = false;
							return;
						}
					}
				}
			}
	}

	void BossEnemy::AttackMove() {

	}

	void BossEnemy::AroundPL() {

	}

	//--------------------------------------------------------------------------------------
	//	弱い敵
	//--------------------------------------------------------------------------------------
	//構築と破棄
	WeakEnemy::WeakEnemy(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<StageCellMap>& CellMap,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Tag,
		const int& Durability

	) :
		Enemy(StagePtr, CellMap, Scale, Rotation, Position, Tag, Durability)
	{}
	WeakEnemy::~WeakEnemy() {}

	void WeakEnemy::PlayrSearch() {

		//if (MapPtr) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		//自分のポジション
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();


		//cosθ
		dx = PlayerPos.x - Pos.x;
		//sinθ
		dz = PlayerPos.z - Pos.z;
		//tan
		float tWork = dz / dx;
		angle = atan2(dz, dx);

	}

	void WeakEnemy::OnUpdate()
	{

		m_StateMachine->Update();

		auto PtrRigid = GetComponent<Rigidbody>();
		//	auto Velo = PtrRigid->GetVelocity();
		//	Velo *= 0.95f;
		////	PtrRigid->SetVelocity(Velo);
		//	auto MapPtr = m_CelMap.lock();



		//if (MapPtr) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		//自分のポジション
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		//最初のプレイヤーの位置を格納
		auto difference = PlayerPos;
		auto ElapsedTime = App::GetApp()->GetElapsedTime();





		if (Search) {

			Stop = false;

			StopTime = 3;

			PlayrSearch();

			SearchTime -= ElapsedTime;

			if (SearchTime <= 0)
			{

				Search = false;

			}
		}




		if (!Search)
		{
			SearchTime = 2;
			MoveTime -= ElapsedTime;
			if (MoveTime <= 0 && !Stop)
			{
				//Search = true;
				Stop = true;

			}
			if (Stop)
			{
				MoveTime = 2;
				StopTime -= ElapsedTime;
				auto rigi = AddComponent<Rigidbody>();

				rigi->SetVelocity(0.0f, 0.0f, 0.0f);
				BulletFireCount--;

				if (BulletFireCount == 150 || BulletFireCount == 90 || BulletFireCount == 30)
				{
					auto Group = GetStage()->GetSharedObjectGroup(L"EnemyAttackBall");
					for (auto& v : Group->GetGroupVector()) {
						auto shptr = v.lock();
						if (shptr) {
							auto AttackPtr = dynamic_pointer_cast<EnemyAttackBall>(shptr);
							if (AttackPtr && !AttackPtr->IsUpdateActive()) {
								//回転の計算
								auto RotY = Trans->GetRotation().y;
								auto Angle = Vector3(sin(RotY), 0, cos(RotY));
								Angle.Normalize();
								Quaternion Qt;
								Qt.RotationRollPitchYaw(0, RotY, 0);
								Qt.Normalize();
								auto Span = Angle * 1.5f;
								AttackPtr->Weakup(Trans->GetPosition() + Span, Qt, Angle * 12.5f);
								//AttackCoolTime = 60;
								return;
							}
							//}
						}

					}
				}

				if (StopTime <= 0)
				{

					Search = true;

				}
			}




		}


		if (!Stop)
		{
			Vector3 tintin(dx, 0, dz);
			tintin.Normalize();
			BulletFireCount = 180;
			PlayrSearch();
			//Trans->SetPosition(angle,Pos.y,angle);
			auto rigi = AddComponent<Rigidbody>();

			rigi->SetVelocity(tintin * 6);




		}







	}
	//--------------------------------------------------------------------------------------
	//	プレイヤーを狙ってくる敵
	//--------------------------------------------------------------------------------------
	//構築と破棄
	AimingEnemys::AimingEnemys(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<StageCellMap>& CellMap,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Tag,
		const int& Durability

	) :
		Enemy(StagePtr, CellMap, Scale, Rotation, Position, Tag, Durability)
	{}
	AimingEnemys::~AimingEnemys() {}

	void AimingEnemys::PlayrSearch() {


		//if (MapPtr) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTrans = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerTrans->GetPosition();
		auto PlayerRot = PlayerTrans->GetRotation();
		//自分のポジション
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		auto Rot = Trans->GetRotation();



		//cosθ
		dx = PlayerPos.x - Pos.x;
		//sinθ
		dz = PlayerPos.z - Pos.z;
		//tan
		float tWork = dz / dx;
		//目的の角度取得
		angle = atan2(dx, dz);

		//目的の角度（Player）へ向ける
		Rot = Vector3(0.0f, angle, 0.0f);

		Trans->SetPosition(Pos);
		//Trans->SetQuaternion(Qt);
		Trans->SetRotation(Rot);




	}

	void AimingEnemys::OnUpdate() {
		m_StateMachine->Update();

		auto PtrRigid = GetComponent<Rigidbody>();
		auto Velo = PtrRigid->GetVelocity();
		Velo *= 0.95f;
		PtrRigid->SetVelocity(Velo);
		auto MapPtr = m_CelMap.lock();


		//if (MapPtr) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		//auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		//	auto PlayerTrans = PlayerPtr->GetComponent<Transform>();
		//	auto PlayerRot = PlayerTrans->GetRotation();
		//自分のポジション
		auto Trans = GetComponent<Transform>();
		//auto Pos = Trans->GetPosition();
		//auto Rot = Trans->GetRotation();


		//GetStateMachine()->ChangeState(AimingEnemys_AttackState::Instance());

		if (Search) {

			CoolTime = 60;
			PlayrSearch();
			SearchTime--;

			if (SearchTime <= 0)
			{

				Search = false;
				BulletFire = true;


			}
		}


		if (!Search)
		{
			CoolTime--;
			SearchTime = 60;


			//弾の発射
			if (BulletFire)
			{
				//AttackCoolTime--;
				//if (AttackCoolTime <= 0) {         
				auto Group = GetStage()->GetSharedObjectGroup(L"EnemyAttackBall");
				for (auto& v : Group->GetGroupVector()) {
					auto shptr = v.lock();
					if (shptr) {
						auto AttackPtr = dynamic_pointer_cast<EnemyAttackBall>(shptr);
						if (AttackPtr && !AttackPtr->IsUpdateActive()) {
							//回転の計算
							auto RotY = Trans->GetRotation().y;
							auto Angle = Vector3(sin(RotY), 0, cos(RotY));
							Angle.Normalize();
							Quaternion Qt;
							Qt.RotationRollPitchYaw(0, RotY, 0);
							Qt.Normalize();
							auto Span = Angle * 1.5f;
							AttackPtr->Weakup(Trans->GetPosition() + Span, Qt, Angle * 12.5f);
							//AttackCoolTime = 60;
							return;
						}
						//}
					}
					BulletFire = false;
					//}
				}
			}



			if (CoolTime <= 0)
			{
				Search = true;


			}
		}




		////cosθ
		//dx = PlayerPos.x - Pos.x;
		////sinθ
		//dz = PlayerPos.z - Pos.z;
		////tan
		//float tWork = dz / dx;
		//angle = atan2(dx, dz);


		//
		////目的の角度（Player）へ向ける
		//Rot = Vector3(0.0f, angle,0.0f);
		//Pos = EnemyPos;

		//Enemyにポジション合わせる



		//Trans->SetPosition(angle,Pos.y,angle);
		auto rigi = AddComponent<Rigidbody>();

		//rigi->SetVelocity(tintin * 6);


		MoveTime--;


	}



	//--------------------------------------------------------------------------------------
	//	プレイヤーを狙ってくる敵
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TeleportEnemys::TeleportEnemys(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<StageCellMap>& CellMap,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Tag,
		const int& Durability

	) :
		Enemy(StagePtr, CellMap, Scale, Rotation, Position, Tag, Durability)
	{}
	TeleportEnemys::~TeleportEnemys() {}

	void TeleportEnemys::OnCreate()
	{
		//ステートマシンの構築
		m_TeleportStateMachine.reset(new LayeredStateMachine<TeleportEnemys>(GetThis<TeleportEnemys>()));
		//最初のステートをPlayerDefaultにリセット
		m_TeleportStateMachine->Reset(TeleportDefaultState::Instance());



		Enemy::OnCreate();

	}

	void TeleportEnemys::PlayrSearch() {


		////if (MapPtr) {
		//auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		//auto PlayerTrans = PlayerPtr->GetComponent<Transform>();
		//auto PlayerPos = PlayerTrans->GetPosition();
		//auto PlayerRot = PlayerTrans->GetRotation();
		////自分のポジション
		//auto Trans = GetComponent<Transform>();
		//auto Pos = Trans->GetPosition();
		//auto Rot = Trans->GetRotation();



		////cosθ
		//dx = PlayerPos.x - Pos.x;
		////sinθ
		//dz = PlayerPos.z - Pos.z;
		////tan
		//float tWork = dz / dx;
		////目的の角度取得
		//angle = atan2(dx, dz);

		////目的の角度（Player）へ向ける
		//Rot = Vector3(0.0f, angle, 0.0f);

		//Trans->SetPosition(Pos);
		////Trans->SetQuaternion(Qt);
		//Trans->SetRotation(Rot);




	}

	void TeleportEnemys::OnUpdate() {

		m_StateMachine->Update();

		//m_TeleportStateMachine->Update();

		//auto PtrRigid = GetComponent<Rigidbody>();
		//auto Velo = PtrRigid->GetVelocity();
		//Velo *= 0.95f;
		//PtrRigid->SetVelocity(Velo);
		//auto MapPtr = m_CelMap.lock();
		//auto ElapsedTime = App::GetApp()->GetElapsedTime();


		////if (MapPtr) {
		//auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		//auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		//	//auto PlayerTrans = PlayerPtr->GetComponent<Transform>();
		////	auto PlayerRot = PlayerTrans->GetRotation();
		////自分のポジション
		//auto Trans = GetComponent<Transform>();
		//auto Pos = Trans->GetPosition();

		//

		////GetStateMachine()->ChangeState(AimingEnemys_AttackState::Instance());
		//
		//if (TeleportType >= 3)
		//{
		//	TeleportType = 0;
		//}


		//if (TeleportCoolTime <= 0)
		//{
		//	Teleport = true;

		//}



		//if (Teleport)
		//{
		//	switch (TeleportType)
		//	{
		//	case 0:
		//		Trans->SetPosition(Pos.x + 0.5, Pos.y, Pos.z);
		//		TeleportCoolTime = 180;
		//		Teleport = false;
		//		TeleportType += 1;
		//		Search = true;
		//		break;

		//	case 1:
		//		Trans->SetPosition(Pos.x - 0.5, Pos.y, Pos.z);
		//		TeleportCoolTime = 180;
		//		Teleport = false;
		//		TeleportType += 1;
		//		Search = true;
		//		break;

		//	case 2:
		//		Trans->SetPosition(Pos.x, PlayerPos.y, Pos.z + 0.5);
		//		TeleportCoolTime = 180;
		//		Teleport = false;
		//		TeleportType += 1;
		//		Search = true;
		//		break;

		//	case 3:
		//		Trans->SetPosition(Pos.x, PlayerPos.y, Pos.z - 0.5);
		//		TeleportCoolTime = 180;
		//		Teleport = false;
		//		TeleportType += 1;
		//		Search = true;
		//		break;

		//	}

		//}
		//








		//if (Search) {

		//	CoolTime = 60;
		//	PlayrSearch();
		//	SearchTime--;

		//	if (SearchTime <= 0)
		//	{

		//		Search = false;
		//		BulletFire = true;
		//		SearchTime = 60;

		//	}
		//}


		//if (!Search)
		//{
		//	CoolTime--;
		//

		//
		//	



		//	//弾の発射
		//	if (BulletFire)
		//	{
		//		//AttackCoolTime--;
		//		//if (AttackCoolTime <= 0) {         
		//		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyAttackLaser");
		//		for (auto& v : Group->GetGroupVector()) {
		//			auto shptr = v.lock();
		//			if (shptr) {
		//				auto AttackPtr = dynamic_pointer_cast<EnemyAttackLaser>(shptr);
		//				if (AttackPtr && !AttackPtr->IsUpdateActive()) {
		//					//回転の計算
		//					auto RotY = Trans->GetRotation().y;
		//					auto Angle = Vector3(sin(RotY), 0, cos(RotY));
		//					Angle.Normalize();
		//					Quaternion Qt;
		//					Qt.RotationRollPitchYaw(0, RotY, 0);
		//					Qt.Normalize();
		//					auto Span = Angle * 1.5f;
		//					AttackPtr->Weakup(Trans->GetPosition() + Span, Qt, Angle * 12.5f);
		//					//AttackCoolTime = 60;
		//					return;
		//				}
		//				//}
		//			}
		//			BulletFire = false;
		//			//}
		//		}
		//	}



		//	if (CoolTime <= 0)
		//	{
		//		Teleport = true;
		//		TeleportCoolTime--;


		//	}
		//}






		////Trans->SetPosition(angle,Pos.y,angle);
		//auto rigi = AddComponent<Rigidbody>();

		////rigi->SetVelocity(tintin * 6);


		//MoveTime--;


	}

	void TeleportEnemys::OnUpdate2() {


		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetWorldPosition();
		auto Scale = Trans->GetScale();
		//	auto ss = GetStage()->AddGameObject<StringSprite>();

		//シーン呼ぶ
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto BoostPtr = GetStage()->GetSharedGameObject<BoosterSprite>(L"Boost", false);
		auto PtrBoost = GetStage()->GetSharedObjectGroup(L"Boost", false);

		auto LaserPtr = GetStage()->GetSharedGameObject<LaserEffect>(L"LaserEffect");
		auto LaserTrans = LaserPtr->GetComponent<Transform>();
		auto LaserScale = LaserTrans->GetScale();
		auto LaserPos = LaserTrans->GetWorldPosition();

		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player", false);
		auto PlayerTrans = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerTrans->GetWorldPosition();


		//LaserTrans->SetPosition(PlayerPos);
		//Vector3 length = LaserPos -Pos;
		//auto xx = Pos.x-LaserPos.x;
		//auto zz = Pos.z-LaserPos.z;
		ScenePtr->TPos = Pos;


		///*float Lr = LaserScale.x*LaserPos.z/2;
		//auto r=Scale.x*Scale.z/2;*/
		//	//length- Pos;
		//
		//	if ((xx*xx+zz*zz))			{
		//		this->SetDrawActive(false);
		//		this->SetUpdateActive(false);

		//	}
		;
		//length += PlayerPos;

		//auto Group = GetStage()->GetSharedObjectGroup(L"Laser");
		//for (auto& v : Group->GetGroupVector()) {
		//	auto shptr = v.lock();
		//	if (shptr) {
		//		//length += PlayerPos;
		//		if (length.x <= 0.5&&length.x >= 0 && length.z <= 0.5&&length.z >= 0
		//			|| length.x >= -0.5&&length.x <= 0 && length.z >= -0.5&&length.z <= 0)
		//		{
		//			this->SetDrawActive(false);
		//			this->SetUpdateActive(false);

		//		}
		//	}
		//}




		if (PlayerPtr->m_Armor <= 0)
		{
			SetUpdateActive(false);
		}
		m_TeleportStateMachine->Update();


	}
	//--------------------------------------------------------------------------------------
	///	通常ステート
	//--------------------------------------------------------------------------------------

	IMPLEMENT_SINGLETON_INSTANCE(TeleportDefaultState)

		void TeleportDefaultState::Enter(const shared_ptr<TeleportEnemys>& Obj)
	{

	}

	void TeleportDefaultState::Execute(const shared_ptr<TeleportEnemys>& Obj)
	{

		auto PtrRigid = Obj->GetComponent<Rigidbody>();
		auto Velo = PtrRigid->GetVelocity();
		Velo *= 0.95f;
		PtrRigid->SetVelocity(Velo);

		//if (MapPtr) {
		auto PlayerPtr = Obj->GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTrans = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerTrans->GetPosition();
		auto PlayerRot = PlayerTrans->GetRotation();
		//自分のポジション
		auto Trans = Obj->GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		auto Rot = Trans->GetRotation();

		auto ElapsedTime = App::GetApp()->GetElapsedTime();

		//cosθ
		float dx = PlayerPos.x - Pos.x;
		//sinθ
		float dz = PlayerPos.z - Pos.z;
		//tan
		float tWork = dz / dx;
		//目的の角度取得
		float angle = atan2(dx, dz);

		//目的の角度（Player）へ向ける
		Rot = Vector3(0.0f, angle, 0.0f);

		Trans->SetPosition(Pos);
		//Trans->SetQuaternion(Qt);
		Trans->SetRotation(Rot);


		Obj->SearchTime--;

		if (Obj->SearchTime <= 0)
		{
			Obj->SearchTime = 60;
			//Obj->GetTeleportStateMachine()->Reset(TeleportAttackState::Instance());
			Obj->GetTeleportStateMachine()->Reset(TeleportLaserAttackState::Instance());

		}


	}

	void TeleportDefaultState::Exit(const shared_ptr<TeleportEnemys>& Obj)
	{
	}

	//--------------------------------------------------------------------------------------
	///	攻撃ステート
	//--------------------------------------------------------------------------------------

	IMPLEMENT_SINGLETON_INSTANCE(TeleportAttackState)
		void TeleportAttackState::Enter(const shared_ptr<TeleportEnemys>& Obj)
	{
		auto Trans = Obj->GetComponent<Transform>();
		auto Group = Obj->GetStage()->GetSharedObjectGroup(L"EnemyAttackLaser");
		for (auto& v : Group->GetGroupVector()) {
			auto shptr = v.lock();
			if (shptr) {
				auto AttackPtr = dynamic_pointer_cast<EnemyAttackLaser>(shptr);
				if (AttackPtr && !AttackPtr->IsUpdateActive()) {
					//回転の計算
					auto RotY = Trans->GetRotation().y;
					auto Angle = Vector3(sin(RotY), 0, cos(RotY));
					Angle.Normalize();
					Quaternion Qt;
					Qt.RotationRollPitchYaw(0, RotY, 0);
					Qt.Normalize();
					auto Span = Angle * 1.5f;
					AttackPtr->Weakup(Trans->GetPosition() + Span, Qt, Angle * 12.5f);
					//AttackCoolTime = 60;
					return;
				}

			}

		}
	}

	void TeleportAttackState::Execute(const shared_ptr<TeleportEnemys>& Obj)
	{
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		Obj->TeleportTime -= ElapsedTime;
		if (Obj->TeleportTime <= 0)
		{
			Obj->TeleportTime = 2;
			Obj->GetTeleportStateMachine()->Reset(TeleportMoveState::Instance());

		}

	}

	void TeleportAttackState::Exit(const shared_ptr<TeleportEnemys>& Obj)
	{
	}


	//--------------------------------------------------------------------------------------
	///	レーザー攻撃ステート
	//--------------------------------------------------------------------------------------

	IMPLEMENT_SINGLETON_INSTANCE(TeleportLaserAttackState)
		void TeleportLaserAttackState::Enter(const shared_ptr<TeleportEnemys>& Obj)
	{




	}

	void TeleportLaserAttackState::Execute(const shared_ptr<TeleportEnemys>& Obj)
	{


		//LaserTrans->SetPosition(LaserPos.x, LaserPos.y, LaserPos.z + 0.5);



		//レーザー発射のときにだけLaserball描画
		//PtrLBall->SetDrawActive(true);

		//auto ElapsedTime = App::GetApp()->GetElapsedTime();
		//auto PtrLBall = Obj->GetStage()->GetSharedGameObject<LaserBallEffect>(L"LBall", false);
		/*auto PlayerPos = PlayerTrans->GetPosition();
		auto PlayerRot = PlayerTrans->GetRotation();*/
		auto PtrEnemyLaserEffect = Obj->GetStage()->GetSharedGameObject<EnemyLaserEffect>(L"EnemyLaserEffect", false);
		auto PtrLaserRingEffect = Obj->GetStage()->GetSharedGameObject<LaserRingEffect>(L"LaserRingEffect", false);
		auto ELaserGroup = Obj->GetStage()->GetSharedObjectGroup(L"EnemyLaser");
		auto ELaserVec = ELaserGroup->GetGroupVector();

		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		/*auto LaserTrans = PtrEnemyLaserEffect->GetComponent<Transform>();
		auto LaserPos = LaserTrans->GetPosition();
		*/



		//auto Trans = Obj->GetComponent<Transform>();
		auto Group = Obj->GetStage()->GetSharedObjectGroup(L"Enemys");
		auto GroupVec = Group->GetGroupVector();
		auto ScenePtr = App::GetApp()->GetScene<Scene>();

		for (size_t i = 0; i < Group->size(); i++) {
			if (Group->at(i)->IsUpdateActive()) {
				for (size_t e = 0; e <
					ELaserGroup->size(); e++) {
					auto ELaserPtr = dynamic_pointer_cast<EnemyLaserEffect>(ELaserGroup->at(e));
					auto EnemyPtr = dynamic_pointer_cast<Enemy>(Group->at(i));
					auto EnemyPos = Group->at(i)->GetComponent<Transform>()->GetPosition();



					auto EnemyTrans = Group->at(i)->GetComponent<Transform>();

					auto EnemyRot = EnemyTrans->GetRotation();
					auto LaserEffectRot = Vector3(EnemyRot.x, EnemyRot.y, EnemyRot.z);
					auto LaserEffectPos = Vector3(EnemyPos.x, EnemyPos.y, EnemyPos.z);

					LaserEffectRot.x += sin(EnemyPos.y);
					LaserEffectRot.z += cos(EnemyPos.y);

					LaserEffectPos.x += sin(EnemyRot.y);
					LaserEffectPos.z += cos(EnemyRot.y);
					//PtrEnemyLaserEffect->
					ELaserPtr->InsertEffect(LaserEffectPos);
					//ELaserGroup->at(e)->Get


				}
				//return;
			}

		}



		//auto ElapsedTime = App::GetApp()->GetElapsedTime();
		Obj->TeleportTime -= ElapsedTime;
		if (Obj->TeleportTime <= 0)
		{
			Obj->TeleportTime = 2;
			Obj->GetTeleportStateMachine()->Reset(TeleportMoveState::Instance());

		}



		//LaserTrans->SetPosition(LaserPos.x, LaserPos.y, LaserPos.z + 0.5);
	}

	void TeleportLaserAttackState::Exit(const shared_ptr<TeleportEnemys>& Obj)
	{

	}


	//--------------------------------------------------------------------------------------
	///	動きのステート
	//--------------------------------------------------------------------------------------

	IMPLEMENT_SINGLETON_INSTANCE(TeleportMoveState)
		void TeleportMoveState::Enter(const shared_ptr<TeleportEnemys>& Obj)
	{
		auto Trans = Obj->GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		auto PtrRigid = Obj->GetComponent<Rigidbody>();

		//if (MapPtr) {
		auto PlayerPtr = Obj->GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTrans = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerTrans->GetPosition();

		if (Obj->TeleportType >= 4)
		{
			Obj->TeleportType = 0;
		}
		/*
		srand((unsigned)time(NULL));
		srand(rand() % 100);*/
		switch (rand() % 4)
		{
		case 0:
			Trans->SetPosition(Pos.x + 3, Pos.y, Pos.z);
			Obj->TeleportType += 1;
			PtrRigid->SetVelocityZero();
			Obj->GetTeleportStateMachine()->Reset(TeleportDefaultState::Instance());

			break;

		case 1:
			Trans->SetPosition(Pos.x - 3, Pos.y, Pos.z);
			Obj->TeleportType += 1;
			PtrRigid->SetVelocityZero();
			Obj->GetTeleportStateMachine()->Reset(TeleportDefaultState::Instance());
			break;

		case 2:
			Trans->SetPosition(Pos.x, Pos.y, Pos.z + 3);
			Obj->TeleportType += 1;
			PtrRigid->SetVelocityZero();
			Obj->GetTeleportStateMachine()->Reset(TeleportDefaultState::Instance());
			break;

		case 3:
			Trans->SetPosition(Pos.x, Pos.y, Pos.z - 3);
			Obj->TeleportType += 1;
			PtrRigid->SetVelocityZero();
			Obj->GetTeleportStateMachine()->Reset(TeleportDefaultState::Instance());
			break;

		}


	}

	void TeleportMoveState::Execute(const shared_ptr<TeleportEnemys>& Obj)
	{
	}

	void TeleportMoveState::Exit(const shared_ptr<TeleportEnemys>& Obj)
	{
	}


	//--------------------------------------------------------------------------------------
	//	回転して弾を発射する敵
	//--------------------------------------------------------------------------------------
	//構築と破棄
	RotationEnemys::RotationEnemys(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<StageCellMap>& CellMap,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Tag,
		const int& Durability

	) :
		Enemy(StagePtr, CellMap, Scale, Rotation, Position, Tag, Durability)
	{}
	RotationEnemys::~RotationEnemys() {}

	void RotationEnemys::PlayrSearch() {


		//if (MapPtr) {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTrans = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerTrans->GetPosition();
		auto PlayerRot = PlayerTrans->GetRotation();
		//自分のポジション
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		auto Rot = Trans->GetRotation();



		//cosθ
		dx = PlayerPos.x - Pos.x;
		//sinθ
		dz = PlayerPos.z - Pos.z;
		//tan
		float tWork = dz / dx;
		//目的の角度取得
		angle = atan2(dx, dz);

		//目的の角度（Player）へ向ける
		Rot = Vector3(0.0f, angle, 0.0f);

		Trans->SetPosition(Pos);
		//Trans->SetQuaternion(Qt);
		Trans->SetRotation(Rot);




	}

	void RotationEnemys::OnUpdate() {
		auto PtrRigid = GetComponent<Rigidbody>();
		auto Velo = PtrRigid->GetVelocity();
		Velo *= 0.95f;
		PtrRigid->SetVelocity(Velo);
		auto MapPtr = m_CelMap.lock();
		auto Elapsodtime = App::GetApp()->GetElapsedTime();
		auto Trans = GetComponent<Transform>();
		auto Rotation = Trans->GetRotation();
		//回転の計算
		auto RotY = Trans->GetRotation().y;

		RotY += 1 / 60.0f;
		auto Angle = Vector3(sin(RotY), 0, cos(RotY));
		Quaternion Qt;
		Qt.RotationRollPitchYaw(0, RotY, 0);
		Qt.Normalize();

		//攻撃するまでの時間
		AttackTime++;
		//auto Rota;
		//Angle.Normalize();
		Trans->SetQuaternion(Qt);

		//Trans->SetRotation(Rotation.x, Rotation.y+3.5*Elapsodtime, Rotation.z);




		if (AttackTime == 180)
		{
			BulletFire = true;
			AttackTime = 0;
		}

		if (RotY <= 12.0f&&BulletFire)
		{
			//AttackCoolTime--;
			//if (AttackCoolTime <= 0) {         
			auto Group = GetStage()->GetSharedObjectGroup(L"AttackBall");
			for (auto& v : Group->GetGroupVector()) {
				auto shptr = v.lock();
				if (shptr) {
					auto AttackPtr = dynamic_pointer_cast<AttackBall>(shptr);
					if (AttackPtr && !AttackPtr->IsUpdateActive()) {
						//回転の計算
						auto RotY = Trans->GetRotation().y;
						auto Angle = Vector3(sin(RotY), 0, cos(RotY));
						Angle.Normalize();
						auto Span = Angle * 1.5f;
						//AttackPtr->Weakup(Trans->GetPosition() + Span, Angle * 12.5f);
						//AttackCoolTime = 60;

						return;
					}
					//}
				}
				BulletFire = false;
				//}
			}




		}


		if (RotY >= 18.0f)
		{
			RotY = 0.0f;
		}
		/*if (CoolTime < 0) {

		BulletFire = true;
		CoolTime = 2;
		}*/

		////弾の発射
		//if (BulletFire)
		//{
		//	//AttackCoolTime--;
		//	//if (AttackCoolTime <= 0) {         
		//	auto Group = GetStage()->GetSharedObjectGroup(L"AttackBall");
		//	for (auto& v : Group->GetGroupVector()) {
		//		auto shptr = v.lock();
		//		if (shptr) {
		//			auto AttackPtr = dynamic_pointer_cast<AttackBall>(shptr);
		//			if (AttackPtr && !AttackPtr->IsUpdateActive()) {
		//				//回転の計算
		//				auto RotY = Trans->GetRotation().y;
		//				auto Angle = Vector3(sin(RotY), 0, cos(RotY));
		//				Angle.Normalize();
		//				auto Span = Angle * 1.5f;
		//				AttackPtr->Weakup(Trans->GetPosition() + Span, Angle * 12.5f);
		//				//AttackCoolTime = 60;
		//				return;
		//			}
		//			//}
		//		}
		//		BulletFire = false;
		//		//}
		//	}
		//}
		//


	}


	//回復アイテムの作成
	Recovery::Recovery(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position,
		const wstring& Modelkey
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_Modelkey(Modelkey)
	{
	}
	Recovery::~Recovery() {}

	void Recovery::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Position);
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//PtrDraw->SetFogEnabled(true);
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"CURE_TX");

	}

	void Recovery::OnUpdate() {
	}

	//--------------------------------------------------------------------------------------------------------------------------


	//構築と破棄
	TopSpawner::TopSpawner(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	TopSpawner::~TopSpawner() {}

	//初期化
	void TopSpawner::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);
	}

	void TopSpawner::OnUpdate() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTransform = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		Trans->SetPosition(PlayerPos.x, PlayerPos.y, PlayerPos.z + 5.0f);
	}

	void TopSpawner::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
	}




	//--------------------------------------------------------------------------------------
	///	固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	UnderSpawner::UnderSpawner(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	UnderSpawner::~UnderSpawner() {}

	//初期化
	void UnderSpawner::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);
	}

	void UnderSpawner::OnUpdate() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTransform = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		Trans->SetPosition(PlayerPos.x, PlayerPos.y, PlayerPos.z - 5.0f);


	}

	void UnderSpawner::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
	}







	//--------------------------------------------------------------------------------------
	///	固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	RightSpawner::RightSpawner(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	RightSpawner::~RightSpawner() {}

	//初期化
	void RightSpawner::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);
	}

	void RightSpawner::OnUpdate() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTransform = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		Trans->SetPosition(PlayerPos.x + 5, PlayerPos.y, PlayerPos.z);

	}

	void RightSpawner::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

	}








	//--------------------------------------------------------------------------------------
	///	固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	LeftSpawner::LeftSpawner(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	LeftSpawner::~LeftSpawner() {}

	//初期化
	void LeftSpawner::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);
	}
	void LeftSpawner::OnUpdate() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerTransform = PlayerPtr->GetComponent<Transform>();
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto Trans = GetComponent<Transform>();
		auto Pos = Trans->GetPosition();
		Trans->SetPosition(PlayerPos.x - 5, PlayerPos.y, PlayerPos.z);


	}
	void LeftSpawner::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

	}











	//構築と破棄
	TopWarpBox::TopWarpBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	TopWarpBox::~TopWarpBox() {}

	//初期化
	void TopWarpBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

	}
	void TopWarpBox::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

	}




	//--------------------------------------------------------------------------------------
	///	固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	UnderWarpBox::UnderWarpBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	UnderWarpBox::~UnderWarpBox() {}

	//初期化
	void UnderWarpBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);
	}

	void UnderWarpBox::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

	}







	//--------------------------------------------------------------------------------------
	///	固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	RightWarpBox::RightWarpBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	RightWarpBox::~RightWarpBox() {}

	//初期化
	void RightWarpBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

	}

	void RightWarpBox::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		if (PlayerPtr)
		{

		}


	}








	//--------------------------------------------------------------------------------------
	///	固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	LeftWarpBox::LeftWarpBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	LeftWarpBox::~LeftWarpBox() {}

	//初期化
	void LeftWarpBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

	}

	void LeftWarpBox::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		if (PlayerPtr)
		{

		}


	}


	///////////////////////////////////////////////////////////////////////////////////6/23///////////////////////////////////////////////////////////////
	//StageClearスプライトの作成
	StageClearSprite::StageClearSprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	StageClearSprite::~StageClearSprite() {}

	void StageClearSprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
		SetDrawActive(false);
	}

	void StageClearSprite::OnUpdate() {
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		//	auto ElapsedTime = App::GetApp()->GetElapsedTime();
		//ScenePtr->DrawTime += ElapsedTime;
		//auto ScenePtr = App::GetApp()->GetScene<Scene>();

		auto Ptr = GetComponent<PTSpriteDraw>();
		if (ScenePtr->DrawTime >= 1)
		{
			SetDrawActive(true);
		}
	}


	//Clear時のNEXTスプライトの作成
	NextSprite::NextSprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	NextSprite::~NextSprite() {}

	void NextSprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
		SetDrawActive(false);
	}

	void NextSprite::OnUpdate() {


		auto ScenePtr = App::GetApp()->GetScene<Scene>();

		auto PTPtr = GetComponent<PTSpriteDraw>();
		if (ScenePtr->DrawTime >= 2)
		{
			SetDrawActive(true);
		}

	}


	//Clearとゲームオーバー時のTitleスプライトの作成
	TitleSprite::TitleSprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	TitleSprite::~TitleSprite() {}

	void TitleSprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);

		SetDrawActive(false);
	}

	void TitleSprite::OnUpdate() {
		auto ScenePtr = App::GetApp()->GetScene<Scene>();

		auto PTPtr = GetComponent<PTSpriteDraw>();
		if (ScenePtr->DrawTime >= 2)
		{
			SetDrawActive(true);
		}
	}



	//GameClearとオーバー時のcursorスプライトの作成
	CursorSprite::CursorSprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	CursorSprite::~CursorSprite() {}

	void CursorSprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
		SetDrawActive(false);
	}

	void CursorSprite::OnUpdate() {



		auto PtrTrans = GetComponent<Transform>();

		auto Pos = PtrTrans->GetPosition();
		auto ScenePtr = App::GetApp()->GetScene<Scene>();


		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		ScenePtr->DrawTime += ElapsedTime;

		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		if (CntlVec[0].bConnected) {
			if (ScenePtr->GameClear)
			{

				auto NextPtr = GetStage()->GetSharedGameObject<NextSprite>(L"NEXTTX");
				auto NextPos = NextPtr->GetComponent<Transform>()->GetPosition();
				auto TitlePtr = GetStage()->GetSharedGameObject<TitleSprite>(L"GOTITLETX");
				auto TitlePos = TitlePtr->GetComponent<Transform>()->GetPosition();

				if (CntlVec[0].fThumbLY <= -0.5) {
					PtrTrans->SetPosition(TitlePos);
				}

				if (CntlVec[0].fThumbLY >= 0.5) {
					PtrTrans->SetPosition(NextPos);
				}
				//Aボタンが押されたら.
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A&&Pos == TitlePos) {
					//イベント送出
					PostEvent(0.0f, GetThis<CursorSprite>(), App::GetApp()->GetSceneInterface(), L"ToTitle");
				}


			}
			else if (ScenePtr->GameOver)
			{
				auto RetryPtr = GetStage()->GetSharedGameObject<RetrySprite>(L"RETRYTX");
				auto RetryPos = RetryPtr->GetComponent<Transform>()->GetPosition();
				auto TitlePtr = GetStage()->GetSharedGameObject<TitleSprite>(L"GOTITLETX");
				auto TitlePos = TitlePtr->GetComponent<Transform>()->GetPosition();

				if (CntlVec[0].fThumbLY <= -0.5) {

					PtrTrans->SetPosition(TitlePos);


				}

				if (CntlVec[0].fThumbLY >= 0.5) {

					PtrTrans->SetPosition(RetryPos);

				}
				//Aボタンが押されたら.
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A&&Pos == TitlePos) {
					//イベント送出
					PostEvent(0.0f, GetThis<CursorSprite>(), App::GetApp()->GetSceneInterface(), L"ToTitle");
				}


			}





		}


		if (ScenePtr->DrawTime >= 2)
		{
			SetDrawActive(true);
		}




	}









	//GameClear時の青い線スプライトの作成
	BGSCSprite::BGSCSprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	BGSCSprite::~BGSCSprite() {}

	void BGSCSprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
	}

	void BGSCSprite::OnUpdate() {

		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto ElapsedTime = App::GetApp()->GetElapsedTime();

		if (ScenePtr->DrawTime >= 0)
		{
			SetDrawActive(true);
		}





	}










	//Clear時のスコアスプライトの作成
	ScoreSprite::ScoreSprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	ScoreSprite::~ScoreSprite() {}

	void ScoreSprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
		Ptr->SetDiffuse(Color4(1, 1, 1, 0));
		//Color4(1, 1, 1, 0);
	}

	void ScoreSprite::OnUpdate() {
		auto Ptr = GetComponent<PTSpriteDraw>();
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		//	ScenePtr->DrawTime += ElapsedTime;

		if (ScenePtr->DrawTime >= 2 && Alpha <= 1)
		{
			Alpha += ElapsedTime;
			Ptr->SetDiffuse(Color4(1, 1, 1, Alpha));
			//SetDrawActive(true);
		}





	}





	//クリア時のランクSスプライトの作成
	RankSSprite::RankSSprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	RankSSprite::~RankSSprite() {}

	void RankSSprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
		//Ptr->SetDiffuse(Color4(1, 1, 1, 0));
		SetDrawActive(false);
	}

	void RankSSprite::OnUpdate() {

		auto Ptr = GetComponent<PTSpriteDraw>();
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		//	ScenePtr->DrawTime += ElapsedTime;

		if (ScenePtr->DrawTime >= 4 && Alpha <= 1)
		{
			SetDrawActive(true);
		}




	}







	//Clear時のランクスプライトの作成
	RankSprite::RankSprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	RankSprite::~RankSprite() {}

	void RankSprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
		Ptr->SetDiffuse(Color4(1, 1, 1, 0));
	}

	void RankSprite::OnUpdate() {

		auto Ptr = GetComponent<PTSpriteDraw>();
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		//	ScenePtr->DrawTime += ElapsedTime;

		if (ScenePtr->DrawTime >= 3 && Alpha <= 1)
		{
			Alpha += ElapsedTime;
			Ptr->SetDiffuse(Color4(1, 1, 1, Alpha));
			//SetDrawActive(true);
		}





	}




	//ゲームオーバー時のゲームオーバースプライトの作成
	GameOverSprite::GameOverSprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	GameOverSprite::~GameOverSprite() {}

	void GameOverSprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
		Ptr->SetDiffuse(Color4(1, 1, 1, 0));
	}

	void GameOverSprite::OnUpdate() {

		auto Ptr = GetComponent<PTSpriteDraw>();
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		//	ScenePtr->DrawTime += ElapsedTime;

		if (ScenePtr->DrawTime >= 1 && Alpha <= 1)
		{
			Alpha += ElapsedTime;
			Ptr->SetDiffuse(Color4(1, 1, 1, Alpha));
			//SetDrawActive(true);
		}





	}





	//ゲームオーバー時のRetryスプライトの作成
	RetrySprite::RetrySprite(const shared_ptr<Stage>& StagePtr,
		bool Trace,
		const Vector2& Scale,
		const Vector3& Position,
		wstring Media) :
		GameObject(StagePtr),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Position),
		m_Media(Media),
		m_TotalTime(0.0f) {}
	//破棄
	RetrySprite::~RetrySprite() {}

	void RetrySprite::OnCreate() {
		//頂点の配列
		float HalfSize = 0.5f;
		m_BackupVertices = {
			{ VertexPositionTexture(Vector3(-HalfSize,HalfSize,0),Vector2(0,0)) },
			{ VertexPositionTexture(Vector3(HalfSize,HalfSize,0),Vector2(1,0)) },
			{ VertexPositionTexture(Vector3(-HalfSize,-HalfSize,0),Vector2(0,1)) },
			{ VertexPositionTexture(Vector3(HalfSize,-HalfSize,0),Vector2(1,1)) },
		};

		//インデックス配列
		vector<uint16_t> indices = { 0,1,2,1,3,2 };
		SetAlphaActive(m_Trace);
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetPosition(m_Pos);
		//頂点とインデックスを指定してスプライトを作成
		auto Ptr = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		Ptr->SetSamplerState(SamplerState::LinearWrap);
		Ptr->SetTextureResource(m_Media);
		Ptr->SetDrawActive(false);
	}

	void RetrySprite::OnUpdate() {

		auto Ptr = GetComponent<PTSpriteDraw>();
		auto ScenePtr = App::GetApp()->GetScene<Scene>();

		if (ScenePtr->DrawTime >= 2)
		{
			Ptr->SetDrawActive(true);
		}





	}

	////////////////////////////////////////////////////////////////////////////////////////////////////6/23///////////////////////////////////////////////////



}
//end basecross
