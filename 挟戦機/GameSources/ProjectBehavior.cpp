/*!
@file ProjectBehavior.cpp
@brief プロジェク定義の行動クラス実体
*/

#include "stdafx.h"
#include "Project.h"


namespace basecross {


	//--------------------------------------------------------------------------------------
	///	プレイヤーの行動クラス
	//--------------------------------------------------------------------------------------
	Vector3 PlayerBehavior::GetMoveVector() const {
		Vector3 Angle(0, 0, 0);
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			if (CntlVec[0].fThumbLX != 0 || CntlVec[0].fThumbLY != 0) {
				float MoveLength = 0;	//動いた時のスピード
				auto PtrTransform = GetGameObject()->GetComponent<Transform>();
				auto PtrCamera = GetGameObject()->OnGetDrawCamera();
				//進行方向の向きを計算
				Vector3 Front = PtrTransform->GetPosition() - PtrCamera->GetEye();
				Front.y = 0;
				Front.Normalize();
				//進行方向向きからの角度を算出
				float FrontAngle = atan2(Front.z, Front.x);
				//コントローラの向き計算
				float MoveX = CntlVec[0].fThumbLX;
				float MoveZ = CntlVec[0].fThumbLY;
				Vector2 MoveVec(MoveX, MoveZ);
				float MoveSize = MoveVec.Length();
				//コントローラの向きから角度を計算
				float CntlAngle = atan2(MoveZ, MoveX);
				//トータルの角度を算出
				float TotalAngle = FrontAngle + CntlAngle;
				//角度からベクトルを作成
				Angle = Vector3(cos(TotalAngle), 0, sin(TotalAngle));
				//正規化する
				Angle.Normalize();
				//移動サイズを設定。
				Angle *= MoveSize;
				//Y軸は変化させない
				Angle.y = 0;
			}
		}
		return Angle;
	}

	void PlayerBehavior::MovePlayer() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		Vector3 Angle = GetMoveVector();
		//Rigidbodyを取り出す
		auto PtrRedit = GetGameObject()->GetComponent<Rigidbody>();
		auto Velo = PtrRedit->GetVelocity();


		//コントローラーの入力とって移動
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected)
		{
			float MoveX = CntlVec[0].fThumbLX;
			float MoveZ = CntlVec[0].fThumbLY;
			Velo += Vector3(MoveX, 0, MoveZ);//* ElapsedTime * 15;
		}

		//減衰
		Velo *= 0.90f;
		PtrRedit->SetVelocity(Velo);

		/*
		if (Angle.Length() <= 0.0f && Velo.y == 0.0f) {
		//コントローラを離したとき対策
		Velo *= GetDecel();
		PtrRedit->SetVelocity(Velo);
		return;
		}
		//Transform
		auto PtrTransform = GetGameObject()->GetComponent<Transform>();
		//現在の速度を取り出す
		//目的地を最高速度を掛けて求める
		auto Target = Angle * GetMaxSpeed();
		//目的地に向かうために力のかける方向を計算する
		//Forceはフォースである
		auto Force = Target - Velo;
		//yは0にする
		Force.y = 0;
		//加速度を求める
		auto Accel = Force / GetMass();
		//ターン時間を掛けたものを速度に加算する
		Velo += (Accel * ElapsedTime);
		//速度を設定する
		PtrRedit->SetVelocity(Velo);
		*/
		//回転の計算
		if (Angle.Length() > 0.0f) {
			auto UtilPtr = GetGameObject()->GetBehavior<UtilBehavior>();

			//プレイヤーの方向変換補間処理
			UtilPtr->RotToHead(Angle, 5.0f * 1 * XM_PI / 180);
		}
	}

	void PlayerBehavior::FireAttackBall() {
		auto PtrCamera = GetGameObject()->OnGetDrawCamera();
		auto PtrTrans = GetGameObject()->GetComponent<Transform>();
		auto Group = GetStage()->GetSharedObjectGroup(L"AttackBall");
		for (auto& v : Group->GetGroupVector()) {
			auto shptr = v.lock();
			if (shptr) {
				auto AttackPtr = dynamic_pointer_cast<AttackBall>(shptr);
				if (AttackPtr && !AttackPtr->IsUpdateActive()) {
					//回転の計算
					auto RotY = PtrTrans->GetRotation().y;
					auto Angle = Vector3(sin(RotY), 0, cos(RotY));
					Quaternion Qt;
					Qt.RotationRollPitchYaw(0, RotY, 0);
					Qt.Normalize();
					Angle.Normalize();
					auto Span = Angle * 1.5f;
					AttackPtr->Weakup(PtrTrans->GetPosition() + Span,Qt, Angle * 20.0f);
					return;
				}
			}
		}

	}

	void PlayerBehavior::FireAttackLaser() {
		auto PtrPlayer = GetStage()->GetSharedGameObject<Player>(L"Player", false);
		auto PtrLBall = GetStage()->GetSharedGameObject<LaserBallEffect>(L"LBall", false);
		auto PlayerTrans = PtrPlayer->GetComponent<Transform>();
		auto PlayerPos = PlayerTrans->GetPosition();
		auto PlayerRot = PlayerTrans->GetRotation();
		auto PtrLaserEffect = GetStage()->GetSharedGameObject<LaserEffect>(L"LaserEffect", false);
		auto PtrLaserRingEffect = GetStage()->GetSharedGameObject<LaserRingEffect>(L"LaserRingEffect", false);
		auto LaserEffectRot = Vector3(PlayerRot.x, PlayerRot.y, PlayerRot.z);
		auto LaserEffectPos = Vector3(PlayerPos.x, PlayerPos.y, PlayerPos.z);
		auto ElapsedTime = App::GetApp()->GetElapsedTime();
		auto LaserTrans = PtrLaserEffect->GetComponent<Transform>();
		auto LaserPos = LaserTrans->GetPosition();


		//レーザー発射のときにだけLaserball描画
		//PtrLBall->SetDrawActive(true);


		LaserEffectRot.x += sin(PlayerPos.y);
		LaserEffectRot.z += cos(PlayerPos.y);

		LaserEffectPos.x += sin(PlayerRot.y);
		LaserEffectPos.z += cos(PlayerRot.y);
		PtrLaserEffect->InsertEffect(LaserEffectPos);
		SpawnRing += ElapsedTime;
		if (SpawnRing >= 0.5)
		{
			PtrLaserRingEffect->InsertEffect(LaserEffectPos);
			SpawnRing = 0;
		}
		LaserTrans->SetPosition(LaserPos.x, LaserPos.y, LaserPos.z + 0.5);
	}


	void PlayerBehavior::FireAttackLaser2() {
		auto PtrCamera = GetGameObject()->OnGetDrawCamera();
		auto PtrTrans = GetGameObject()->GetComponent<Transform>();
		auto Group = GetStage()->GetSharedObjectGroup(L"AttackLaser");

		auto XM_PIDIV8 = XM_PIDIV4 / 2;
		for (auto& v : Group->GetGroupVector()) {
			auto shptr = v.lock();
			if (shptr) {
				auto AttackPtr = dynamic_pointer_cast<AttackLaser>(shptr);
				if (AttackPtr && !AttackPtr->IsUpdateActive()) {
					//回転の計算
					auto RotY = PtrTrans->GetRotation().y;
					auto Angle = Vector3(sin(RotY), 0, cos(RotY));
					Quaternion Qt;
					Qt.RotationRollPitchYaw(0, RotY, 0);
					Qt.Normalize();
					//auto Rota;
					auto AngleR = Vector3(sin(RotY + XM_PIDIV8), 0, cos(RotY + XM_PIDIV8));
					auto Span = AngleR * 1.5f;
					AttackPtr->Weakup(PtrTrans->GetPosition() + Span, Qt, Angle * 25.0f);
					return;
				}
			}
		}
	}
	void PlayerBehavior::FireAttackLaser3() {
		auto PtrCamera = GetGameObject()->OnGetDrawCamera();
		auto PtrTrans = GetGameObject()->GetComponent<Transform>();
		auto Group = GetStage()->GetSharedObjectGroup(L"AttackLaser");

		auto XM_PIDIV8 = XM_PIDIV4 / 2;
		for (auto& v : Group->GetGroupVector()) {
			auto shptr = v.lock();
			if (shptr) {
				auto AttackPtr = dynamic_pointer_cast<AttackLaser>(shptr);
				if (AttackPtr && !AttackPtr->IsUpdateActive()) {
					//回転の計算
					auto RotY = PtrTrans->GetRotation().y;
					auto Angle = Vector3(sin(RotY), 0, cos(RotY));
					Quaternion Qt;
					Qt.RotationRollPitchYaw(0, RotY, 0);
					Qt.Normalize();
					//auto Rota;
					Angle.Normalize();
					auto AngleL = Vector3(sin(RotY - XM_PIDIV8), 0, cos(RotY - XM_PIDIV8));
					auto Span = AngleL * 1.5f;
					AttackPtr->Weakup(PtrTrans->GetPosition() + Span, Qt, Angle * 25.0f);
					return;
				}
			}
		}
	}


	void PlayerBehavior::FireAttackMissile() {
		auto PtrCamera = GetGameObject()->OnGetDrawCamera();
		auto PtrTrans = GetGameObject()->GetComponent<Transform>();
		auto Group = GetStage()->GetSharedObjectGroup(L"AttackMissile");
		for (auto& v : Group->GetGroupVector()) {
			auto shptr = v.lock();
			if (shptr) {
				auto AttackPtr = dynamic_pointer_cast<AttackMissile>(shptr);
				if (AttackPtr && !AttackPtr->IsUpdateActive()) {
					//回転の計算
					auto RotY = PtrTrans->GetRotation().y;
					auto Angle = Vector3(sin(RotY), 0, cos(RotY));
					Quaternion Qt;
					Qt.RotationRollPitchYaw(0, RotY, 0);
					Qt.Normalize();
					//auto Rota;
					Angle.Normalize();
					auto Span = Angle * 1.5f;
					AttackPtr->Weakup(PtrTrans->GetPosition() + Span, Qt, Angle * 15.0f);
					return;
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	///	エネミーの行動クラス 生田目 2017.04.20
	//--------------------------------------------------------------------------------------
	void EnemyBehavior::FireAttackEnemyBall() {
		auto PtrCamera = GetGameObject()->OnGetDrawCamera();
		auto PtrTrans = GetGameObject()->GetComponent<Transform>();
		auto Group = GetStage()->GetSharedObjectGroup(L"AttackBall");
		for (auto& v : Group->GetGroupVector()) {
			auto shptr = v.lock();
			if (shptr) {
				auto AttackPtr = dynamic_pointer_cast<AttackBall>(shptr);
				if (AttackPtr && !AttackPtr->IsUpdateActive()) {
					//回転の計算
					auto RotY = PtrTrans->GetRotation().y;
					auto Angle = Vector3(sin(RotY), 0, cos(RotY));
					Quaternion Qt;
					Qt.RotationRollPitchYaw(0, RotY, 0);
					Qt.Normalize();
					Angle.Normalize();
					auto Span = Angle * 0.5f;
					AttackPtr->Weakup(PtrTrans->GetPosition() + Span,Qt, Angle * 10.0f);
					return;
				}
			}
		}
	}

}

//end basecross

