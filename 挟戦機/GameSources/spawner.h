#pragma once
#include "stdafx.h"

namespace basecross {

	class TestEnemy : public GameObject {
		Vector3 m_Position;
		Color4 m_Col;
	public:
		//生成
		TestEnemy(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position,
			const Color4& col);
		//初期化
		virtual void OnCreate() override;
		//アップデート
		virtual void OnUpdate() override;
	};
	//スポナー　生田目
	class Spawner : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		const float m_Time = 3.0f;
		unique_ptr<StateMachine<Spawner>>  m_StateMachine;
		//敵が出る確率
		UINT m_Probability;
		//敵の色
		Color4 m_Col;
		//敵の配列
		vector<weak_ptr<TestEnemy>> m_EnemyVec;
	public:
		unique_ptr<StateMachine<Spawner>>& GetStateMachine() {
			return m_StateMachine;
		};
		//生成
		Spawner(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position,
			UINT Probability,
			const Color4 Col);
		virtual ~Spawner();
		//初期化
		virtual void OnCreate() override;
		//アップデート
		virtual void OnUpdate() override;
	};
}