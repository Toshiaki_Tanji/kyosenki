/*!
@file Player.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{


	//--------------------------------------------------------------------------------------
	///	プレイヤー
	//--------------------------------------------------------------------------------------
	class Player : public GameObject {
		//文字列の表示
		void DrawStrings();
		//入力ハンドラー
		InputHandler<Player> m_InputHandler;
		//階層化ステートマシーン
		unique_ptr<LayeredStateMachine<Player>>  m_StateMachine;
		Vector3 m_StartPos;


		//たまかず
		int m_RemainsBullets;
		//敵タイプ
		wstring m_EnemyType;
		//発射の間隔[時間]
		float m_FireInterval;
	
		//無敵時間
		float m_invisibleTime;
		//点滅間隔
		float m_flashinterval;
		//透明度切り替え
		int ClearChanger = 1;
		//被弾したかどうか
		bool IsDamaged;
		//被弾時の動作
		void DamageMotion();

	public:
		//構築と破棄
		//--------------------------------------------------------------------------------------
		/*!
		@brief	コンストラクタ
		@param[in]	StagePtr	ステージ
		*/
		//--------------------------------------------------------------------------------------
		Player(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		//--------------------------------------------------------------------------------------
		/*!
		@brief	デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Player() {}
		//アクセサ
		//--------------------------------------------------------------------------------------
		/*!
		@brief	ステートマシンを得る
		@return	ステートマシン
		*/
		//--------------------------------------------------------------------------------------
		unique_ptr<LayeredStateMachine<Player>>& GetStateMachine() {
			return m_StateMachine;
		}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		//後更新
		virtual void OnUpdate2() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//Aボタン
		void OnPushA();
		void OutReleaseA();
		//Bボタン
		void OnPushB() {}
		void OutReleaseB() {}
		//Xボタン
		void OnPushX(){}
		
		//押しっぱなしで発射間隔を指定して発射する
		void FireLoop();

		//耐久値
		int m_Armor;

		//発射の間隔[判定]
		bool m_isFire;


		int GetRemainsBullet();
		void SetRemainsBullet(int Bullets);
	};


	//--------------------------------------------------------------------------------------
	///	通常ステート
	//--------------------------------------------------------------------------------------
	class PlayerDefaultState : public ObjState<Player>
	{
		PlayerDefaultState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(PlayerDefaultState)
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};


	//--------------------------------------------------------------------------------------
	///	アタックステート
	//--------------------------------------------------------------------------------------
	class PlayerAttackState : public ObjState<Player>
	{
		PlayerAttackState() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(PlayerAttackState)
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};


	enum class FunnelStatus {
		Stay,Open,Close
	};

	//--------------------------------------------------------------------------------------
	///	プレイヤーに付随する子機　2017.04.20　丹治
	//--------------------------------------------------------------------------------------
	class CatchFunnel : public GameObject {

		//上限
		float m_OpenClose;
		//開閉方向
		int m_MoveDirect;
		//弾の数
		int m_Bullet;
		//弾の種類
		wstring m_Tag;
		//キャッチのフラグ
		bool IsCatch;
		//
		bool IsThrow;

		//
		bool m_IsMovement;

		//
		bool IsPressA;


	public:
		CatchFunnel(const shared_ptr<Stage>& StagePtr,
			const wstring& tag);
		virtual ~CatchFunnel();

		bool PressedA();

		void PressedB();

		bool CatchedSome();

		//キャッチする状態か
		bool IsCatcher();

		//敵を捨てるか否か
		bool ThrowEnemy();

		//空いたり閉じたりする動き
		void OpenCloseMove();
		//敵を捕まえる
		void CatchEnemy();

		virtual void OnCreate()override;

		virtual void OnUpdate()override;

	};

}
//end basecross

